﻿namespace Airlines.Persistence.Basic;
public class InvalidInputException(string message) : Exception(message)
{
}

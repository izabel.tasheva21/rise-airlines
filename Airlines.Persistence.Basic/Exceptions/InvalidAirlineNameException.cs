﻿namespace Airlines.Persistence.Basic;
public class InvalidAirlineNameException(string message) : Exception(message)
{
}

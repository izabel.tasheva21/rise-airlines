﻿namespace Airlines.Persistence.Basic;
public class InvalidFlightCodeException(string message) : Exception(message)
{
}

﻿namespace Airlines.Persistence.Basic;
public class PrivateAircraft(string model, int seats) : Aircraft(model)
{
    private readonly int _seats = seats;
    public int GetSeats()
    {
        return _seats;
    }
}
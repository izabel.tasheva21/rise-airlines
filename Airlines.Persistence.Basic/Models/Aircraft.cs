﻿namespace Airlines.Persistence.Basic;

public abstract class Aircraft(string model)
{
    public string Model = model;
}

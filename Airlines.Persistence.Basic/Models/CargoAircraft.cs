﻿
namespace Airlines.Persistence.Basic;
public class CargoAircraft(string model, int weight, double volume) : Aircraft(model)
{
    private readonly int _weight = weight;
    private readonly double _volume = volume;

    public int GetWeight()
    {
        return _weight;
    }

    public double GetVolume()
    {
        return _volume;
    }
}

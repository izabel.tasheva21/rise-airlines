﻿
namespace Airlines.Persistence.Basic;
public class PassengerAircraft(string model, int weight, double volume, int seats) : Aircraft(model)
{
    private readonly int _weight = weight;
    private readonly double _volume = volume;
    private readonly int _seats = seats;

    public int GetWeight()
    {
        return _weight;
    }

    public double GetVolume()
    {
        return _volume;
    }

    public int GetSeats()
    {
        return _seats;
    }
}

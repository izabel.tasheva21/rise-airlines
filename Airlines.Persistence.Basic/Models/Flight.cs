﻿using Airlines.Persistence.Basic;

namespace Airlines.Persistence.Basic;
public class Flight(string id, string departureAirport, string arrivalAirport, string aircraftModel, string price, string duration)
{
    // Variables 
    public string Id { get; set; } = id;
    public string DepartureAirport { get; set; } = departureAirport;
    public string ArrivalAirport { get; set; } = arrivalAirport;
    public string AircraftModel { get; set; } = aircraftModel;
    public int Price { get; set; } = int.Parse(price);
    public double Duration { get; set; } = double.Parse(duration);

    // Functions
    public bool IsValidInput() => InputData.IsValid(Id, @"(?=^)[A-Za-z0-9]+(?<=$)", "Invalid input: flight identifier must consist of alphanumeric characters.");
}

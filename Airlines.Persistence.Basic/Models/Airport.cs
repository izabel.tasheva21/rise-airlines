﻿
namespace Airlines.Persistence.Basic;
public class Airport(string identifier, string name, string city, string country)
{
    // Variables 
    public string Identifier { get; set; } = identifier;
    public string Name { get; set; } = name;
    public string City { get; set; } = city;
    public string Country { get; set; } = country;

    // Functions
    public bool IsValidInput()
    {
        var validIdentifier = InputData.IsValid(Identifier, @"(?=^)([A-Za-z]){3}(?<=$)", "Invalid input: airport identifier must consist of exactly three alphabetic characters.");
        var validName = InputData.IsValid(Name, @"(?=^)[A-Za-z]+(\s?[A-Za-z]+)*(?<=$)", "Invalid input: airport name must consist of only alphabet and space characters.");
        var validCity = InputData.IsValid(City, @"(?=^)[A-Za-z]+(\s?[A-Za-z]+)?(?<=$)", "Invalid input: airport city must consist of only alphabet and space characters.");
        var validCountry = InputData.IsValid(Country, @"(?=^)[A-Za-z]+(\s?[A-Za-z]+)?(?<=$)", "Invalid input: airport country must consist of only alphabet and space characters.");

        var isValid = validIdentifier && validName && validCity && validCountry;

        return isValid;
    }
}

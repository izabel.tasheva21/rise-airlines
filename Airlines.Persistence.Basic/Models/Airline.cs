﻿using Airlines.Persistence.Basic;

namespace Airlines.Persistence.Basic;
public class Airline(string name)
{
    // Variables 
    public string Name { get; set; } = name;

    // Functions
    public bool IsValidInput() => InputData.IsValid(Name, @"(?=^)(\w){1,5}(?<=$)", "Invalid input: airline name must consist of less than 6 characters.");

}

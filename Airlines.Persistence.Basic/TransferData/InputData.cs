using System.Text.RegularExpressions;

namespace Airlines.Persistence.Basic;

public class InputData
{
    // Validate the input data accorting to the pattern or return error message
    internal static bool IsValid(string input, string pattern, string errorMessage)
    {
        var rg = new Regex(pattern);
        var matched = rg.Matches(input);
        if (matched.Count == 0)
        {
            throw new InvalidInputException(errorMessage);
        }

        return matched.Count != 0;
    }

    // Validate the input data Name is unique
    public static bool IsUnique<T>(List<T> entities, T currentEntity, Func<T, string> getName)
    {
        foreach (var entity in entities)
        {
            if (entity != null && getName(entity).Equals(getName(currentEntity)))
            {
                Console.WriteLine($"You already have this {typeof(T).Name}. Please assign another.");
                return false;
            }
        }

        return true;
    }
}

namespace Airlines.Persistence.Basic;

public class OutputData
{
    // Print the list of entities
    public static void PrintEntities<T>(List<T> entities, Func<T, string> getName) where T : class
    {
        foreach (var entity in entities)
        {
            Console.WriteLine("- " + getName(entity));
        }
    }
}
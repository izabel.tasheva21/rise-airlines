﻿using Airlines.Persistence.Basic;
using static BusinessLogic.CommandProcessing;
using BusinessLogic;
using Airlines.Repository.Configuration;
using Airline = Airlines.Persistence.Basic.Airline;
using Airport = Airlines.Persistence.Basic.Airport;
using Flight = Airlines.Persistence.Basic.Flight;


namespace Airlines.Console;

public class Program
{
    public static void ReadAirports(string filePath, List<Airport> airports, Func<Airport, bool> isValid, Func<List<Airport>, Airport, bool> isUnique)
    {
        // Ensures that the file resources are properly disposed once the data loading is complete
        using var sr = new StreamReader(filePath);
        string line;
        while ((line = sr.ReadLine()!) != null)
        {
            var data = line.Split(",").ToArray();

            var identifier = data[0];
            var name = data[1];
            var city = data[2];
            var country = data[3];

            var currentAirport = new Airport(identifier, name, city, country);

            if (isValid(currentAirport) && isUnique(airports, currentAirport))
            {
                airports.Add(currentAirport);
            }
            else
            {
                // Handle the case where the airport is not valid or not unique.
                System.Console.WriteLine($"{currentAirport.Identifier} is not valid or not unique. Please try again.");
            }
        }
    }

    public static void ReadAirlines(string filePath, List<Airline> airlines, Func<Airline, bool> isValid, Func<List<Airline>, Airline, bool> isUnique)
    {
        // Ensures that the file resources are properly disposed once the data loading is complete
        using var sr = new StreamReader(filePath);
        string line;
        while ((line = sr.ReadLine()!) != null)
        {
            var data = line.Split(",").ToArray();

            var name = data[0];

            var currentAirline = new Airline(name);

            if (isValid(currentAirline) && isUnique(airlines, currentAirline))
            {
                airlines.Add(currentAirline);
            }
            else
            {
                // Handle the case where the airline is not valid or not unique.
                System.Console.WriteLine($"{currentAirline.Name} is not valid or not unique. Please try again.");
            }
        }
    }

    public static void ReadFlights(string filePath, List<Flight> flights, Func<Flight, bool> isValid, Func<List<Flight>, Flight, bool> isUnique)
    {
        // Ensures that the file resources are properly disposed once the data loading is complete
        using var sr = new StreamReader(filePath);
        string line;
        while ((line = sr.ReadLine()!) != null)
        {
            var data = line.Split(",").ToArray();

            var id = data[0];
            var departureAirport = data[1];
            var arrivalAirport = data[2];
            var aircraftModel = data[3];
            var price = data[4];
            var duration = data[5];

            var currentFlight = new Flight(id, departureAirport, arrivalAirport, aircraftModel, price, duration);

            if (isValid(currentFlight) && isUnique(flights, currentFlight))
            {
                flights.Add(currentFlight);
            }
            else
            {
                // Handle the case where the airline is not valid or not unique.
                System.Console.WriteLine($"{currentFlight.Id} is not valid or not unique. Please try again.");
            }
        }
    }

    public static void ReadAircrafts(string filePath, List<Aircraft> aircrafts)
    {
        // Ensures that the file resources are properly disposed once the data loading is complete
        using var sr = new StreamReader(filePath);
        string line;
        while ((line = sr.ReadLine()!) != null)
        {
            var data = line.Split(",").ToArray();

            var model = data[0];

            if (data[1].Equals("-"))
            {
                var seats = int.Parse(data[3]);
                Aircraft currentAircraft = new PrivateAircraft(model, seats);
                aircrafts.Add(currentAircraft);
            }
            else if (data[3].Equals("-"))
            {
                var weight = int.Parse(data[1]);
                var volume = double.Parse(data[2]);
                Aircraft currentAircraft = new CargoAircraft(model, weight, volume);
                aircrafts.Add(currentAircraft);
            }
            else
            {
                var weight = int.Parse(data[1]);
                var volume = double.Parse(data[2]);
                var seats = int.Parse(data[3]);
                Aircraft currentAircraft = new PassengerAircraft(model, weight, volume, seats);
                aircrafts.Add(currentAircraft);
            }
        }
    }

    public static FlightRouteTree ReadFlightRoutes(string filePath, List<Flight> flights)
    {
        // Ensures that the file resources are properly disposed once the data loading is complete
        using var sr = new StreamReader(filePath);

        FlightRouteTree flightTree;
        // Read the first line separately
        var firstLine = sr.ReadLine();
        if (firstLine != null)
        {
            // Create the Flight object using the extracted information
            var rootFlight = new Flight("-", "-", firstLine, "-", "0", "0");

            // Add the first flight to the flight list
            flightTree = new FlightRouteTree(rootFlight);
        }
        else
        {
            throw new InvalidInputException("Invalid format for airport data in the first line of the file flightRouteTreeInput.csv.");
        }

        // Read subsequent lines
        string line;
        while ((line = sr.ReadLine()!) != null)
        {
            var currFlight = flights.Find(f => f.Id.Equals(line));
            if (currFlight != null)
            {
                flightTree.Add(currFlight);
            }
        }

        return flightTree;
    }

    public static async Task PrintAirlinesAsync()
    {

        var airlineRepository = ConfigurationManager.GetAirlineRepository();

        var airlineList = await airlineRepository.GetAirlinesAsync();

        if (airlineList.Count == 0)
        {
            System.Console.WriteLine("No airlines in the db.");
            return;
        }

        foreach (var airline in airlineList)
        {
            System.Console.WriteLine($"Airline: {airline.AirlineID}");
        }
    }

    public static async Task PrintAirportsAsync()
    {

        var airportRepository = ConfigurationManager.GetAirportRepository();
        var airportList = await airportRepository.GetAirportsAsync();

        if (airportList.Count == 0)
        {
            System.Console.WriteLine("No airports in the db.");
            return;
        }

        foreach (var airport in airportList)
        {
            System.Console.WriteLine($"Airport: {airport.Code}");
        }
    }

    public static async Task PrintFlightsAsync()
    {
        var flightRepository = ConfigurationManager.GetFlightRepository();
        var flightList = await flightRepository.GetFlightsAsync();

        if (flightList.Count == 0)
        {
            System.Console.WriteLine("No flights in the db.");
            return;
        }

        foreach (var flight in flightList)
        {
            System.Console.WriteLine($"Flight: {flight.FlightNumber}");
        }
    }

    public static void PrintFlightsFromJFK()
    {
        var flightRepository = ConfigurationManager.GetFlightRepository();

        try
        {
            var flightList = flightRepository.GetFlightsByFilter("DepartureAirport", "JFK");
            foreach (var flight in flightList)
            {
                System.Console.WriteLine($"Flight: {flight.FlightNumber}");
            }
        }
        catch (Exception ex)
        {
            System.Console.WriteLine(ex.ToString());
        }
    }

    public static void Main(string[] args)
    {
        if (args is null) throw new ArgumentNullException(nameof(args));

        // Assign airports list
        var airports = new List<Airport>();

        // Assign airports list
        var airlines = new List<Airline>();

        // Assign flights list
        var flights = new List<Flight>();

        // Assign aircrafts list
        var aircrafts = new List<Aircraft>();

        // Read input data
        ReadAirports("../../../../airportInput.csv", airports, airport => airport.IsValidInput(), (a, b) => InputData.IsUnique(a, b, x => x.Identifier));
        ReadAirlines("../../../../airlineInput.csv", airlines, airline => airline.IsValidInput(), (a, b) => InputData.IsUnique(a, b, x => x.Name));
        ReadFlights("../../../../flightInput.csv", flights, flight => flight.IsValidInput(), (f, b) => InputData.IsUnique(f, b, x => x.Id));
        ReadAircrafts("../../../../aircraftInput.csv", aircrafts);
        var flightRouteTree = ReadFlightRoutes("../../../../flightRouteTreeInput.csv", flights);

        // Assign list for string all the routes
        var routes = new List<IFlightRoute<Flight>>();

        // Create a graph
        var weightedGraph = new WeightedGraph();

        // Assign edges to the graph
        foreach (var flight in flights)
        {
            var d = airports.Find(a => a.Identifier.Equals(flight.DepartureAirport));
            var a = airports.Find(a => a.Identifier.Equals(flight.ArrivalAirport));
            var weight = flight.Price;

            if (d != null && a != null)
            {
                weightedGraph.AddEdge(d, a, weight);
            }
        }

        // Command reading and processing
        while (true)
        {
            string? commandLine = null;

            commandLine = System.Console.ReadLine();

            ProcessCommand(commandLine, airports, airlines, flights, aircrafts, routes, flightRouteTree);
            System.Console.WriteLine("\n");
        }
    }
}


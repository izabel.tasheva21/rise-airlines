﻿using BusinessLogic;
using static BusinessLogic.CommandProcessing;
using static Airlines.Console.Program;
using Airline = Airlines.Persistence.Basic.Airline;
using Airport = Airlines.Persistence.Basic.Airport;
using Flight = Airlines.Persistence.Basic.Flight;
using Airlines.Persistence.Basic;

namespace Airlines.UnitTest;
public class CommandExecutionUnitTests
{
    [Theory]
    [InlineData(null)]
    public void ProcessCommand_NullCommand_ThrowsInvalidInputException(string? commandLine)
    {
        // Arrange
        var airports = new List<Airport>();
        ReadAirports("../../../../airportInput.csv", airports, airport => airport.IsValidInput(), (a, b) => InputData.IsUnique(a, b, x => x.Identifier));

        var airlines = new List<Airline>();
        ReadAirlines("../../../../airlineInput.csv", airlines, airline => airline.IsValidInput(), (a, b) => InputData.IsUnique(a, b, x => x.Name));

        var flights = new List<Flight>();
        ReadFlights("../../../../flightInput.csv", flights, flight => flight.IsValidInput(), (a, b) => InputData.IsUnique(a, b, x => x.Id));

        var aircrafts = new List<Aircraft>();
        ReadAircrafts("../../../../aircraftInput.csv", aircrafts);

        var routes = new List<IFlightRoute<Flight>>();

        var rootFlight = new Flight(" ", " ", " ", " ", "0", "0");
        var flightRouteTree = new FlightRouteTree(rootFlight);

        var weightedGraph = new WeightedGraph();

        // Act & Assert
        _ = Assert.Throws<InvalidInputException>(() => ProcessCommand(commandLine, airports, airlines, flights, aircrafts, routes, flightRouteTree));
    }

    [Theory]
    [InlineData("abcd")]
    [InlineData("sort abcd")]
    [InlineData("exist abcd")]
    [InlineData("search abcd")]
    [InlineData("batch abcd")]
    [InlineData("list abcd")]
    [InlineData("route abcd")]
    [InlineData("reserve abcd")]
    public void ProcessCommand_InvalidInputCommands_ThrowsInvalidInputException(string commandLine)
    {
        // Arrange
        var airports = new List<Airport>();
        ReadAirports("../../../../airportInput.csv", airports, airport => airport.IsValidInput(), (a, b) => InputData.IsUnique(a, b, x => x.Identifier));

        var airlines = new List<Airline>();
        ReadAirlines("../../../../airlineInput.csv", airlines, airline => airline.IsValidInput(), (a, b) => InputData.IsUnique(a, b, x => x.Name));

        var flights = new List<Flight>();
        ReadFlights("../../../../flightInput.csv", flights, flight => flight.IsValidInput(), (a, b) => InputData.IsUnique(a, b, x => x.Id));

        var aircrafts = new List<Aircraft>();
        ReadAircrafts("../../../../aircraftInput.csv", aircrafts);

        var routes = new List<IFlightRoute<Flight>>();

        var rootFlight = new Flight(" ", " ", " ", " ", "0", "0");
        var flightRouteTree = new FlightRouteTree(rootFlight);

        var command = CommandFactory.CreateCommand(commandLine);
        var commandParts = commandLine.Split(' ');

        // Act & Assert
        _ = Assert.Throws<InvalidInputException>(() => command.Execute(airports, airlines, flights, aircrafts, routes, commandParts, flightRouteTree));
    }

    [Theory]
    [InlineData("batch start", "sort flights", true)]
    [InlineData("sort flights", "exist wizz", false)]
    [InlineData("batch start", "batch run", false)]
    public void IsBatchModeOn_ShouldReturnTrue(string commandLine1, string commandLine2, bool expected)
    {
        // Arrange
        var airports = new List<Airport>();
        ReadAirports("../../../../airportInput.csv", airports, airport => airport.IsValidInput(), (a, b) => InputData.IsUnique(a, b, x => x.Identifier));

        var airlines = new List<Airline>();
        ReadAirlines("../../../../airlineInput.csv", airlines, airline => airline.IsValidInput(), (a, b) => InputData.IsUnique(a, b, x => x.Name));

        var flights = new List<Flight>();
        ReadFlights("../../../../flightInput.csv", flights, flight => flight.IsValidInput(), (a, b) => InputData.IsUnique(a, b, x => x.Id));

        var aircrafts = new List<Aircraft>();
        ReadAircrafts("../../../../aircraftInput.csv", aircrafts);

        var routes = new List<IFlightRoute<Flight>>();

        var rootFlight = new Flight(" ", " ", " ", " ", "0", "0");
        var flightRouteTree = new FlightRouteTree(rootFlight);

        // Act 
        ProcessCommand(commandLine1, airports, airlines, flights, aircrafts, routes, flightRouteTree);
        ProcessCommand(commandLine2, airports, airlines, flights, aircrafts, routes, flightRouteTree);

        var isBatchModeOn = IsBatchModeOn;

        IsBatchModeOn = false;

        // Assert
        Assert.Equal(expected, isBatchModeOn);
    }

    [Theory]
    [InlineData("sort flights", false)]
    [InlineData("batch start", true)]
    public void ImmediateCommandExecution_ShouldReturnTrue(string commandLine, bool expected)
    {
        // Arrange
        var airports = new List<Airport>();
        ReadAirports("../../../../airportInput.csv", airports, airport => airport.IsValidInput(), (a, b) => InputData.IsUnique(a, b, x => x.Identifier));

        var airlines = new List<Airline>();
        ReadAirlines("../../../../airlineInput.csv", airlines, airline => airline.IsValidInput(), (a, b) => InputData.IsUnique(a, b, x => x.Name));

        var flights = new List<Flight>();
        ReadFlights("../../../../flightInput.csv", flights, flight => flight.IsValidInput(), (a, b) => InputData.IsUnique(a, b, x => x.Id));

        var aircrafts = new List<Aircraft>();
        ReadAircrafts("../../../../aircraftInput.csv", aircrafts);

        var routes = new List<IFlightRoute<Flight>>();

        var rootFlight = new Flight(" ", " ", " ", " ", "0", "0");
        var flightRouteTree = new FlightRouteTree(rootFlight);

        var weightedGraph = new WeightedGraph();

        // Act
        ProcessCommand(commandLine, airports, airlines, flights, aircrafts, routes, flightRouteTree);

        var isBatchModeOn = IsBatchModeOn;

        IsBatchModeOn = false;

        // Assert
        Assert.Equal(expected, isBatchModeOn);
    }
}

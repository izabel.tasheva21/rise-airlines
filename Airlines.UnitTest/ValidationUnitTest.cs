﻿using BusinessLogic;
using Airline = Airlines.Persistence.Basic.Airline;
using Airport = Airlines.Persistence.Basic.Airport;
using Flight = Airlines.Persistence.Basic.Flight;
using Airlines.Persistence.Basic;

namespace Airlines.UnitTest;

public class ValidationUnitTest
{
    [Theory]
    [InlineData("JFK", "John F Kennedy International Airport", "New York", "USA", true)]
    [InlineData("jfk", "John F Kennedy International Airport", "New York", "USA", true)]
    public void IsValidAirport_ThreeLettersItem_ShouldReturnTrue(string identifier, string name, string city, string country, bool expected)
    {
        // Arrange
        var airport = new Airport(identifier, name, city, country);
        // Act
        var validationResult = airport.IsValidInput();
        // Assert
        Assert.Equal(expected, validationResult);
    }

    [Theory]
    [InlineData("JFK", "John123", "New York", "USA")]
    [InlineData("JFK", "John F Kennedy International Airport", "123", "USA")]
    [InlineData("JFK", "John F Kennedy International Airport", "New York", "123")]
    [InlineData("jf", "John F Kennedy International Airport", "New York", "USA")]
    [InlineData(" ", "John F Kennedy International Airport", "New York", "USA")]
    [InlineData("JFKDSD", "John F Kennedy International Airport", "New York", "USA")]
    [InlineData("123", "John F Kennedy International Airport", "New York", "USA")]
    [InlineData("./|", "John F Kennedy International Airport", "New York", "USA")]
    [InlineData("12hds345", "John F Kennedy International Airport", "New York", "USA")]
    public void Airport_InvalidIdentifier_ThrowsInvalidInputException(string identifier, string name, string city, string country)
    {
        // Arrange
        var airport = new Airport(identifier, name, city, country);

        // Act & Assert
        _ = Assert.Throws<InvalidInputException>(() => airport.IsValidInput());
    }

    [Theory]
    [InlineData("JFKKJ", true)]
    [InlineData("jfkdf", true)]
    [InlineData("12345", true)]
    [InlineData("123fe", true)]
    public void IsValidAirline_LessThanSixLettersItem_ShouldReturnTrue(string name, bool expected)
    {
        // Arrange
        var airlineName = name;
        var airline = new Airline(airlineName);
        // Act
        var validationResult = airline.IsValidInput();
        // Assert
        Assert.Equal(expected, validationResult);
    }

    [Theory]
    [InlineData(" ")]
    [InlineData("123456")]
    [InlineData("./|';/")]
    [InlineData("12hds345")]
    public void Airline_InvalidIdentifier_ThrowsInvalidInputException(string name)
    {
        // Arrange
        var airline = new Airline(name);

        // Act & Assert
        _ = Assert.Throws<InvalidInputException>(() => airline.IsValidInput());
    }

    [Theory]
    [InlineData("JFKKJ", true)]
    [InlineData("jfkdf", true)]
    [InlineData("12345", true)]
    [InlineData("123fe", true)]
    [InlineData("123456", true)]
    //[InlineData(" ", false)]
    //[InlineData("./|';/", false)]
    //[InlineData("12hds3##", false)]
    public void IsValidFlight_OnlyAlphanumericItem_ShouldReturnTrue(string id, bool expected)
    {
        // Arrange
        var flightId = id;
        var flight = new Flight(flightId, "JFK", "LAX", "Boeing", "0", "0");
        // Act
        var validationResult = flight.IsValidInput();
        // Assert
        Assert.Equal(expected, validationResult);
    }

    [Theory]
    [InlineData(" ")]
    [InlineData("./|';/")]
    [InlineData("12hds3##")]
    public void Flight_InvalidIdentifier_ThrowsInvalidInputException(string id)
    {
        // Arrange
        var flight = new Flight(id, "JFK", "LAX", "Boeing", "0", "0");

        // Act & Assert
        _ = Assert.Throws<InvalidInputException>(() => flight.IsValidInput());
    }
}

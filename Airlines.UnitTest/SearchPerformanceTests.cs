﻿using BusinessLogic;
using System.Diagnostics;
using Airline = Airlines.Persistence.Basic.Airline;
using Airport = Airlines.Persistence.Basic.Airport;
using Flight = Airlines.Persistence.Basic.Flight;
using Airlines.Persistence.Basic;
using static BusinessLogic.SearchingAlgorithms;

namespace Airlines.UnitTest;
public class SearchPerformanceTests
{
    private static List<Airport> GenerateRandomAirports(int count, Random random)
    {
        // Act & Assert
        return Enumerable.Range(0, count).Select(i =>
            new Airport(RandomString(3, random), "John F Kennedy International Airport", "New York", "USA") { Identifier = RandomString(3, random) }).ToList();
    }

    private static string RandomString(int length, Random random)
    {
        // Arrange
        const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

        // Act & Assert
        return new string(Enumerable.Repeat(chars, length)
            .Select(s => s[random.Next(s.Length)]).ToArray());
    }

    [Fact]
    public void LinearSearchPerformanceTest()
    {
        // Arrange
        var random = new Random(123);
        var airports = GenerateRandomAirports(1000, random);
        var target = new Airport("XYZ", "John F Kennedy International Airport", "New York", "USA") { Identifier = "XYZ" };

        // Act
        var stopwatch = Stopwatch.StartNew();
        var iterations = 0;

        while (stopwatch.ElapsedMilliseconds < 1000)
        {
            _ = InputData.IsUnique(airports, target, a => a.Identifier);
            iterations++;
        }

        // Assert
        Assert.True(iterations > 0, "No iterations completed.");
        System.Console.WriteLine($"Linear Search (Unique) Iterations in 1 second: {iterations}");
    }

    [Fact]
    public void BinarySearchPerformanceTest()
    {
        // Arrange
        var random = new Random(123);
        var airports = GenerateRandomAirports(1000, random);
        airports.Sort((a1, a2) => a1.Identifier.CompareTo(a2.Identifier));

        // Act
        var stopwatch = Stopwatch.StartNew();
        var iterations = 0;

        while (stopwatch.ElapsedMilliseconds < 1000)
        {
            _ = SearchAirports(airports, "XYZ");
            iterations++;
        }

        // Assert
        Assert.True(iterations > 0, "No iterations completed.");
        System.Console.WriteLine($"Binary Search (SearchAirports) Iterations in 1 second: {iterations}");
    }
}

﻿using BusinessLogic;
using static Airlines.Console.Program;
using static BusinessLogic.RouteCommand;
using Airline = Airlines.Persistence.Basic.Airline;
using Airport = Airlines.Persistence.Basic.Airport;
using Flight = Airlines.Persistence.Basic.Flight;
using Airlines.Persistence.Basic;

namespace Airlines.UnitTest;
public class RouteManagementUnitTest
{
    [Fact]
    public void IsInitialisedRoute_ShouldReturnTrue()
    {
        // Arrange
        var routes = new List<IFlightRoute<Flight>>();

        // Act
        InitialiseRoute(routes);
        var size = routes.Count;

        // Assert
        Assert.True(size > 0);
    }

    [Theory]
    [InlineData("FL123", true)]
    [InlineData("FL101", true)]
    [InlineData("FL123ABC", false)]
    [InlineData("FL", false)]
    [InlineData(" ", false)]
    public void IsAddedFlight_ShouldReturnTrue(string id, bool expected)
    {
        // Arrange
        var flights = new List<Flight>();
        string[] commandParts = ["route", "add", id];
        var routes = new List<IFlightRoute<Flight>>();

        // Act
        ReadFlights("../../../../flightInput.csv", flights, flight => flight.IsValidInput(), (a, b) => InputData.IsUnique(a, b, x => x.Id));
        InitialiseRoute(routes);
        AddFlightToLastRoute(commandParts, flights, routes);

        // Assert
        Assert.Equal(expected, routes.Last().Count != 0);
    }

    [Theory]
    [InlineData("FL123")]
    [InlineData("FL101")]
    [InlineData("FL")]
    [InlineData(" ")]
    public void IsRemovedFlight_OneFlightOnRoute_ShouldReturnTrue(string id)
    {
        // Arrange
        var flights = new List<Flight>();
        var routes = new List<IFlightRoute<Flight>>();
        string[] commands = ["route", "add", id];

        // Act
        ReadFlights("../../../../flightInput.csv", flights, flight => flight.IsValidInput(), (a, b) => InputData.IsUnique(a, b, x => x.Id));
        InitialiseRoute(routes);
        AddFlightToLastRoute(commands, flights, routes);
        RemoveFlightFromLastRoute(routes);

        var size = routes[0].Count;

        // Assert
        Assert.Equal((double)0, size);
    }

    [Fact]
    public void IsRemovedFlight_EmptyRoad_ShouldReturnTrue()
    {
        // Arrange
        var routes = new List<IFlightRoute<Flight>>();
        IFlightRoute<Flight> route = new FlightRoute();

        // Act
        routes.Add(route);
        RemoveFlightFromLastRoute(routes);

        var size = routes[0].Count;

        // Assert
        Assert.Equal((double)0, size);
    }
}

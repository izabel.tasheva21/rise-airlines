﻿using BusinessLogic;
using static BusinessLogic.SearchingAlgorithms;
using Airline = Airlines.Persistence.Basic.Airline;
using Airport = Airlines.Persistence.Basic.Airport;
using Flight = Airlines.Persistence.Basic.Flight;
using Airlines.Persistence.Basic;

namespace Airlines.UnitTest;
public class SearchingUnitTest
{
    [Theory]
    [InlineData("JFK", "SOF", "VAR", "JFK", true)]
    [InlineData("JFK", "SOF", "VAR", "SOF", true)]
    [InlineData("JFK", "SOF", "VAR", "VAR", true)]
    [InlineData("JFK", "SOF", "VAR", "BUR", false)]
    [InlineData("JFK", "SOF", "VAR", " ", false)]
    public void IsAirport_ShouldReturnTrue(string airport1, string airport2, string airport3, string keyWord, bool expected)
    {
        // Arrange

        var currentAirport1 = new Airport(airport1, "John F Kennedy International Airport", "New York", "USA");
        var currentAirport2 = new Airport(airport2, "John F Kennedy International Airport", "New York", "USA");
        var currentAirport3 = new Airport(airport3, "John F Kennedy International Airport", "New York", "USA");
        var airports = new List<Airport>(3) { currentAirport1, currentAirport2, currentAirport3 };
        var isAirport = false;

        // Act
        var indexOfAirport = SearchAirports(airports, keyWord);

        if (isAirport | indexOfAirport >= 0)
        {
            isAirport = true;
        }

        // Assert
        Assert.Equal(expected, isAirport);
    }

    [Theory]
    [InlineData("buzz", "ryan", "wizz", "buzz", true)]
    [InlineData("buzz", "ryan", "wizz", "ryan", true)]
    [InlineData("buzz", "ryan", "wizz", "wizz", true)]
    [InlineData("buzz", "ryan", "wizz", "turkish", false)]
    [InlineData("buzz", "ryan", "wizz", " ", false)]
    public void IsAirline_ShouldReturnTrue(string airline1, string airline2, string airline3, string keyWord, bool expected)
    {
        // Arrange
        var currentAirline1 = new Airline(airline1);
        var currentAirline2 = new Airline(airline2);
        var currentAirline3 = new Airline(airline3);
        var airlines = new List<Airline>(3) { currentAirline1, currentAirline2, currentAirline3 };
        var isAirline = false;

        // Act
        var indexOfAirline = SearchAirlines(airlines, keyWord);

        if (isAirline | indexOfAirline >= 0)
        {
            isAirline = true;
        }

        // Assert
        Assert.Equal(expected, isAirline);
    }

    [Theory]
    [InlineData("123", "456", "789", "123", true)]
    [InlineData("123", "456", "789", "456", true)]
    [InlineData("123", "456", "789", "789", true)]
    [InlineData("123", "456", "789", "ABC", false)]
    [InlineData("123", "456", "789", " ", false)]
    public void IsFlight_ShouldReturnTrue(string flight1, string flight2, string flight3, string keyWord, bool expected)
    {
        // Arrange
        var currentFlight1 = new Flight(flight1, "JFK", "LAX", "Boeing", "0", "0");
        var currentFlight2 = new Flight(flight2, "JFK", "LAX", "Boeing", "0", "0");
        var currentFlight3 = new Flight(flight3, "JFK", "LAX", "Boeing", "0", "0");
        var flights = new List<Flight>(3) { currentFlight1, currentFlight2, currentFlight3 };
        var isFlight = false;


        // Act
        var indexOfFlight = SearchFlights(flights, keyWord);

        if (isFlight | indexOfFlight >= 0)
        {
            isFlight = true;
        }

        // Assert
        Assert.Equal(expected, isFlight);
    }

    private static readonly Airport _currentAirport1 = new("JFK", "John F Kennedy International Airport", "New York", "USA");
    private static readonly Airport _currentAirport2 = new("SOF", "John F Kennedy International Airport", "New York", "USA");
    private static readonly Airport _currentAirport3 = new("VAR", "John F Kennedy International Airport", "New York", "USA");
    private static readonly List<Airport> _airports = [_currentAirport1, _currentAirport2, _currentAirport3];

    private static readonly Airline _currentAirline1 = new("buzz");
    private static readonly Airline _currentAirline2 = new("ryan");
    private static readonly Airline _currentAirline3 = new("wizz");
    private static readonly List<Airline> _airlines = [_currentAirline1, _currentAirline2, _currentAirline3];

    private static readonly Flight _currentFlight1 = new("123", "JFK", "LAX", "Boeing", "0", "0");
    private static readonly Flight _currentFlight2 = new("456", "JFK", "LAX", "Boeing", "0", "0");
    private static readonly Flight _currentFlight3 = new("789", "JFK", "LAX", "Boeing", "0", "0");
    private static readonly List<Flight> _flights = [_currentFlight1, _currentFlight2, _currentFlight3];

    public static IEnumerable<object[]> GetArraysValid()
    {
        yield return new object[] { _airports, _airlines, _flights, "JFK" };
        yield return new object[] { _airports, _airlines, _flights, "SOF" };
        yield return new object[] { _airports, _airlines, _flights, "VAR" };
        yield return new object[] { _airports, _airlines, _flights, "buzz" };
        yield return new object[] { _airports, _airlines, _flights, "ryan" };
        yield return new object[] { _airports, _airlines, _flights, "wizz" };
        yield return new object[] { _airports, _airlines, _flights, "123" };
        yield return new object[] { _airports, _airlines, _flights, "456" };
        yield return new object[] { _airports, _airlines, _flights, "789" };
    }

    public static IEnumerable<object[]> GetArraysInvalid()
    {
        yield return new object[] { _airports, _airlines, _flights, "ABC" };
        yield return new object[] { _airports, _airlines, _flights, "ABC" };
        yield return new object[] { _airports, _airlines, _flights, "ABC" };
    }


    [Theory]
    [MemberData(nameof(GetArraysValid))]
    public void IsFound_ShouldReturnTrue(List<Airport> airports, List<Airline> airlines, List<Flight> flights, string searchWord)
    {
        // Act
        var isFound = SearchKeyWord(airports, airlines, flights, searchWord);

        // Assert
        Assert.True(isFound);
    }

    [Theory]
    [MemberData(nameof(GetArraysInvalid))]
    public void IsFound_InvalidInput_ThrowsInvalidInputException(List<Airport> airports, List<Airline> airlines, List<Flight> flights, string searchWord)
    {
        // Act & Assert
        _ = Assert.Throws<InvalidInputException>(() => SearchKeyWord(airports, airlines, flights, searchWord));
    }
}

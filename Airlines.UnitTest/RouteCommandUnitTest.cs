using BusinessLogic;
using Airline = Airlines.Persistence.Basic.Airline;
using Airport = Airlines.Persistence.Basic.Airport;
using Flight = Airlines.Persistence.Basic.Flight;
using Airlines.Persistence.Basic;
using static Airlines.Console.Program;

namespace Airlines.UnitTest;

public class RouteCommandUnitTest
{
    [Fact]
    public void FindRoute_DirectFlight_ShouldReturnTrue()
    {
        // Arrange
        var flights = new List<Flight>();

        string[] commandParts = ["route", "find", "JFK"];

        // Act
        ReadFlights("../../../../flightInput.csv", flights, flight => flight.IsValidInput(), (a, b) => InputData.IsUnique(a, b, x => x.Id));
        var flightRouteTree = ReadFlightRoutes("../../../../flightRouteTreeInput.csv", flights);

        var flightNode = flightRouteTree.Find(commandParts[2]);

        if (flightNode != null)
        {
            var level = flightRouteTree.SizeFlightRoute(flightNode);

            // Assert
            Assert.Equal(2, level);
        }
    }

    [Fact]
    public void FindRoute_IndirectFlight_ShouldReturnTrue()
    {
        // Arrange
        var flights = new List<Flight>();
        string[] commands = ["route", "find", "ATL"];

        // Act
        ReadFlights("../../../../flightInput.csv", flights, flight => flight.IsValidInput(), (a, b) => InputData.IsUnique(a, b, x => x.Id));
        var flightRouteTree = ReadFlightRoutes("../../../../flightRouteTreeInput.csv", flights);

        var flightNode = flightRouteTree.Find(commands[2]);

        if (flightNode != null)
        {
            var level = flightRouteTree.SizeFlightRoute(flightNode);

            // Assert
            Assert.Equal(5, level);
        }
    }

    [Fact]
    public void FindRoute_InvalidDestionation_ThrowsInvalidInputException()
    {
        // Arrange
        var flights = new List<Flight>();
        string[] commands = ["route", "find", "VAR"];

        // Act
        ReadFlights("../../../../flightInput.csv", flights, flight => flight.IsValidInput(), (a, b) => InputData.IsUnique(a, b, x => x.Id));
        var flightRouteTree = ReadFlightRoutes("../../../../flightRouteTreeInput.csv", flights);

        // Assert
        _ = Assert.Throws<InvalidInputException>(() => flightRouteTree.Find(commands[2]));
    }

    [Fact]
    public void SearchStopsRoute_DirectFlight_ShouldReturnTrue()
    {
        // Arrange
        var flights = new List<Flight>();
        var airports = new List<Airport>();
        ReadAirports("../../../../airportInput.csv", airports, airport => airport.IsValidInput(), (a, b) => InputData.IsUnique(a, b, x => x.Identifier));

        var departure = airports[0];
        var arrival = airports[1];
        var weightedGraph = new WeightedGraph();
        var distance = new Dictionary<Airport, int>();

        // Act
        ReadFlights("../../../../flightInput.csv", flights, flight => flight.IsValidInput(), (a, b) => InputData.IsUnique(a, b, x => x.Id));

        foreach (var flight in flights)
        {
            var d = airports.Find(a => a.Identifier.Equals(flight.DepartureAirport));
            var a = airports.Find(a => a.Identifier.Equals(flight.ArrivalAirport));
            var weight = 0;

            if (d != null && a != null)
            {
                weightedGraph.AddEdge(d, a, weight);
            }
        }

        var shortestPath = weightedGraph.FewestStopsPath(departure, arrival, distance, airports);

        // Assert
        Assert.Equal([departure, arrival], shortestPath);
    }

    [Fact]
    public void SearchStopsRoute_IndirectFlight_ShouldReturnTrue()
    {
        // Arrange
        var flights = new List<Flight>();
        var airports = new List<Airport>();
        ReadAirports("../../../../airportInput.csv", airports, airport => airport.IsValidInput(), (a, b) => InputData.IsUnique(a, b, x => x.Identifier));

        var departure = airports[0];
        var mid = airports[2];
        var arrival = airports[3];
        var weightedGraph = new WeightedGraph();
        var distance = new Dictionary<Airport, int>();

        // Act
        ReadFlights("../../../../flightInput.csv", flights, flight => flight.IsValidInput(), (a, b) => InputData.IsUnique(a, b, x => x.Id));

        foreach (var flight in flights)
        {
            var d = airports.Find(a => a.Identifier.Equals(flight.DepartureAirport));
            var a = airports.Find(a => a.Identifier.Equals(flight.ArrivalAirport));
            var weight = 0;

            if (d != null && a != null)
            {
                weightedGraph.AddEdge(d, a, weight);
            }
        }

        var shortestPath = weightedGraph.FewestStopsPath(departure, arrival, distance, airports);

        // Assert
        Assert.Equal([departure, mid, arrival], shortestPath);
    }

    [Fact]
    public void SearchStopsRoute_InvalidInput_ShouldReturnTrue()
    {
        // Arrange
        var flights = new List<Flight>();
        var airports = new List<Airport>();
        ReadAirports("../../../../airportInput.csv", airports, airport => airport.IsValidInput(), (a, b) => InputData.IsUnique(a, b, x => x.Identifier));

        var departure = airports[3];
        var arrival = airports[4];
        var weightedGraph = new WeightedGraph();
        var distance = new Dictionary<Airport, int>();

        // Act
        ReadFlights("../../../../flightInput.csv", flights, flight => flight.IsValidInput(), (a, b) => InputData.IsUnique(a, b, x => x.Id));

        foreach (var flight in flights)
        {
            var d = airports.Find(a => a.Identifier.Equals(flight.DepartureAirport));
            var a = airports.Find(a => a.Identifier.Equals(flight.ArrivalAirport));
            var weight = 1;

            if (d != null && a != null)
            {
                weightedGraph.AddEdge(d, a, weight);
            }
        }

        var shortestPath = weightedGraph.FewestStopsPath(departure, arrival, distance, airports);

        // Assert
        Assert.Null(shortestPath);
    }

    [Fact]
    public void SearchCheapRoute_DirectFlight_ShouldReturnTrue()
    {
        // Arrange
        var flights = new List<Flight>();
        var airports = new List<Airport>();
        ReadAirports("../../../../airportInput.csv", airports, airport => airport.IsValidInput(), (a, b) => InputData.IsUnique(a, b, x => x.Identifier));

        var departure = airports[0];
        var arrival = airports[1];
        var weightedGraph = new WeightedGraph();
        var distance = new Dictionary<Airport, double>();

        // Act
        ReadFlights("../../../../flightInput.csv", flights, flight => flight.IsValidInput(), (a, b) => InputData.IsUnique(a, b, x => x.Id));

        foreach (var flight in flights)
        {
            var d = airports.Find(a => a.Identifier.Equals(flight.DepartureAirport));
            var a = airports.Find(a => a.Identifier.Equals(flight.ArrivalAirport));
            var weight = flight.Price;

            if (d != null && a != null)
            {
                weightedGraph.AddEdge(d, a, weight);
            }
        }

        var cheapestPath = weightedGraph.LowestSumWeightPath(departure, arrival, distance, airports);

        // Assert
        Assert.Equal([departure, arrival], cheapestPath);
    }

    [Fact]
    public void SearchCheapRoute_IndirectFlight_ShouldReturnTrue()
    {
        // Arrange
        var flights = new List<Flight>();
        var airports = new List<Airport>();
        ReadAirports("../../../../airportInput.csv", airports, airport => airport.IsValidInput(), (a, b) => InputData.IsUnique(a, b, x => x.Identifier));

        var departure = airports[0];
        var mid = airports[1];
        var arrival = airports[2];
        var weightedGraph = new WeightedGraph();
        var distance = new Dictionary<Airport, double>();

        // Act
        ReadFlights("../../../../flightInput.csv", flights, flight => flight.IsValidInput(), (a, b) => InputData.IsUnique(a, b, x => x.Id));

        foreach (var flight in flights)
        {
            var d = airports.Find(a => a.Identifier.Equals(flight.DepartureAirport));
            var a = airports.Find(a => a.Identifier.Equals(flight.ArrivalAirport));
            var weight = flight.Price;

            if (d != null && a != null)
            {
                weightedGraph.AddEdge(d, a, weight);
            }
        }

        var cheapestPath = weightedGraph.LowestSumWeightPath(departure, arrival, distance, airports);

        // Assert
        Assert.Equal([departure, mid, arrival], cheapestPath);
    }

    [Fact]
    public void SearchCheapRoute_InvalidInput_ShouldReturnTrue()
    {
        // Arrange
        var flights = new List<Flight>();
        var airports = new List<Airport>();
        ReadAirports("../../../../airportInput.csv", airports, airport => airport.IsValidInput(), (a, b) => InputData.IsUnique(a, b, x => x.Identifier));

        var departure = airports[3];
        var arrival = airports[4];
        var weightedGraph = new WeightedGraph();
        var distance = new Dictionary<Airport, double>();


        // Act
        ReadFlights("../../../../flightInput.csv", flights, flight => flight.IsValidInput(), (a, b) => InputData.IsUnique(a, b, x => x.Id));

        foreach (var flight in flights)
        {
            var d = airports.Find(a => a.Identifier.Equals(flight.DepartureAirport));
            var a = airports.Find(a => a.Identifier.Equals(flight.ArrivalAirport));
            var weight = flight.Price;

            if (d != null && a != null)
            {
                weightedGraph.AddEdge(d, a, weight);
            }
        }

        var cheapestPath = weightedGraph.LowestSumWeightPath(departure, arrival, distance, airports);

        // Assert
        Assert.Null(cheapestPath);
    }

    [Fact]
    public void SearchShortRoute_DirectFlight_ShouldReturnTrue()
    {
        // Arrange
        var flights = new List<Flight>();
        var airports = new List<Airport>();
        ReadAirports("../../../../airportInput.csv", airports, airport => airport.IsValidInput(), (a, b) => InputData.IsUnique(a, b, x => x.Identifier));

        var departure = airports[0];
        var arrival = airports[1];
        var weightedGraph = new WeightedGraph();
        var distance = new Dictionary<Airport, double>();

        // Act
        ReadFlights("../../../../flightInput.csv", flights, flight => flight.IsValidInput(), (a, b) => InputData.IsUnique(a, b, x => x.Id));

        foreach (var flight in flights)
        {
            var d = airports.Find(a => a.Identifier.Equals(flight.DepartureAirport));
            var a = airports.Find(a => a.Identifier.Equals(flight.ArrivalAirport));
            var weight = flight.Duration;

            if (d != null && a != null)
            {
                weightedGraph.AddEdge(d, a, weight);
            }
        }

        var shortestPath = weightedGraph.LowestSumWeightPath(departure, arrival, distance, airports);

        // Assert
        Assert.Equal([departure, arrival], shortestPath);
    }

    [Fact]
    public void SearchShortRoute_IndirectFlight_ShouldReturnTrue()
    {
        // Arrange
        var flights = new List<Flight>();
        var airports = new List<Airport>();
        ReadAirports("../../../../airportInput.csv", airports, airport => airport.IsValidInput(), (a, b) => InputData.IsUnique(a, b, x => x.Identifier));

        var departure = airports[0];
        var mid = airports[1];
        var arrival = airports[2];
        var weightedGraph = new WeightedGraph();
        var distance = new Dictionary<Airport, double>();

        // Act
        ReadFlights("../../../../flightInput.csv", flights, flight => flight.IsValidInput(), (a, b) => InputData.IsUnique(a, b, x => x.Id));

        foreach (var flight in flights)
        {
            var d = airports.Find(a => a.Identifier.Equals(flight.DepartureAirport));
            var a = airports.Find(a => a.Identifier.Equals(flight.ArrivalAirport));
            var weight = flight.Duration;

            if (d != null && a != null)
            {
                weightedGraph.AddEdge(d, a, weight);
            }
        }

        var shortestPath = weightedGraph.LowestSumWeightPath(departure, arrival, distance, airports);

        // Assert
        Assert.Equal([departure, mid, arrival], shortestPath);
    }

    [Fact]
    public void SearchShortRoute_InvalidInput_ShouldReturnTrue()
    {
        // Arrange
        var flights = new List<Flight>();
        var airports = new List<Airport>();
        ReadAirports("../../../../airportInput.csv", airports, airport => airport.IsValidInput(), (a, b) => InputData.IsUnique(a, b, x => x.Identifier));

        var departure = airports[3];
        var arrival = airports[4];
        var weightedGraph = new WeightedGraph();
        var distance = new Dictionary<Airport, int>();

        // Act
        ReadFlights("../../../../flightInput.csv", flights, flight => flight.IsValidInput(), (a, b) => InputData.IsUnique(a, b, x => x.Id));

        foreach (var flight in flights)
        {
            var d = airports.Find(a => a.Identifier.Equals(flight.DepartureAirport));
            var a = airports.Find(a => a.Identifier.Equals(flight.ArrivalAirport));
            var weight = 0;

            if (d != null && a != null)
            {
                weightedGraph.AddEdge(d, a, weight);
            }
        }

        var shortestPath = weightedGraph.FewestStopsPath(departure, arrival, distance, airports);

        // Assert
        Assert.Null(shortestPath);
    }

    [Fact]
    public void CheckRoute_DirectFlight_ShouldReturnTrue()
    {
        // Arrange
        var flights = new List<Flight>();
        var airports = new List<Airport>();
        ReadAirports("../../../../airportInput.csv", airports, airport => airport.IsValidInput(), (a, b) => InputData.IsUnique(a, b, x => x.Identifier));

        var departure = airports[0];
        var arrival = airports[1];
        var weightedGraph = new WeightedGraph();

        // Act
        ReadFlights("../../../../flightInput.csv", flights, flight => flight.IsValidInput(), (a, b) => InputData.IsUnique(a, b, x => x.Id));

        foreach (var flight in flights)
        {
            var d = airports.Find(a => a.Identifier.Equals(flight.DepartureAirport));
            var a = airports.Find(a => a.Identifier.Equals(flight.ArrivalAirport));
            var weight = 0;

            if (d != null && a != null)
            {
                weightedGraph.AddEdge(d, a, weight);
            }
        }

        var exist = weightedGraph.IsReachable(departure, arrival);

        // Assert
        Assert.True(exist);
    }

    [Fact]
    public void CheckRoute_IndirectFlight_ShouldReturnTrue()
    {
        // Arrange
        var flights = new List<Flight>();
        var airports = new List<Airport>();
        ReadAirports("../../../../airportInput.csv", airports, airport => airport.IsValidInput(), (a, b) => InputData.IsUnique(a, b, x => x.Identifier));

        var departure = airports[0];
        var arrival = airports[3];
        var weightedGraph = new WeightedGraph();

        // Act
        ReadFlights("../../../../flightInput.csv", flights, flight => flight.IsValidInput(), (a, b) => InputData.IsUnique(a, b, x => x.Id));

        foreach (var flight in flights)
        {
            var d = airports.Find(a => a.Identifier.Equals(flight.DepartureAirport));
            var a = airports.Find(a => a.Identifier.Equals(flight.ArrivalAirport));
            var weight = 0;

            if (d != null && a != null)
            {
                weightedGraph.AddEdge(d, a, weight);
            }
        }

        var exist = weightedGraph.IsReachable(departure, arrival);

        // Assert
        Assert.True(exist);
    }

    [Fact]
    public void CheckRoute_InvalidInput_ShouldReturnTrue()
    {
        // Arrange
        var flights = new List<Flight>();
        var airports = new List<Airport>();
        ReadAirports("../../../../airportInput.csv", airports, airport => airport.IsValidInput(), (a, b) => InputData.IsUnique(a, b, x => x.Identifier));

        var departure = airports[3];
        var arrival = airports[4];
        var weightedGraph = new WeightedGraph();

        // Act
        ReadFlights("../../../../flightInput.csv", flights, flight => flight.IsValidInput(), (a, b) => InputData.IsUnique(a, b, x => x.Id));

        foreach (var flight in flights)
        {
            var d = airports.Find(a => a.Identifier.Equals(flight.DepartureAirport));
            var a = airports.Find(a => a.Identifier.Equals(flight.ArrivalAirport));
            var weight = 0;

            if (d != null && a != null)
            {
                weightedGraph.AddEdge(d, a, weight);
            }
        }

        var exist = weightedGraph.IsReachable(departure, arrival);

        // Assert
        Assert.False(exist);
    }
}

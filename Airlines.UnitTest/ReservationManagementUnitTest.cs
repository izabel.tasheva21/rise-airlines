﻿using BusinessLogic;
using static Airlines.Console.Program;
using static BusinessLogic.ReserveCommand;
using Airline = Airlines.Persistence.Basic.Airline;
using Airport = Airlines.Persistence.Basic.Airport;
using Flight = Airlines.Persistence.Basic.Flight;
using Airlines.Persistence.Basic;


namespace Airlines.UnitTest;
public class ReservationManagementUnitTest
{
    [Theory]
    [InlineData("FL123", "100", "100", true)]
    [InlineData("FL101", "100", "100", false)]
    [InlineData("FL123", "1000000", "100", false)]
    [InlineData("FL101", "100", "1000000", false)]

    public void IsReservedCargo_ShouldReturnTrue(string id, string weight, string volume, bool expected)
    {
        // Arrange
        var flights = new List<Flight>();
        var aircrafts = new List<Aircraft>();

        string[] commandParts = ["reserve", "cargo", id, weight, volume];

        // Act
        ReadFlights("../../../../flightInput.csv", flights, flight => flight.IsValidInput(), (a, b) => InputData.IsUnique(a, b, x => x.Id));
        ReadAircrafts("../../../../aircraftInput.csv", aircrafts);

        var isSuccessfull = CargoReservation(commandParts, flights, aircrafts);

        // Assert
        Assert.Equal(expected, isSuccessfull);
    }

    [Theory]
    [InlineData("FL101", "1", "10", "10", true)]
    [InlineData("FL123", "1", "10", "10", false)]
    [InlineData("FL101", "1000", "10", "10", false)]
    [InlineData("FL101", "1", "1000000", "10", false)]
    [InlineData("FL101", "1", "10", "1000000", false)]
    public void IsReservedTicket_ShouldReturnTrue(string id, string seats, string smallBaggage, string largeBaggage, bool expected)
    {
        // Arrange
        var flights = new List<Flight>();
        var aircrafts = new List<Aircraft>();

        string[] commands = ["reserve", "cargo", id, seats, smallBaggage, largeBaggage];

        // Act
        ReadFlights("../../../../flightInput.csv", flights, flight => flight.IsValidInput(), (a, b) => InputData.IsUnique(a, b, x => x.Id));
        ReadAircrafts("../../../../aircraftInput.csv", aircrafts);

        var isSuccessfull = TicketReservation(commands, flights, aircrafts);

        // Assert
        Assert.Equal(expected, isSuccessfull);
    }
}

﻿using BusinessLogic;
using Airline = Airlines.Persistence.Basic.Airline;
using Airport = Airlines.Persistence.Basic.Airport;
using Flight = Airlines.Persistence.Basic.Flight;
using Airlines.Persistence.Basic;
using static Airlines.Console.Program;

namespace Airlines.UnitTest
{
    public class ReadingUnitTest
    {
        [Theory]
        [InlineData("../../../../airportInput.csv", true)]
        public void IsReadAirportsFromFile_ShouldReturnTrue(string filePath, bool expected)
        {
            // Arrange
            var airports = new List<Airport>();

            // Act
            ReadAirports(filePath, airports, airport => airport.IsValidInput(), (a, b) => InputData.IsUnique(a, b, x => x.Identifier));

            // Assert
            Assert.Equal(expected, airports != null);
        }

        [Theory]
        [InlineData("../../../../invalidInput.csv")]
        public void IsReadAirportsFromFile_ThrowsIndexOutOfRangeException(string filePath)
        {
            // Arrange
            var airports = new List<Airport>();

            // Act & Assert
            _ = Assert.Throws<IndexOutOfRangeException>(() => ReadAirports(filePath, airports, airport => airport.IsValidInput(), (a, b) => InputData.IsUnique(a, b, x => x.Identifier)));
        }

        [Theory]
        [InlineData("../../../../airlineInput.csv", true)]
        public void IsReadAirlinesFromFile_ShouldReturnTrue(string filePath, bool expected)
        {
            // Arrange
            var airlines = new List<Airline>();

            // Act
            ReadAirlines(filePath, airlines, airline => airline.IsValidInput(), (a, b) => InputData.IsUnique(a, b, x => x.Name));

            // Assert
            Assert.Equal(expected, airlines.Count > 0);
        }

        [Theory]
        [InlineData("../../../../invalidInput.csv")]
        public void IsReadAirlinesFromFile_ThrowsInvalidInputException(string filePath)
        {
            // Arrange
            var airlines = new List<Airline>();

            // Act & Assert
            _ = Assert.Throws<InvalidInputException>(() => ReadAirlines(filePath, airlines, airline => airline.IsValidInput(), (a, b) => InputData.IsUnique(a, b, x => x.Name)));
        }

        [Theory]
        [InlineData("../../../../flightInput.csv", true)]
        public void IsReadFlightsFromFile_ShouldReturnTrue(string filePath, bool expected)
        {
            // Arrange
            var flights = new List<Flight>();

            // Act
            ReadFlights(filePath, flights, flight => flight.IsValidInput(), (a, b) => InputData.IsUnique(a, b, x => x.Id));

            // Assert
            Assert.Equal(expected, flights.Count > 0);
        }

        [Theory]
        [InlineData("../../../../invalidInput.csv")]
        public void IsReadFlightsFromFile_ThrowsIndexOutOfRangeException(string filePath)
        {
            // Arrange
            var flights = new List<Flight>();

            // Act & Assert
            _ = Assert.Throws<IndexOutOfRangeException>(() => ReadFlights(filePath, flights, flight => flight.IsValidInput(), (a, b) => InputData.IsUnique(a, b, x => x.Id)));
        }

        [Theory]
        [InlineData("../../../../aircraftInput.csv", true)]
        public void IsReadAircraftsFromFile_ShouldReturnTrue(string filePath, bool expected)
        {
            // Arrange
            var aircrafts = new List<Aircraft>();

            // Act
            ReadAircrafts(filePath, aircrafts);

            // Assert
            Assert.Equal(expected, aircrafts.Count > 0);
        }

        [Theory]
        [InlineData("../../../../invalidInput.csv")]
        public void IsReadAircraftsFromFile_ThrowsIndexOutOfRangeException(string filePath)
        {
            // Arrange
            var aircrafts = new List<Aircraft>();

            // Act & Assert
            _ = Assert.Throws<IndexOutOfRangeException>(() => ReadAircrafts(filePath, aircrafts));
        }

        [Theory]
        [InlineData("../../../../flightInput.csv", "../../../../flightRouteTreeInput.csv", true)]
        public void IsReadFlightRouteTreeFromFile_ShouldReturnTrue(string filePathFlight, string filePathTree, bool expected)
        {
            // Arrange
            var flights = new List<Flight>();

            // Act
            ReadFlights(filePathFlight, flights, flight => flight.IsValidInput(), (a, b) => InputData.IsUnique(a, b, x => x.Id));

            var flightRouteTree = ReadFlightRoutes(filePathTree, flights);

            // Assert
            Assert.Equal(expected, flightRouteTree.Size() > 0);
        }

        [Theory]
        [InlineData("../../../../flightInput.csv", "../../../../invalidInput.csv")]
        public void IsReadFlightRouteTreeFromFile_ThrowsIndexOutOfRangeException(string filePathFlight, string filePathTree)
        {
            // Arrange
            var flights = new List<Flight>();

            // Act
            ReadFlights(filePathFlight, flights, flight => flight.IsValidInput(), (a, b) => InputData.IsUnique(a, b, x => x.Id));

            var flightRouteTree = ReadFlightRoutes(filePathTree, flights);

            // Assert
            Assert.Equal(1, flightRouteTree.Size());
        }
    }
}
﻿using BusinessLogic;
using System.Diagnostics;
using static BusinessLogic.SortingAlgorithms;
using Airline = Airlines.Persistence.Basic.Airline;
using Airport = Airlines.Persistence.Basic.Airport;
using Flight = Airlines.Persistence.Basic.Flight;
using Airlines.Persistence.Basic;


namespace Airlines.UnitTest;
public class SortingPerformanceTests
{
    public const int Seed = 123;
    public const int MaxEntities = 1000;

    [Fact]
    public void BubbleSortPerformanceTest()
    {
        // Arrange
        var random = new Random(Seed);

        // Act
        var stopwatch = Stopwatch.StartNew();
        var iterations = 0;

        while (stopwatch.ElapsedMilliseconds < 1000)
        {
            var airports = GenerateRandomAirports(MaxEntities, random);
            BubbleSort(airports.ToList(), (a1, a2) => string.Compare(a1.Name, a2.Name) > 0);
            iterations++;
        }

        // Assert
        Assert.True(iterations > 0, "No iterations completed. Increase MaxEntities or adjust test.");
        System.Console.WriteLine($"Bubble Sort Iterations in 1 second: {iterations}");
    }

    [Fact]
    public void SelectionSortPerformanceTest()
    {
        // Arrange
        var random = new Random(Seed);

        // Act
        var stopwatch = Stopwatch.StartNew();
        var iterations = 0;

        while (stopwatch.ElapsedMilliseconds < 1000)
        {
            var airlines = GenerateRandomAirlines(MaxEntities, random);
            SelectionSort(airlines.ToList(), (a1, a2) => a1.Name.CompareTo(a2.Name));
            iterations++;
        }

        // Assert
        Assert.True(iterations > 0, "No iterations completed. Increase MaxEntities or adjust test.");
        System.Console.WriteLine($"Selection Sort Iterations in 1 second: {iterations}");
    }

    private static Airport[] GenerateRandomAirports(int count, Random random)
    {
        var airports = new Airport[count];
        for (var i = 0; i < count; i++)
        {
            airports[i] = new Airport(RandomString(3, random), "John F Kennedy International Airport", "New York", "USA");
        }
        return airports;
    }

    private static List<Airline> GenerateRandomAirlines(int count, Random random)
    {
        var airlines = new List<Airline>(count);
        for (var i = 0; i < count; i++)
        {
            airlines.Add(new Airline(RandomString(3, random)));
        }
        return airlines;
    }

    private static string RandomString(int length, Random random)
    {
        // Arrange
        const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

        // Act & Assert
        return new string(Enumerable.Repeat(chars, length)
          .Select(s => s[random.Next(s.Length)]).ToArray());
    }
}

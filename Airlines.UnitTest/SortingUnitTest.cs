﻿using static BusinessLogic.SortingAlgorithms;
using Airline = Airlines.Persistence.Basic.Airline;
using Airport = Airlines.Persistence.Basic.Airport;
using Flight = Airlines.Persistence.Basic.Flight;

namespace Airlines.UnitTest;
public class SortingUnitTest
{
    private static readonly List<string> _sortedArray1 = ["ABC", "BCD", "CDE", "DEF", "EFG"];
    private static readonly List<string> _unsortedArray1 = ["EFG", "DEF", "CDE", "BCD", "ABC"];
    private static readonly List<string> _sortedArray2 = [" ", " ", " ", " ", " "];
    private static readonly List<string> _unsortedArray2 = [" ", " ", " ", " ", " "];
    private static readonly List<string> _sortedArray3 = ["ABC"];
    private static readonly List<string> _unsortedArray3 = ["ABC"];
    private static readonly List<string> _sortedArray4 = ["ABC", "ABC", "CDE", "DEF", "EFG"];
    private static readonly List<string> _unsortedArray4 = ["EFG", "DEF", "CDE", "ABC", "ABC"];

    [Theory]
    [MemberData(nameof(GetAirportData))]
    public void IsSortedAirport_BubbleSort_ShouldReturnTrue(List<string> sortedArray, List<string> unsortedArray, bool expected)
    {
        // Arrange
        var sortedAirports = new List<Airport>(sortedArray.Count);

        for (var i = 0; i <= sortedArray.Count - 1; i++)
        {
            var airport = new Airport(sortedArray[i], "John F Kennedy International Airport", "New York", "USA");
            sortedAirports.Add(airport);
        }

        var unsortedAirports = new List<Airport>(unsortedArray.Count);

        for (var i = 0; i <= unsortedArray.Count - 1; i++)
        {
            var airport = new Airport(unsortedArray[i], "John F Kennedy International Airport", "New York", "USA");
            unsortedAirports.Add(airport);
        }

        // Act
        BubbleSort(unsortedAirports, (a1, a2) => string.Compare(a1.Identifier, a2.Identifier) > 0);

        var isSorted = true;
        for (var i = 0; i <= sortedAirports.Count - 1; i++)
        {
            if (sortedAirports[i].Identifier != unsortedAirports[i].Identifier)
            {
                isSorted = false;
            }
        }

        // Assert
        Assert.Equal(expected, isSorted);
    }

    [Theory]
    [MemberData(nameof(GetAirlineData))]
    public void IsSortedAirline_SelectingSort_ShouldReturnTrue(List<string> sortedArray, List<string> unsortedArray, bool expected)
    {
        // Arrange
        var sortedAirlines = new List<Airline>(sortedArray.Count);

        for (var i = 0; i <= sortedArray.Count - 1; i++)
        {
            var airline = new Airline(sortedArray[i]);
            sortedAirlines.Add(airline);
        }

        var unsortedAirlines = new List<Airline>(unsortedArray.Count);

        for (var i = 0; i <= unsortedArray.Count - 1; i++)
        {
            var airline = new Airline(unsortedArray[i]);
            unsortedAirlines.Add(airline);
        }

        // Act
        SelectionSort(unsortedAirlines, (a1, a2) => a1.Name.CompareTo(a2.Name));

        var isSorted = true;
        for (var i = 0; i <= sortedAirlines.Count - 1; i++)
        {
            if (sortedAirlines[i].Name != unsortedAirlines[i].Name)
            {
                isSorted = false;
            }
        }

        // Assert
        Assert.Equal(expected, isSorted);
    }

    [Theory]
    [MemberData(nameof(GetFlightData))]
    public void IsSortedFlight_SelectingSort_ShouldReturnTrue(List<string> sortedArray, List<string> unsortedArray, bool expected)
    {
        // Arrange
        var sortedFlights = new List<Flight>(sortedArray.Count);

        for (var i = 0; i <= sortedArray.Count - 1; i++)
        {
            var flight = new Flight(sortedArray[i], "JFK", "LAX", "Boeing", "0", "0");
            sortedFlights.Add(flight);
        }

        var unsortedFlights = new List<Flight>(unsortedArray.Count);

        for (var i = 0; i <= unsortedArray.Count - 1; i++)
        {
            var flight = new Flight(unsortedArray[i], "JFK", "LAX", "Boeing", "0", "0");
            unsortedFlights.Add(flight);
        }

        // Act
        SelectionSort(unsortedFlights, (a1, a2) => a1.Id.CompareTo(a2.Id));

        var isSorted = true;
        for (var i = 0; i <= sortedFlights.Count - 1; i++)
        {
            if (sortedFlights[i].Id != unsortedFlights[i].Id)
            {
                isSorted = false;
            }
        }

        // Assert
        Assert.Equal(expected, isSorted);
    }

    public static IEnumerable<object[]> GetAirportData()
    {
        yield return new object[] { _sortedArray1, _unsortedArray1, true };
        yield return new object[] { _sortedArray2, _unsortedArray2, true };
        yield return new object[] { _sortedArray3, _unsortedArray3, true };
        yield return new object[] { _sortedArray4, _unsortedArray4, true };
    }

    public static IEnumerable<object[]> GetAirlineData()
    {
        yield return new object[] { _sortedArray1, _unsortedArray1, true };
        yield return new object[] { _sortedArray2, _unsortedArray2, true };
        yield return new object[] { _sortedArray3, _unsortedArray3, true };
        yield return new object[] { _sortedArray4, _unsortedArray4, true };
    }

    public static IEnumerable<object[]> GetFlightData()
    {
        yield return new object[] { _sortedArray1, _unsortedArray1, true };
        yield return new object[] { _sortedArray2, _unsortedArray2, true };
        yield return new object[] { _sortedArray3, _unsortedArray3, true };
        yield return new object[] { _sortedArray4, _unsortedArray4, true };
    }
}

﻿using BusinessLogic;
using Airline = Airlines.Persistence.Basic.Airline;
using Airport = Airlines.Persistence.Basic.Airport;
using Flight = Airlines.Persistence.Basic.Flight;
using Airlines.Persistence.Basic;

namespace Airlines.UnitTest;
public class UniqueValidationUnitTest
{
    [Theory]
    [InlineData("JFK", "SOF", true)]
    [InlineData("JFK", "JFK", false)]
    public void IsUnique_ShouldReturnTrue(string firstAirport, string secondAirport, bool expected)
    {
        // Arrange
        var airport = new Airport(firstAirport, "John F Kennedy International Airport", "New York", "USA");
        var airports = new List<Airport>(1) { airport };

        var currentAirport = new Airport(secondAirport, "John F Kennedy International Airport", "New York", "USA");

        // Act
        var isUnique = InputData.IsUnique(airports, currentAirport, x => x.Identifier);

        // Assert
        Assert.Equal(expected, isUnique);

    }
}

INSERT INTO dbo.Flight (FlightNumber, DepartureAirport, ArrivalAirport, DepartureDateTime, ArrivalDateTime)
VALUES 
('FL123', 'JFK', 'LAX', '2024-04-16 21:30:00', '2024-04-16 23:30:00'),
('FL456', 'LAX', 'ORD', '2024-04-15 15:00:00', '2024-04-15 19:30:00')

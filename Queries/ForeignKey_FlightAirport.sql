ALTER TABLE Flight
ADD FOREIGN KEY (DepartureAirport) REFERENCES Airport(Code);

ALTER TABLE Flight
ADD FOREIGN KEY (ArrivalAirport) REFERENCES Airport(Code);

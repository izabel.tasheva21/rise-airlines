DROP TABLE IF EXISTS dbo.Flight;
GO

CREATE TABLE dbo.Flight (
    FlightID INT IDENTITY,
    FlightNumber VARCHAR(5) CONSTRAINT PK_Flight PRIMARY KEY NOT NULL,
	DepartureAirport CHAR(3) NOT NULL,
	ArrivalAirport CHAR(3) NOT NULL,
	DepartureDateTime DateTime NOT NULL,
	ArrivalDateTime DateTime NOT NULL,
    CONSTRAINT CK_ArrivalDateTime CHECK (ArrivalDateTime >= GETDATE()),
    CONSTRAINT CK_DepartureBeforeArrival CHECK (DepartureDateTime < ArrivalDateTime)
);
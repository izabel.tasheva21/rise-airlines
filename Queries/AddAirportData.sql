INSERT INTO dbo.Airport (Code, Name, City, Country, RunwaysCount, Founded)
VALUES 
('JFK', 'John F Kennedy International Airport', 'New York', 'USA', 1,'2024-05-20'),
('LAX', 'Los Angeles International Airport', 'Los Angelis', 'USA', 1,'2024-05-20'),
('ORD', 'Chicago OHare International Airport', 'Chicago', 'USA', 1,'2024-05-20'),
('ATL', 'Hartsfield Jackson Atlanta International Airport', 'Atlanta', 'USA', 1,'2024-05-20')

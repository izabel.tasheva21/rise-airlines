SELECT 
    F.FlightID,
    F.FlightNumber,
    F.DepartureAirport AS DepartureAirportCode,
    D.City AS DepartureCity,
	D.Country AS DepartureCountry,
	F.DepartureDateTime,
    F.ArrivalAirport AS ArrivalAirportCode,
    A.City AS ArrivalCity,
	A.Country AS DepartureCountry,
	F.ArrivalDateTime
FROM 
    dbo.Flight F
JOIN 
    dbo.Airport D ON F.DepartureAirport = D.Code
JOIN 
    dbo.Airport A ON F.ArrivalAirport = A.Code
WHERE
    CAST(F.DepartureDateTime AS DATE) = CAST(DATEADD(DAY, 1, GETDATE()) AS DATE);
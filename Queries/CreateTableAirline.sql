DROP TABLE IF EXISTS dbo.Airline;
GO

CREATE TABLE dbo.Airline (
    AirlineID INT CONSTRAINT PK_Airline PRIMARY KEY NOT NULL,
    Name VARCHAR(100) NOT NULL,
    Founded Date NOT NULL,
    FleetSize INT NOT NULL,
    Description VARCHAR(255) NOT NULL,
);
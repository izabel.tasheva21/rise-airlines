--Create an index on Airline name
GO  
CREATE NONCLUSTERED INDEX IX_Airline_Name  
ON Airline (Name)  
GO
SELECT COUNT(*) AS DepartedFlights
FROM dbo.Flight
WHERE Flight.DepartureDateTime < GETDATE();
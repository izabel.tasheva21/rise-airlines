-- The latest departure time
SELECT MAX(Flight.DepartureDateTime) AS LatestDepartureTime
FROM dbo.Flight

-- The flight with the latest departure time
SELECT FlightNumber
FROM dbo.Flight
WHERE DepartureDateTime = (
    SELECT MAX(DepartureDateTime)
    FROM dbo.Flight
);
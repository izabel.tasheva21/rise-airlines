# RISE-Airlines

**name**: Izabel Tasheva
**email**: izabel.tasheva21@gmail.com

## Project Overview

RISE-Airlines is a comprehensive C# console application designed to manage and display information related to airports, airlines, and flights. This application provides a user-friendly interface for inputting data, ensures the accuracy of the information through validation, and neatly presents the data to the user.

## Features

### Task 1: Airlines Console Application

- **Read Input**: Users can input names of airports, airlines, and flights, which are then stored in separate arrays for efficient data management.
- **Validate Input**: The application checks for valid input, ensuring:
  - Airport names consist of exactly three alphabetic characters.
  - Airline names are less than six characters long.
  - Flight identifiers are alphanumeric strings.
- **Error Handling**: Implements error handling to manage invalid inputs gracefully.
- **Print Data**: Displays the validated data on the console in an easily readable format.
- **Code Organization**: The code is structured into distinct functions for input reading, validation, and output printing, with clear naming conventions and documentation for easy understanding and maintenance.

### Task 2: GitLab Repository and Version Control

- **Repository Creation**: A new GitLab repository named “RISE-Airlines” was established to host the project.
- **Branch Management**: Developed a feature branch named “Task-1-Airlines-Project-Data” to integrate the Airlines console application.
- **Version Control**: Set up a `.gitignore` file to ensure only necessary files are tracked and committed. Commit messages are crafted carefully to convey the purpose of each commit.
- **Merge Requests**: Changes are pushed to the feature branch and merged into the main branch through merge requests, ensuring code review and maintaining code quality.

### Task 3: Project Documentation

- This README file has been updated to provide a detailed description of the project, outlining its purpose, key features, and the methodology behind its development.


### Task 4: Airlines Data Search and Sorting

- **Uniqueness Validation**: Extend the existing validation for Airlines, Airports and Flights to ensure
their values are unique on every category using a linear search.
- **Apply Sorting**:
- Implement bubble sort to sort Airports.
- Use selection sort for sorting Airlines and Flights.
- Display the sorted results after all data is entered​
Implement Search:
- Read user input of a string and use binary search to locate specific input
data for an exact match.
- Print whether the found data is an Airport, Airline, Flight or not found.


### Task 5: Basics of Unit Testing

- **Create a Unit Test Project**: Set up a new unit test project within the Airlines solution, integrating XUnit via NuGet packages.
- **Unit Tests for Validation**: Develop unit tests for Airports, Airlines and Flights, adhering to the Arrange-Act-Assert pattern.
- **Unit Tests for Sorting and Searching Algorithms**:
- Develop unit tests for bubble and selection sort algorithms, adhering to the Arrange-Act-Assert pattern.
- Create unit tests for linear and binary search functions to validate their correctness.
- **Pseudo Performance Test for Sorting Algorithms**:
- Perform a performance test, running bubble and selection sort for one second each.
- Use pseudo-randomly generated data with the same seed for both algorithms.
- Employ the Stopwatch class to measure execution time and print iteration counts.
- **Performance Comparison of Search Algorithms**:
- Conduct a similar performance test to compare binary and linear search functions.
- Evaluate and report the number of searches completed in a fixed time frame.


### Task 6: Integrating Unit Tests into CI Pipeline

- **Update CI Pipeline Configuration**: 
- Set up your the .gitlab-ci.yml file to include steps for running unit tests as part of the CI pipeline and ensure unit tests are automatically executed. 
- Configure the pipeline to generate a coverage report and ensure the pipeline is configured to fail if the test coverage is below the 50% threshold.
- **Check Test Results and Coverage**: Configure the CI pipeline to display test outcomes and coverage reports within the GitLab UI. This includes parsing the Coverlet coverage report to show detailed coverage metrics and ensuring that the pipeline status reflects success or failure based on test results and coverage thresholds.


### Task 7: Refactoring and Static Code Analysis Integration

- **Integrate Static Code Analysis**:
- Add static code analysis to the CI pipeline.
- Review the analysis reports for issues and address critical concerns.
- **Code Style and Refactoring**:
- Review and refactor the project code to improve readability.
- Ensure the code adheres to established C# coding standards and best practices.
- Identify and refactor any code smells in the project.


### Task 8: Implementing OOP Concepts and Console Commands

- **Develop Data and Algorithm Classes**:
- Create a static class with extension methods for sorting algorithms. Add parameters for sorting order (ascending, descending)
- Develop classes to encapsulate input data, including adding, validating, and sorting data in ascending or descending order for airports, airlines, and flights. 
- Use appropriate visibility modifiers to safeguard data integrity.
- **Implement Console Commands for Enhanced Data Interaction**:
- This section modifies the default console-based search behavior from the previous task by introducing specific commands, offering a more structured approach to interacting with data in the Airlines project.


### Task 9: Establishing the Business Layer

- **Create the Airlines.Business Project**:
- Initiate a new project within the solution, named Airlines.Business, to serve exclusively as the business layer. This project will contain all business logic pertaining to airports, airlines, and flights.
- **Encapsulate Business Logic**:
- Transfer all business logic operations—such as validation, searching, and sorting of airports, airlines, and flights—from the existing application to the Airlines.Business project. Ensure this logic is cleanly separated from any input/output handling.
- **Refactor for Business Logic Isolation**:
- Modify the main application project to reference the Airlines.Business project for all business logic operations. This refactoring ensures that the core functionalities are managed within the business layer, emphasizing a clean separation of concerns.
- **Project Structure and Dependency Management**:
- Organize the solution to ensure that the main application and tests projects depend on the Airlines.Business project, but not vice versa. This structure reinforces the independence of the business logic from the presentation and input handling.


### Task 10: Refactoring Data Structures with .NET Collections and LINQ

- **Define Airport and Airline Classes**:
- Create an Airport class with properties: Name, City, and Country.
- Define an Airline class with properties: Name.
- **Read Airports and Airlines Data from Files**:
- Implement functionality to continuously read data from airport and airline files until the end of each file is reached.
- Airport file format CSV: Identifier, Name, City, Country.
- Example: JFK, John F. Kennedy International Airport, New York, USA.
- **Validate Airport properties**:
- Identifier can contain only alphanumeric characters between 2 and 4 alphanumeric characters.
- Name, City, Country can contain only alphabet and space characters.
- **Utilize .NET Collections and LINQ**:
- Refactor the application to use .NET collections and LINQ for efficient and accurate data handling.
- **Implement easy access commands**:
- Implement a command to display if an airline is existing in our project.
- Format: exist <airline name>.
- The command prints true if an airline with a matching name is existing in our project, or false if not.
- The command must be fast. Choose a suitable data structure so it can run with complexity of O(1).
- **Implement a command to display all of the airports in a city or a country**:
- Implement a command to list all airports in a city or a country.
- Format: list <input data> <from>.
- <input data>: Required, the name of a city or a country.
- <from>: Required, specify 'City’ or 'Country’ corresponding to the input data.
- The command must be fast. Choose a suitable data structure so it can run with complexity of O(1).


### Task 11: Flight Route Management

- **Flight Class Definition**:
- Create a Flight class with properties: Identifier, DepartureAirport, and ArrivalAirport.
- **Read Flights Data from File**:
- Load flight data from a file in CSV format, with each line as Identifier, DepartureAirport, ArrivalAirport.
- **Console Commands for Route Management**:
- Command: route new
- Initializes a new flight route.
- Command: route add <Flight Identifier>
- Adds a flight to the end of the route using its mandatory unique identifier, ensuring its logical connection to the route.
- Command: route remove
- Removes the last flight from the route.
- Command: route print
- Removes the last flight from the route.
- **Validate Flight Connection**:
- Ensure new flights logically connect to the route / Ensure the DepartureAirport of the new flight matches the ArrivalAirport of the last flight in the route.
- **Implement Unit Tests**:
- Test each console command, covering normal operation and edge cases.
- Include tests for:
- Successful route creation.
- Adding flights with valid and invalid identifiers.
- Removing flights when the route is empty or has multiple flights.


### Task 12: Efficient Release of File Resources

- **Define Specific Aircraft Types**:
- Cargo Aircraft: Characterized by significant cargo load and volume.
- Passenger Aircraft: Distinguished by passenger seats along with cargo capacity.
- Private Aircraft: Defined primarily by passenger seating, lacking cargo capacity.
- Aircraft definitions include Aircraft Model, and depending on its type, eventually Cargo Weight, Cargo Volume, and/or Seats.
- **Load Aircraft Definitions from File**:
- CSV format, with each line as: Aircraft Model, Cargo Weight, Cargo Volume, Seats. The absence of Cargo Load, Cargo Volume, or Seats helps determine the aircraft type.
- **Update Flight Data with Aircraft Model**:
- Modify the Flight class to include an aircraft model property.
- Revised CSV format: Flight Identifier, Departure Airport, Arrival Airport, Aircraft Model.


### Task 13: Flight Reservation Features

- **Release File Resources After Use**:
- Implement logic to automatically close and release file streams after the input data from files (flights, airports, airlines) has been processed.Use resource management constructs like “.Dispose” and “using” in C#, which ensures that the file resources are properly disposed once the data loading is complete.


### Task 14: Command Batch execution

- **Create Command Interfaces and Classes**:
- Define a common interface for console commands.
- Develop classes for each command type, implementing this interface.
- **Implement Command Pattern**:
- Encapsulate execution logic within each command class, aligning with its specific functionality.
- **Develop a Command Factory**:
- Implement a static factory for parsing console input and creating the appropriate command objects.
- **Integrate Command Execution**:
- Execute commands immediately by default.
- **Introduce a batch command execution feature**:
- Starting a Batch: Typing batch start in the console activates batch mode, where new commands are added to a batch queue instead of being executed immediately.
- Executing the Batch: Typing batch run executes all commands in the batch queue in the order they were added.
- Canceling the Batch: Typing batch cancel clears the batch queue and cancels the execution of queued commands.
- When batch mode is not active (not started or after being canceled), new commands revert to executing immediately upon entry.
- Ensure that the application can seamlessly switch between immediate execution and batch execution modes as per user inputs.
- **Testing Requirements**:
- Immediate Command Execution Tests: Verify immediate execution of console commands.
- Batch Command Execution Tests: Test functionality of batch mode, including adding, executing, and canceling commands.
- Mode Switching Tests: Ensure smooth transition between immediate and batch execution modes.


### Task 15: Advanced Error Handling in the Airlines Project

- **Define Custom Exceptions**:
- Develop custom exception classes tailored to the project's needs, such as InvalidInputException, InvalidFlightCodeException, and InvalidAirlineNameException.
- Ensure each custom exception provides a clear, user-friendly error message detailing the cause of the error.
- Catch and manage exceptions to provide feedback to the user without crashing the application.
- **Enhanced Input Validation with Custom Exceptions**:
- Update input validation for airports, airlines, and flights to utilize custom exceptions when encountering invalid data. For example, throwing an InvalidFlightCodeException if a flight identifier doesn't meet the specified criteria.
- **Error Handling in User Commands**:
- Integrate custom exceptions into the handling of console commands, particularly for operations like searching for a flight or adding new flight data.


### Task 16: Flight Route Finder Implementation

- **Flight Route Tree Construction**:
- Construct a tree data structure with the root representing the start airport.
- Each child node represents a direct flight from the start airport or a flight from one of the parent node's destination airports to another airport.
- **Read Flight Routes Data from File**:
- Implement functionality to read flight route data from a file, loading routes for a single airport.
- **File format**:
- First line: Start Airport.
- Subsequent lines: Flight Identifiers.
- **Implement Flight Route Search Command**:
- Introduce a console command route find <Destination Airport> to search for a flight route.
- Verify if a direct or indirect route to the destination airport exists in the tree.
- If a route exists, display the sequence of flights in the route.


### Task 17: Testing the Flight Route Finder

- **File Reading and Parsing Tests**:
- Verify accurate reading and parsing of flight route data from files.
- Test scenarios with invalid file data that could violate the tree's integrity.
- **Route Search Functionality Tests**:
- Destinations with direct routes.
- Destinations with indirect routes requiring multiple stops.
- Non-existent destinations to ensure graceful error handling.

### Task 18: Flight Network

- **Flight Network Graph Construction**:
- Create a graph data structure where each node represents an airport, and edges represent direct flights between airports.
- Integrate existing flight data from the previous task “Flight Route Management”, representing the entire network into the graph, ensuring accurate representation of connections (edges) and airports (nodes).
- **Implement Flights Graph Analysis**:
- Determine if two airports are connected, either directly or through intermediate stops.
- Find the shortest route between two airports using appropriate algorithms.
- **Flights Exploration Commands**:
- Introduce console commands for network analysis:
- Connectivity check: route check <Start Airport> <End Airport>
- Shortest path: route search <Start Airport> <End Airport>


### Task 19: Enhanced Flight Route Search

- **Include Flight Price and Time**:
- Integrate price and duration into flight.
- **Advanced Route Search Strategies**:
- Implement strategies for route search based on least price, shortest time, and fewest stops.
- Command Format: route search <Start Airport> <End Airport> <Strategy>
- <Strategy> options: 'cheap', 'short', 'stops'.
- **Scalability for Future Expansion**:
- Design the system for easy integration of new search strategies.
- **Update Flight Data File Format**:
- New Format: Flight Identifier, Departure Airport, Arrival Airport, Price, Time in Hours


## Getting Started

To run the RISE-Airlines application, clone the repository, navigate to the project directory, and execute the application:

```bash
git clone https://gitlab.com/izabel.tasheva21/RISE-Airlines.git
cd RISE-Airlines/Airlines.Console
dotnet run





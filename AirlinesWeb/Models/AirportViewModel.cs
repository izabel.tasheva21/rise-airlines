﻿using Airlines.Repository.Dtos;

namespace AirlinesWeb.Models;

public class AirportViewModel
{
    public IEnumerable<AirportDto>? Airports { get; set; }
    public required AirportDto NewAirport { get; set; }
    public string? SearchTerm { get; set; }
    public int PageNumber { get; set; }
}

﻿using Airlines.Repository.Dtos;

namespace AirlinesWeb.Models;

public class AirlineViewModel
{
    public IEnumerable<AirlineDto>? Airlines { get; set; }
    public required AirlineDto NewAirline { get; set; }
    public string? SearchTerm { get; set; }
    public int PageNumber { get; set; }
}

﻿namespace AirlinesWeb.Models;

public class CountViewModel
{
    public int AirportsCount { get; set; }
    public int AirlinesCount { get; set; }
    public int FlightsCount { get; set; }
}
﻿using Airlines.Repository.Dtos;

namespace AirlinesWeb.Models;

public class FlightViewModel
{
    public IEnumerable<FlightDto>? Flights { get; set; }
    public required FlightDto NewFlight { get; set; }
    public string? SearchTerm { get; set; }
    public int PageNumber { get; set; }
}

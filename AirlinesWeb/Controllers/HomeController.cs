using AirlinesWeb.Models;
using Microsoft.AspNetCore.Mvc;
using System.Diagnostics;

namespace AirlinesWeb.Controllers;
public class HomeController(ILogger<HomeController> logger) : Controller
{
    private readonly ILogger<HomeController> _logger = logger;

    public async Task<List<int>> GetDatabaseCount()
    {
        var airportRepository = Airlines.Repository.Configuration.ConfigurationManager.GetAirportRepository();
        var airportDtos = await airportRepository.GetAirportsAsync();
        var airportsCount = airportDtos.Count;

        var airlineRepository = Airlines.Repository.Configuration.ConfigurationManager.GetAirlineRepository();
        var airlineDtos = await airlineRepository.GetAirlinesAsync();
        var airlinesCount = airlineDtos.Count;

        var flightRepository = Airlines.Repository.Configuration.ConfigurationManager.GetFlightRepository();
        var flightDtos = await flightRepository.GetFlightsAsync();
        var flightsCount = flightDtos.Count;

        var databaseCount = new List<int>
        {
            airportsCount,
            airlinesCount,
            flightsCount
        };

        return databaseCount;
    }

    public IActionResult Index()
    {
        var databaseCount = GetDatabaseCount().Result;

        var viewModel = new CountViewModel
        {
            AirportsCount = databaseCount[0],
            AirlinesCount = databaseCount[1],
            FlightsCount = databaseCount[2]
        };

        return View(viewModel);
    }

    public IActionResult Privacy()
    {
        return View();
    }

    [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
    public IActionResult Error()
    {
        return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
    }
}

﻿using Airlines.Repository.Dtos;
using Airlines.Service.Services;
using AirlinesWeb.Models;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;

namespace AirlinesWeb.Controllers
{
    public class AirportsController(IMapper mapper, AirportsService airportsService) : Controller
    {
        private readonly IMapper _mapper = mapper;
        private readonly AirportsService _airportsService = airportsService;
        private readonly int _itemsPerPage = 5;

        private async Task<AirportViewModel> CreateAirportViewModel(int currentPage)
        {

            // Index of the first item on the current page
            var startIndex = (currentPage - 1) * _itemsPerPage;

            var airportsForPage = await _airportsService.GetAirportsPerPageAsync(startIndex, _itemsPerPage);

            return new AirportViewModel
            {
                Airports = airportsForPage,
                // Initialize a new AirportModel for the form
                NewAirport = new AirportDto()
                {
                    Code = "",
                    Name = "",
                    City = "",
                    Country = "",
                    Founded = default,
                    RunwaysCount = 0,
                },
                PageNumber = currentPage
            };
        }

        private async Task<int> GetAirportsCount()
        {
            var airportDtos = await _airportsService.GetAllAirportsAsync();

            return airportDtos.Count;
        }

        private int GetAirportPages()
        {
            var airportsCount = GetAirportsCount().Result;

            if (airportsCount % _itemsPerPage == 0)
            {
                return airportsCount / _itemsPerPage;
            }
            else
            {
                return (airportsCount / _itemsPerPage) + 1;
            }
        }

        public async Task<IActionResult> IndexAsync(int? pageNumber)
        {
            int currentPage;

            if (pageNumber == null)
            {
                currentPage = 1;
            }
            else
            {
                currentPage = (int)pageNumber;
            }

            var viewModel = await CreateAirportViewModel(currentPage);

            TempData["TotalPages"] = GetAirportPages().ToString();

            return View(viewModel);
        }


        // POST: Create a new Airport
        [HttpPost]
        public async Task<IActionResult> Create(AirportViewModel viewModel)
        {
            var firstKey = ModelState.Keys.First();
            ModelState.Remove(firstKey);

            if (ModelState.IsValid)
            {
                _airportsService.AddAirport(viewModel.NewAirport);
                return RedirectToAction(nameof(Index));
            }

            viewModel = await CreateAirportViewModel(1);

            return View("Index", viewModel);
        }

        public async Task<IActionResult> Search(string searchTerm)
        {
            var searchResults = await _airportsService.SearchAirportsAsync(searchTerm);

            var viewModel = new AirportViewModel
            {
                Airports = searchResults,
                NewAirport = new AirportDto()
                {
                    Code = "",
                    Name = "",
                    City = "",
                    Country = "",
                    Founded = default,
                    RunwaysCount = 0,
                }
            };

            return View("Index", viewModel);
        }

        public IActionResult Filter(string filter, string value)
        {
            try
            {
                var filteredAirports = _airportsService.GetAirportsByFilter(filter, value);

                var viewModel = new AirportViewModel
                {
                    Airports = filteredAirports,
                    NewAirport = new AirportDto()
                    {
                        Code = "",
                        Name = "",
                        City = "",
                        Country = "",
                        Founded = default,
                        RunwaysCount = 0,
                    }
                };

                return View("Index", viewModel);
            }
            catch (ArgumentException ex)
            {
                // Invalid filter argument
                ModelState.AddModelError(string.Empty, ex.Message);

                var viewModel = new AirportViewModel
                {
                    Airports = default,
                    // Initialize a new AirportModel for the form
                    NewAirport = new AirportDto()
                    {
                        Code = "",
                        Name = "",
                        City = "",
                        Country = "",
                        Founded = default,
                        RunwaysCount = 0,
                    }
                };

                return View("Index", viewModel);
            }
            catch (Exception ex)
            {
                ModelState.AddModelError(string.Empty, ex.Message);

                var viewModel = new AirportViewModel
                {
                    Airports = default,
                    // Initialize a new AirportModel for the form
                    NewAirport = new AirportDto()
                    {
                        Code = "",
                        Name = "",
                        City = "",
                        Country = "",
                        Founded = default,
                        RunwaysCount = 0,
                    }
                };

                return View("Index", viewModel);
            }
        }
    }
}
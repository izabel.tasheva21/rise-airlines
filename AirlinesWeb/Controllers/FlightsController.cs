﻿using Airlines.Repository.Dtos;
using Airlines.Service.Services;
using AirlinesWeb.Models;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;

namespace AirlinesWeb.Controllers;
public class FlightsController(IMapper mapper, FlightsService flightsService) : Controller
{
    private readonly IMapper _mapper = mapper;
    private readonly FlightsService _flightsService = flightsService;
    private readonly int _itemsPerPage = 5;

    private async Task<FlightViewModel> CreateFlightViewModel(int currentPage)
    {
        // Index of the first item on the current page
        var startIndex = (currentPage - 1) * _itemsPerPage;

        var flightsForPage = await _flightsService.GetFlightsPerPageAsync(startIndex, _itemsPerPage);

        return new FlightViewModel
        {
            Flights = flightsForPage,
            NewFlight = new FlightDto()
            {
                FlightNumber = "",
                DepartureAirport = "",
                ArrivalAirport = "",
                DepartureDateTime = default,
                ArrivalDateTime = default,
            },
            PageNumber = currentPage
        };
    }

    private async Task<int> GetFlightsCount()
    {
        var flightDtos = await _flightsService.GetAllFlightsAsync();

        return flightDtos.Count;
    }

    private int GetFlightPages()
    {
        var flightsCount = GetFlightsCount().Result;

        if (flightsCount % _itemsPerPage == 0)
        {
            return flightsCount / _itemsPerPage;
        }
        else
        {
            return (flightsCount / _itemsPerPage) + 1;
        }
    }

    public async Task<IActionResult> IndexAsync(int? pageNumber)
    {
        int currentPage;

        if (pageNumber == null)
        {
            currentPage = 1;
        }
        else
        {
            currentPage = (int)pageNumber;
        }

        var viewModel = await CreateFlightViewModel(currentPage);

        TempData["TotalPages"] = GetFlightPages().ToString();

        return View(viewModel);
    }

    // POST: Create a new Flight
    [HttpPost]
    public async Task<IActionResult> Create(FlightViewModel viewModel)
    {
        var firstKey = ModelState.Keys.First();
        ModelState.Remove(firstKey);

        if (ModelState.IsValid)
        {
            _flightsService.AddFlight(viewModel.NewFlight);
            return RedirectToAction(nameof(Index));
        }

        viewModel = await CreateFlightViewModel(1);
        return View("Index", viewModel);
    }

    public async Task<IActionResult> Search(string searchTerm)
    {
        var searchResults = await _flightsService.SearchFlightsAsync(searchTerm);
        var viewModel = new FlightViewModel
        {
            Flights = searchResults,
            NewFlight = new FlightDto()
            {
                FlightNumber = "",
                DepartureAirport = "",
                ArrivalAirport = "",
                DepartureDateTime = default,
                ArrivalDateTime = default,
            }
        };

        return View("Index", viewModel);
    }

    public IActionResult Filter(string filter, string value)
    {
        try
        {
            var filteredFlights = _flightsService.GetFlightsByFilter(filter, value);
            var viewModel = new FlightViewModel
            {
                Flights = filteredFlights,
                NewFlight = new FlightDto()
                {
                    FlightNumber = "",
                    DepartureAirport = "",
                    ArrivalAirport = "",
                    DepartureDateTime = default,
                    ArrivalDateTime = default,
                }
            };

            return View("Index", viewModel);
        }
        catch (ArgumentException ex)
        {
            // Invalid filter argument
            ModelState.AddModelError(string.Empty, ex.Message);

            var viewModel = new FlightViewModel
            {
                Flights = default,
                NewFlight = new FlightDto()
                {
                    FlightNumber = "",
                    DepartureAirport = "",
                    ArrivalAirport = "",
                    DepartureDateTime = default,
                    ArrivalDateTime = default,
                }
            };

            return View("Index", viewModel);
        }
        catch (Exception ex)
        {
            ModelState.AddModelError(string.Empty, ex.Message);

            var viewModel = new FlightViewModel
            {
                Flights = default,
                NewFlight = new FlightDto()
                {
                    FlightNumber = "",
                    DepartureAirport = "",
                    ArrivalAirport = "",
                    DepartureDateTime = default,
                    ArrivalDateTime = default,
                }
            };

            return View("Index", viewModel);
        }
    }
}

﻿using Airlines.Repository.Dtos;
using Airlines.Service.Services;
using AirlinesWeb.Models;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;

namespace AirlinesWeb.Controllers;
public class AirlinesController(IMapper mapper, AirlinesService airlinesService) : Controller
{
    private readonly IMapper _mapper = mapper;
    private readonly AirlinesService _airlinesService = airlinesService;
    private readonly int _itemsPerPage = 7;

    private async Task<AirlineViewModel> CreateAirlineViewModel(int currentPage)
    {

        // Index of the first item on the current page
        var startIndex = (currentPage - 1) * _itemsPerPage;

        // Get only the airports for the current page
        var airlinesForPage = await _airlinesService.GetAirlinesPerPageAsync(startIndex, _itemsPerPage);

        return new AirlineViewModel
        {
            Airlines = airlinesForPage,
            NewAirline = new AirlineDto()
            {
                Name = "",
                Founded = default,
                FleetSize = 0,
                Description = "",
            },
            PageNumber = currentPage
        };
    }

    private async Task<int> GetAirlinesCount()
    {
        var airlineDtos = await _airlinesService.GetAllAirlinesAsync();

        return airlineDtos.Count;
    }

    private int GetAirlinesPages()
    {
        var airlinesCount = GetAirlinesCount().Result;

        if (airlinesCount % _itemsPerPage == 0)
        {
            return airlinesCount / _itemsPerPage;
        }
        else
        {
            return (airlinesCount / _itemsPerPage) + 1;
        }
    }

    public async Task<IActionResult> IndexAsync(int? pageNumber)
    {
        int currentPage;

        if (pageNumber == null)
        {
            currentPage = 1;
        }
        else
        {
            currentPage = (int)pageNumber;
        }

        var viewModel = await CreateAirlineViewModel(currentPage);

        TempData["TotalPages"] = GetAirlinesPages().ToString();

        return View(viewModel);
    }

    // POST: Create a new Airline
    [HttpPost]
    public async Task<IActionResult> Create(AirlineViewModel viewModel)
    {
        var firstKey = ModelState.Keys.First();
        ModelState.Remove(firstKey);

        if (ModelState.IsValid)
        {
            _airlinesService.AddAirline(viewModel.NewAirline);
            return RedirectToAction(nameof(Index));
        }

        viewModel = await CreateAirlineViewModel(1);
        return View("Index", viewModel);
    }

    public async Task<IActionResult> Search(string searchTerm)
    {
        var searchResults = await _airlinesService.SearchAirlinesAsync(searchTerm);

        var viewModel = new AirlineViewModel
        {
            Airlines = searchResults,
            NewAirline = new AirlineDto()
            {
                Name = "",
                Founded = default,
                FleetSize = 0,
                Description = "",
            }
        };

        return View("Index", viewModel);
    }

    public IActionResult Filter(string filter, string value)
    {
        try
        {
            var filteredAirlines = _airlinesService.GetAirlinesByFilter(filter, value);

            var viewModel = new AirlineViewModel
            {
                Airlines = filteredAirlines,
                NewAirline = new AirlineDto()
                {
                    Name = "",
                    Founded = default,
                    FleetSize = 0,
                    Description = "",
                }
            };

            return View("Index", viewModel);
        }
        catch (ArgumentException ex)
        {
            // Invalid filter argument
            ModelState.AddModelError(string.Empty, ex.Message);

            var viewModel = new AirlineViewModel
            {
                Airlines = default,
                NewAirline = new AirlineDto()
                {
                    Name = "",
                    Founded = default,
                    FleetSize = 0,
                    Description = "",
                }
            };

            return View("Index", viewModel);
        }
        catch (Exception ex)
        {
            ModelState.AddModelError(string.Empty, ex.Message);

            var viewModel = new AirlineViewModel
            {
                Airlines = default,
                NewAirline = new AirlineDto()
                {
                    Name = "",
                    Founded = default,
                    FleetSize = 0,
                    Description = "",
                }
            };

            return View("Index", viewModel);
        }
    }
}

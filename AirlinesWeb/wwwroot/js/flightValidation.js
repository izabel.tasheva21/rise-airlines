﻿function validateFlightAndEnableButton() {
    // Get the value of the input field with id="flight-flightNumber"
    let flightNumber = document.getElementById("flight-flightNumber").value;

    // Get the value of the input field with id="flight-departureAirport"
    let departureAirport = document.getElementById("flight-departureAirport").value;

    // Get the value of the input field with id="flight-arrivalAirport"
    let arrivalAirport = document.getElementById("flight-arrivalAirport").value;

    // Get the value of the input field with id="flight-departureDateTime"
    let departureDateTime = document.getElementById("flight-departureDateTime").value;

    // Get the value of the input field with id="flight-arrivalDateTime"
    let arrivalDateTime = document.getElementById("flight-arrivalDateTime").value;

    // Disable submit button by default
    let submitButton = document.getElementById("submit-button");
    submitButton.disabled = true;

    // Array to hold error messages
    let errorMessages = [];

    // Validate flightNumber
    if (!flightNumber.trim()) {
        errorMessages.push("Flight number field cannot be empty!");
    }

    // Validate departureAirport
    if (!departureAirport.trim()) {
        errorMessages.push("Departure airport field cannot be empty!");
    }

    // Validate arrivalAirport
    if (!arrivalAirport.trim()) {
        errorMessages.push("Arrival airport field cannot be empty!");
    }

    // Validate departureDateTime
    if (!departureDateTime.trim()) {
        errorMessages.push("Departure time field cannot be empty!");
    }

    // Validate arrivalDateTime
    if (!arrivalDateTime.trim()) {
        errorMessages.push("Arrival time field cannot be empty!");
    }

    // Display error messages
    let demo = document.getElementById("demo");
    if (errorMessages.length > 0) {
        let text = "<ul>";
        errorMessages.forEach(myFunc);
        text += "</ul>";
        demo.innerHTML = text;

        function myFunc(value) {
            text += "<li>" + value + "</li>";
        }
    } else {
        demo.innerHTML = ""; // Clear error messages
        submitButton.disabled = false; // Enable submit button if no errors
    }
}

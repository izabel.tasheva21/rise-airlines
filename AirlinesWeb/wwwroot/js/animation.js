﻿// Function to handle scrolling behavior
function scrollFunction() {
    if (document.body.scrollTop > 300 || document.documentElement.scrollTop > 300) {
        document.getElementById("go-up-button").style.display = "block";
    } else {
        document.getElementById("go-up-button").style.display = "none";
    }
}

// Add event listener for the "go up" button
document.addEventListener("DOMContentLoaded", function () {
    // Add event listener for the "go up" button
    document.getElementById("go-up-button").addEventListener("click", function () {
        // Scroll to the top of the page smoothly
        window.scrollTo({
            top: 0,
            behavior: "smooth"
        });
    });
});

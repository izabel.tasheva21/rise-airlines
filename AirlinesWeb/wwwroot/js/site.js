﻿// Togle view button - Show/hide table
document.addEventListener("DOMContentLoaded", function () {
    var table = document.getElementById("table-form");
    var isVisible = true;

    // Show the table by default
    table.style.display = "flex";

    // Add click event listener to show/hide the table
    var button = document.getElementById("showHideTableButton");
    button.addEventListener("click", function () {
        if (isVisible) {
            table.style.display = "none"; // Hide the table
            isVisible = false; // Update visibility state
        } else {
            table.style.display = "flex"; // Show the table
            isVisible = true; // Update visibility state
        }
    });
});

function CheckItemsCount() {
    var table = document.getElementById("table");
    var tableElementsCount = table.rows.length;

    // If there are more than 3 rows in the table
    if (tableElementsCount > 3) {
        // Create a button to show/hide additional rows
        var button = document.createElement("button");
        button.textContent = "Show only 3 items";

        // Add a class to the button
        button.classList.add("element-3-button");

        button.onclick = function () {
            // Loop through rows starting from index 3
            for (var i = 4; i < table.rows.length; i++) {
                // Toggle the display style of each row
                table.rows[i].style.display = table.rows[i].style.display === "none" ? "" : "none";
            }
        };
        // Append the button to the page
        var pageContent = document.querySelector(".table-form");
        pageContent.appendChild(button);
    }
}

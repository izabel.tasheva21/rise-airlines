﻿function validateAirlineAndEnableButton() {
    // Get the value of the input field with id="airline-name"
    let name = document.getElementById("airline-name").value;

    // Get the value of the input field with id="airline-founded"
    let founded = document.getElementById("airline-founded").value;

    // Get the value of the input field with id="airline-fleetSize"
    let fleetSize = document.getElementById("airline-fleetSize").value;

    // Get the value of the input field with id="airline-description"
    let description = document.getElementById("airline-description").value;

    // Disable submit button by default
    let submitButton = document.getElementById("submit-button");
    submitButton.disabled = true;

    // Array to hold error messages
    let errorMessages = [];

    // Validate name
    if (!name.trim()) {
        errorMessages.push("Name field cannot be empty!");
    }

    // Validate founded
    if (!founded.trim()) {
        errorMessages.push("Founded field cannot be empty!"); s
    }

    // Validate fleetSize
    if (!fleetSize.trim() || parseInt(fleetSize) === 0) {
        errorMessages.push("Fleet Size field cannot be empty or zero!");
    }

    // Validate description
    if (!description.trim()) {
        errorMessages.push("Description field cannot be empty!");
    }

    // Display error messages
    let demo = document.getElementById("demo");
    if (errorMessages.length > 0) {
        let text = "<ul>";
        errorMessages.forEach(myFunc);
        text += "</ul>";
        demo.innerHTML = text;

        function myFunc(value) {
            text += "<li>" + value + "</li>";
        }
    } else {
        demo.innerHTML = ""; // Clear error messages
        submitButton.disabled = false; // Enable submit button if no errors
    }
}
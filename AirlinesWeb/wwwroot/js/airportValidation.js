﻿function validateAirportAndEnableButton() {
    // Get the value of the input field with id="airport-code"
    let code = document.getElementById("airport-code").value;

    // Get the value of the input field with id="airport-name"
    let name = document.getElementById("airport-name").value;

    // Get the value of the input field with id="airport-country"
    let country = document.getElementById("airport-country").value;

    // Get the value of the input field with id="airport-city"
    let city = document.getElementById("airport-city").value;

    // Get the value of the input field with id="airport-runwaysCount"
    let runwaysCount = document.getElementById("airport-runwaysCount").value;

    // Get the value of the input field with id="airport-founded"
    let founded = document.getElementById("airport-founded").value;

    // Disable submit button by default
    let submitButton = document.getElementById("submit-button");
    submitButton.disabled = true;

    // Array to hold error messages
    let errorMessages = [];

    // Validate code
    if (!code.trim()) {
        errorMessages.push("Code field cannot be empty!");
    }

    if (code.length !== 3) {
        errorMessages.push("Code should be exactly 3 letters long!");
    }

    // Validate name
    if (!name.trim()) {
        errorMessages.push("Name field cannot be empty!");
    }

    // Validate country
    if (!country.trim()) {
        errorMessages.push("Country field cannot be empty!");
    }

    // Validate city
    if (!city.trim()) {
        errorMessages.push("City field cannot be empty!");
    }

    // Validate runwaysCount
    if (!runwaysCount.trim() || parseInt(runwaysCount) === 0) {
        errorMessages.push("Runways count field cannot be empty or zero!");
    }

    // Validate founded
    if (!founded.trim()) {
        errorMessages.push("Founded field cannot be empty!");
    }

    // Display error messages
    let demo = document.getElementById("demo");
    if (errorMessages.length > 0) {
        let text = "<ul>";
        errorMessages.forEach(myFunc);
        text += "</ul>";
        demo.innerHTML = text;

        function myFunc(value) {
            text += "<li>" + value + "</li>";
        }
    } else {
        demo.innerHTML = ""; // Clear error messages
        submitButton.disabled = false; // Enable submit button if no errors
    }
}

// This file is used by Code Analysis to maintain SuppressMessage
// attributes that are applied to this project.
// Project-level suppressions either have no target or are given
// a specific target and scoped to a namespace, type, member, etc.

using Microsoft.EntityFrameworkCore;
using Airlines.Repository.Entities;
using Airlines.Repository.Repository;
using Airlines.Service.Services;
using Airlines.Repository.Profiles;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
builder.Services.AddControllersWithViews();

// Call ConfigureServices to register services
ConfigureServices(builder.Services);

var app = builder.Build();

// Configure the HTTP request pipeline.
if (!app.Environment.IsDevelopment())
{
    app.UseExceptionHandler("/Home/Error");
    // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
    app.UseHsts();
}

app.UseHttpsRedirection();
app.UseStaticFiles();

app.UseRouting();

app.UseAuthorization();

app.MapControllerRoute(
    name: "default",
    pattern: "{controller=Home}/{action=Index}/{id?}");

app.Run();

// Method to configure dependency injection and register services
void ConfigureServices(IServiceCollection services)
{
    services.AddDbContext<AirlinesContext>(options =>
        options.UseSqlServer(builder.Configuration.GetConnectionString("Local")));

    // Add services
    services.AddScoped<FlightsService>();
    services.AddScoped<IFlightRepository, FlightRepository>();
    services.AddScoped<AirlinesService>();
    services.AddScoped<IAirlineRepository, AirlineRepository>();
    services.AddScoped<AirportsService>();
    services.AddScoped<IAirportRepository, AirportRepository>();

    // Add AutoMapper profiles
    services.AddAutoMapper(typeof(AirlineProfile));

}

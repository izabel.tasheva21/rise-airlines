﻿using Airlines.Repository.Repository;
using Microsoft.Extensions.DependencyInjection;
using Airlines.Repository.Entities;
using Microsoft.EntityFrameworkCore;
using Airlines.Repository.Profiles;

namespace Airlines.Repository.Configuration;
internal class ServiceProviderFactory
{
    private static IServiceProvider? _serviceProvider;

    public static IServiceProvider GetServiceProvider()
    {
        if (_serviceProvider == null)
        {
            // Create service collection
            var services = new ServiceCollection();

            // Add services
            services.AddScoped<IFlightRepository, FlightRepository>();
            services.AddScoped<IAirlineRepository, AirlineRepository>();
            services.AddScoped<IAirportRepository, AirportRepository>();

            services.AddDbContext<AirlinesContext>(options =>
                options.UseSqlServer(ConfigurationManager.GetConnectionString("Local")));

            services.AddAutoMapper(typeof(AirlineProfile));
            services.AddAutoMapper(typeof(AirportProfile));
            services.AddAutoMapper(typeof(FlightProfile));

            // Build service provider
            _serviceProvider = services.BuildServiceProvider();
        }

        return _serviceProvider;
    }
}

﻿using Airlines.Repository.Repository;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Airlines.Repository.Configuration;
public class ConfigurationManager
{
    private static readonly IConfigurationRoot _configuration;

    // Build configuration once
    static ConfigurationManager()
    {
        _configuration = new ConfigurationBuilder()
            .AddJsonFile("appsettings.json")
            .Build();
    }

    // Get connection string from configuration
    public static string GetConnectionString(string name)
    {
        var connectionString = _configuration.GetConnectionString(name);

        if (connectionString == null)
        {
            throw new Exception("Invalid connection string path.");
        }

        return connectionString;
    }

    // Get Airline Repository
    public static IAirlineRepository GetAirlineRepository()
    {

        // Get service provider
        var serviceProvider = ServiceProviderFactory.GetServiceProvider();

        // Resolve repository
        var repository = serviceProvider.GetRequiredService<IAirlineRepository>();

        return repository;
    }

    // Get Airport Repository
    public static IAirportRepository GetAirportRepository()
    {

        // Get service provider
        var serviceProvider = ServiceProviderFactory.GetServiceProvider();

        // Resolve repository
        var repository = serviceProvider.GetRequiredService<IAirportRepository>();

        return repository;
    }

    // Get Flight Repository
    public static IFlightRepository GetFlightRepository()
    {

        // Get service provider
        var serviceProvider = ServiceProviderFactory.GetServiceProvider();

        // Resolve repository
        var repository = serviceProvider.GetRequiredService<IFlightRepository>();

        return repository;
    }
}

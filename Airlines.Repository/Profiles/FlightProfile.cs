﻿using Airlines.Repository.Dtos;
using Airlines.Repository.Entities;
using AutoMapper;

namespace Airlines.Repository.Profiles;
internal class FlightProfile : Profile
{
    public FlightProfile()
    {
        CreateMap<FlightDto, Flight>().ReverseMap();
    }
}

﻿using Airlines.Repository.Dtos;
using Airlines.Repository.Entities;
using AutoMapper;

namespace Airlines.Repository.Profiles;
public class AirlineProfile : Profile
{
    public AirlineProfile()
    {
        CreateMap<AirlineDto, Airline>().ReverseMap();
    }
}

﻿using Airlines.Repository.Dtos;
using Airlines.Repository.Entities;
using AutoMapper;

namespace Airlines.Repository.Profiles;
internal class AirportProfile : Profile
{
    public AirportProfile()
    {
        CreateMap<AirportDto, Airport>().ReverseMap();
    }
}

﻿using System.ComponentModel.DataAnnotations;

namespace Airlines.Repository.Dtos;
public class AirportDto
{
    // Variables 

    [Required(ErrorMessage = "The Code field is required.")]
    public required string Code { get; set; }
    public int AirportID { get; set; }

    [Required(ErrorMessage = "The Name field is required.")]
    public required string Name { get; set; }

    [Required(ErrorMessage = "The Country field is required.")]
    public required string Country { get; set; }

    [Required(ErrorMessage = "The City field is required.")]
    public required string City { get; set; }

    [Required(ErrorMessage = "The Runways Count field is required.")]
    public required int RunwaysCount { get; set; }

    [Required(ErrorMessage = "The Founded field is required.")]
    public required DateOnly Founded { get; set; }
}

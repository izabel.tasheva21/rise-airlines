﻿using System.ComponentModel.DataAnnotations;

namespace Airlines.Repository.Dtos;
public class FlightDto
{
    [Required(ErrorMessage = "The Flight Number field is required.")]
    public required string FlightNumber { get; set; }

    public int FlightID { get; set; }

    [Required(ErrorMessage = "The Departure airport field is required.")]
    public required string DepartureAirport { get; set; }

    [Required(ErrorMessage = "The Arrival airport field is required.")]
    public required string ArrivalAirport { get; set; }

    [Required(ErrorMessage = "The Departure date time field is required.")]
    public required DateTime DepartureDateTime { get; set; }

    [Required(ErrorMessage = "The Arrival date time field is required.")]
    public required DateTime ArrivalDateTime { get; set; }
}

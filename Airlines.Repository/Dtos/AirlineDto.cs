﻿using System.ComponentModel.DataAnnotations;

namespace Airlines.Repository.Dtos;
public class AirlineDto
{
    // Variables 
    public int AirlineID { get; set; }

    [Required(ErrorMessage = "The Name field is required.")]
    public required string Name { get; set; }

    [Required(ErrorMessage = "The Founded field is required.")]
    public required DateOnly Founded { get; set; }

    [Required(ErrorMessage = "The Fleet size field is required.")]
    public required int FleetSize { get; set; }

    [Required(ErrorMessage = "The Description field is required.")]
    public required string Description { get; set; }
}

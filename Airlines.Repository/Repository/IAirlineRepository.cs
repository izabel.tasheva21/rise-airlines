﻿using Airlines.Repository.Entities;
using Airlines.Repository.Dtos;

namespace Airlines.Repository.Repository
{
    public interface IAirlineRepository
    {
        public AirlineDto GetAirlineById(int airlineId);
        // Return all the airlines in the db
        Task<List<AirlineDto>> GetAirlinesAsync();

        // Return the airlines by search in their Name
        Task<List<AirlineDto>> SearchAirlinesAsync(string searchTerm);

        // Return the airlines by filter as a parameter and value of containing filter
        List<AirlineDto> GetAirlinesByFilter(string filter, string value);

        // Return the airline with the max fleet size
        Airline? GetAirlineMaxFleetSize();

        // Return the airline with the min fleet size
        Airline? GetAirlineMinFleetSize();

        // Add airline object to the db
        void AddAirline(AirlineDto airlineDto);

        // Update the airline object with the same key in the db
        void UpdateAirline(AirlineDto updatedAirlineDto);

        // Update the airline object fleetSize with the same key in the db
        void UpdateAirlineFleetSize(string id, int fleetSize);

        // Delete the airline with the same key from the db
        void DeleteAirline(int id);
    }
}

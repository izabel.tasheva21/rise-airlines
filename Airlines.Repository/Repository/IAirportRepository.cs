﻿using Airlines.Repository.Entities;
using Airlines.Repository.Dtos;

namespace Airlines.Repository.Repository
{
    public interface IAirportRepository
    {
        // Return all the airports in the db
        Task<List<AirportDto>> GetAirportsAsync();

        // Return the airports by search in their Code or Name
        Task<List<AirportDto>> SearchAirportsAsync(string searchTerm);

        // Return the airports by filter as a parameter and value of containing filter
        List<AirportDto> GetAirportsByFilter(string filter, string value);

        // Return the airport with the max runways count
        Airport? GetAirportMaxRunwaysCount();

        // Return the airline with the min runways count
        Airport? GetAirportMinRunwaysCount();

        // Add airport object to the db
        void AddAirport(AirportDto airportDto);

        // Update the airport object with the same key in the db
        void UpdateAirport(AirportDto updatedAirportDto);

        // Update the airport object runtimeCount with the same key in the db
        void UpdateAirportRunwaysCount(string code, int runwayCount);

        // Delete the airport with the same key from the db
        void DeleteAirport(string code);
    }
}

﻿using Airlines.Repository.Entities;
using Airlines.Repository.Dtos;

namespace Airlines.Repository.Repository
{
    public interface IFlightRepository
    {
        // Return all the flights in the db
        Task<List<FlightDto>> GetFlightsAsync();

        // Return the flights by search in their FlightNumber, DepartureAirport or ArrivalAirport
        Task<List<FlightDto>> SearchFlightsAsync(string searchTerm);

        // Return all the flights by filter as a parameter and value of containing filter
        List<FlightDto> GetFlightsByFilter(string filter, string value);

        // Return the flights in the db with this Departure Airport and earliest Departure Time
        Flight? GetEarliestFlightDepartureAirport(string location);

        // Return the flights in the db with this Departure Airport and latest Departure Time
        Flight? GetLatestFlightDepartureAirport(string location);

        // Return the flights in the db with this Arrival Airport and earliest Departure Time
        Flight? GetEarliestFlightArrivalAirport(string location);

        // Return the flights in the db with this Arrival Airport and latest Departure Time
        Flight? GetLatestFlightArrivalAirport(string location);

        // Return all the flights in the db with this Departure Airport
        List<Flight> GetFlightsDepartureAirport(string location);

        // Return all the flights in the db with this Arrival Airport
        List<Flight> GetFlightsArrivalAirport(string location);

        // Add flight object to the db
        void AddFlight(FlightDto flightDto);

        // Update the flight object with the same key in the db
        void UpdateFlight(FlightDto updatedFlightDto);

        // Update the flight object Departure Time with the same key in the db
        void UpdateFlightDepartureTime(string id, DateTime time);

        // Update the flight object Arrival Time with the same key in the db
        void UpdateFlightArrivalTime(string id, DateTime time);

        // Delete the flight with the same key from the db
        void DeleteFlight(string flightNumber);
    }
}

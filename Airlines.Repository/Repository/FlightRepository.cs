﻿using Microsoft.EntityFrameworkCore;
using Airlines.Repository.Entities;
using Airlines.Repository.Dtos;
using AutoMapper;

namespace Airlines.Repository.Repository
{
    public class FlightRepository(AirlinesContext context, IMapper mapper) : IFlightRepository
    {
        private readonly AirlinesContext _context = context;
        private readonly IMapper _mapper = mapper;

        public static void Dispose()
        {
        }

        // Return all the flights in the db
        public async Task<List<FlightDto>> GetFlightsAsync()
        {
            var flights = await _context.Flights.ToListAsync();
            return _mapper.Map<List<FlightDto>>(flights);
        }

        // Return the flights by search in their FlightNumber, DepartureAirport or ArrivalAirport
        public async Task<List<FlightDto>> SearchFlightsAsync(string searchTerm)
        {
            var flights = await _context.Flights
                .Where(a => a.FlightNumber.Contains(searchTerm)
                        || a.DepartureAirport.Contains(searchTerm)
                        || a.ArrivalAirport.Contains(searchTerm))
                .ToListAsync();

            return _mapper.Map<List<FlightDto>>(flights);
        }

        // Return all the flights with filter which is some property of flight and value of that property
        public List<FlightDto> GetFlightsByFilter(string filter, string value)
        {
            // Check if the filter exists in the Flight entity
            var propertyFound = typeof(Flight).GetProperty(filter);
            if (propertyFound == null)
            {
                throw new ArgumentException($"Invalid filter: {filter}. This property does not exist in the Flight entity.");
            }

            var result = _context.Flights.Where(flight => EF.Property<string>(flight, filter) == value);

            // Check if the value does not match any records
            if (result.FirstOrDefault() == null)
            {
                throw new Exception($"No flights found with {filter} equal to {value}.");
            }

            return _mapper.Map<List<FlightDto>>(result.ToList());
        }

        // Return the flights in the db with this Departure Airport and earliest Departure Time
        public Flight? GetEarliestFlightDepartureAirport(string location)
        {
            var query = from flight in _context.Flights
                        where flight.DepartureAirport == location
                        orderby flight.DepartureDateTime ascending
                        select new Flight
                        {
                            FlightNumber = flight.FlightNumber,
                            DepartureAirport = flight.DepartureAirport,
                            ArrivalAirport = flight.ArrivalAirport,
                            DepartureDateTime = flight.DepartureDateTime,
                            ArrivalDateTime = flight.ArrivalDateTime
                        };

            // Check if there are any flights at all
            if (query.FirstOrDefault() == null)
            {
                throw new Exception($"No flights found with this departure airport: {location}.");
            }

            return query.FirstOrDefault();
        }

        // Return the flights in the db with this Departure Airport and latest Departure Time
        public Flight? GetLatestFlightDepartureAirport(string location)
        {
            var query = from flight in _context.Flights
                        where flight.DepartureAirport == location
                        orderby flight.DepartureDateTime descending
                        select new Flight
                        {
                            FlightNumber = flight.FlightNumber,
                            DepartureAirport = flight.DepartureAirport,
                            ArrivalAirport = flight.ArrivalAirport,
                            DepartureDateTime = flight.DepartureDateTime,
                            ArrivalDateTime = flight.ArrivalDateTime
                        };

            // Check if there are any flights at all
            if (query.FirstOrDefault() == null)
            {
                throw new Exception($"No flights found with this departure airport: {location}.");
            }

            return query.FirstOrDefault();
        }

        // Return the flights in the db with this Arrival Airport and earliest Departure Time
        public Flight? GetEarliestFlightArrivalAirport(string location)
        {
            var query = from flight in _context.Flights
                        where flight.ArrivalAirport == location
                        orderby flight.DepartureDateTime ascending
                        select new Flight
                        {
                            FlightNumber = flight.FlightNumber,
                            DepartureAirport = flight.DepartureAirport,
                            ArrivalAirport = flight.ArrivalAirport,
                            DepartureDateTime = flight.DepartureDateTime,
                            ArrivalDateTime = flight.ArrivalDateTime
                        };

            // Check if there are any flights at all
            if (query.FirstOrDefault() == null)
            {
                throw new Exception($"No flights found with this arrival airport: {location}.");
            }

            return query.FirstOrDefault();
        }

        // Return the flights in the db with this Arrival Airport and latest Departure Time
        public Flight? GetLatestFlightArrivalAirport(string location)
        {
            var query = from flight in _context.Flights
                        where flight.ArrivalAirport == location
                        orderby flight.DepartureDateTime descending
                        select new Flight
                        {
                            FlightNumber = flight.FlightNumber,
                            DepartureAirport = flight.DepartureAirport,
                            ArrivalAirport = flight.ArrivalAirport,
                            DepartureDateTime = flight.DepartureDateTime,
                            ArrivalDateTime = flight.ArrivalDateTime
                        };

            // Check if there are any flights at all
            if (query.FirstOrDefault() == null)
            {
                throw new Exception($"No flights found with this arrival airport: {location}.");
            }

            return query.FirstOrDefault();
        }
        // Return all the flights in the db with this Departure Airport
        public List<Flight> GetFlightsDepartureAirport(string location)
        {
            var query = _context.Airports.Where(airport => airport.Name == location)
                .Include(airport => airport.FlightDepartureAirportNavigations)
                .SelectMany(airport => airport.FlightDepartureAirportNavigations)
                .Select(flight => new Flight
                {
                    FlightNumber = flight.FlightNumber,
                    DepartureAirport = flight.DepartureAirport,
                    ArrivalAirport = flight.ArrivalAirport,
                    DepartureDateTime = flight.DepartureDateTime,
                    ArrivalDateTime = flight.ArrivalDateTime
                });

            // Check if there are any flights at all
            if (query.FirstOrDefault() == null)
            {
                throw new Exception($"No flights found with this departure airport: {location}.");
            }

            return query.ToList();
        }

        // Return all the flights in the db with this Arrival Airport
        public List<Flight> GetFlightsArrivalAirport(string location)
        {
            var query = _context.Airports.Where(airport => airport.Name == location)
                .Include(airport => airport.FlightArrivalAirportNavigations)
                .SelectMany(airport => airport.FlightArrivalAirportNavigations)
                .Select(flight => new Flight
                {
                    FlightNumber = flight.FlightNumber,
                    DepartureAirport = flight.DepartureAirport,
                    ArrivalAirport = flight.ArrivalAirport,
                    DepartureDateTime = flight.DepartureDateTime,
                    ArrivalDateTime = flight.ArrivalDateTime
                });

            // Check if there are any flights at all
            if (query.FirstOrDefault() == null)
            {
                throw new Exception($"No flights found with this arrival airport: {location}.");
            }

            return query.ToList();
        }

        // Add flight object to the db
        public void AddFlight(FlightDto flightDto)
        {
            try
            {
                var flight = _mapper.Map<Flight>(flightDto);

                _context.Flights.Add(flight);
                _context.SaveChanges();
            }
            catch (DbUpdateException ex)
            {
                // Database update exception
                Console.WriteLine($"Error adding flight: {ex.Message}");
            }
        }

        // Update the flight object with the same key in the db
        public void UpdateFlight(FlightDto updatedFlightDto)
        {
            try
            {
                var updatedFlight = _mapper.Map<Flight>(updatedFlightDto);

                var existingFlight = _context.Flights.FirstOrDefault(a => a.FlightNumber == updatedFlight.FlightNumber);

                if (existingFlight != null)
                {
                    existingFlight.DepartureAirport = updatedFlight.DepartureAirport;
                    existingFlight.ArrivalAirport = updatedFlight.ArrivalAirport;
                    existingFlight.DepartureDateTime = updatedFlight.DepartureDateTime;
                    existingFlight.ArrivalDateTime = updatedFlight.ArrivalDateTime;

                    _context.SaveChanges();
                }
                else
                {
                    Console.WriteLine($"Flight with code {updatedFlightDto.FlightNumber} not found.");
                }
            }
            catch (DbUpdateException ex)
            {
                // Database update exception
                Console.WriteLine($"Error adding flight {updatedFlightDto.FlightID}: {ex.Message}");
            }
        }

        // Update the flight object Departure Time with the same key in the db
        public void UpdateFlightDepartureTime(string id, DateTime time)
        {
            var flight = _context.Flights.FirstOrDefault(flight => flight.FlightNumber == id);

            if (flight == null)
            {
                throw new ArgumentException($"Invalid id: {id}. No flight found with id equal to {id}.");
            }

            try
            {
                flight.DepartureDateTime = time;
                _context.SaveChanges();
            }
            catch (DbUpdateException ex)
            {
                // Database update exception
                Console.WriteLine($"Error updating flight {id}: {ex.Message}");
            }
        }

        // Update the flight object Arrival Time with the same key in the db
        public void UpdateFlightArrivalTime(string id, DateTime time)
        {
            var flight = _context.Flights.FirstOrDefault(flight => flight.FlightNumber == id);

            if (flight == null)
            {
                throw new ArgumentException($"Invalid id: {id}. No flight found with id equal to {id}.");
            }

            try
            {
                flight.ArrivalDateTime = time;
                _context.SaveChanges();
            }
            catch (DbUpdateException ex)
            {
                // Database update exception
                Console.WriteLine($"Error updating flight {id}: {ex.Message}");
            }
        }

        // Delete the flight with the same key from the db
        public void DeleteFlight(string flightNumber)
        {
            var flight = _context.Flights.FirstOrDefault(flight => flight.FlightNumber == flightNumber);

            if (flight == null)
            {
                throw new ArgumentException($"Invalid flight number: {flightNumber}. No flight found with flight number equal to {flightNumber}.");
            }

            try
            {
                _context.Flights.Remove(flight);
                _context.SaveChanges();
            }
            catch (DbUpdateException ex)
            {
                // Database update exception
                Console.WriteLine($"Error deleting flight {flightNumber}: {ex.Message}");
            }
        }
    }
}

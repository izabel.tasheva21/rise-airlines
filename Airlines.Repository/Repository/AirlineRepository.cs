﻿using Microsoft.EntityFrameworkCore;
using Airlines.Repository.Entities;
using AutoMapper;
using Airlines.Repository.Dtos;

namespace Airlines.Repository.Repository
{
    public class AirlineRepository(AirlinesContext context, IMapper mapper) : IAirlineRepository
    {
        private readonly AirlinesContext _context = context;
        private readonly IMapper _mapper = mapper;

        public static void Dispose()
        {
        }

        public AirlineDto GetAirlineById(int airlineId)
        {
            var airline = _context.Airlines.FirstOrDefault(a => a.AirlineID == airlineId);

            return _mapper.Map<AirlineDto>(airline);
        }

        // Return all the airlines in the db
        public async Task<List<AirlineDto>> GetAirlinesAsync()
        {
            var airlines = await _context.Airlines.ToListAsync();
            return _mapper.Map<List<AirlineDto>>(airlines);
        }

        // Return the airlines by search in their Name
        public async Task<List<AirlineDto>> SearchAirlinesAsync(string searchTerm)
        {
            var airlines = await _context.Airlines
                .Where(a => a.Name.Contains(searchTerm))
                .ToListAsync();

            return _mapper.Map<List<AirlineDto>>(airlines);
        }

        // Return the airlines with filter which is some property of airline and value of that property
        public List<AirlineDto> GetAirlinesByFilter(string filter, string value)
        {
            // Check if the filter exists in the Airline entity
            var propertyFound = typeof(Airline).GetProperty(filter);
            if (propertyFound == null)
            {
                throw new ArgumentException($"Invalid filter: {filter}. This property does not exist in the Airline entity.");
            }

            var result = _context.Airlines.Where(airline => EF.Property<string>(airline, filter) == value);

            // Check if the value does not match any records
            if (result.FirstOrDefault() == null)
            {
                throw new Exception($"No airlines found with {filter} equal to {value}.");
            }

            return _mapper.Map<List<AirlineDto>>(result.ToList()); ;
        }

        // Return the airline with the max fleet size
        public Airline? GetAirlineMaxFleetSize()
        {
            var query = from airline in _context.Airlines
                        orderby airline.FleetSize descending
                        select new Airline
                        {
                            AirlineID = airline.AirlineID,
                            Name = airline.Name,
                            Founded = airline.Founded,
                            FleetSize = airline.FleetSize,
                            Description = airline.Description
                        };

            // Check if there are any airlines at all
            if (query.FirstOrDefault() == null)
            {
                throw new Exception($"No airlines found.");
            }

            return query.FirstOrDefault();
        }

        // Return the airline with the min fleet size
        public Airline? GetAirlineMinFleetSize()
        {
            var query = from airline in _context.Airlines
                        orderby airline.FleetSize ascending
                        select new Airline
                        {
                            AirlineID = airline.AirlineID,
                            Name = airline.Name,
                            Founded = airline.Founded,
                            FleetSize = airline.FleetSize,
                            Description = airline.Description
                        };

            // Check if there are any airlines at all
            if (query.FirstOrDefault() == null)
            {
                throw new Exception($"No airlines found.");
            }

            return query.FirstOrDefault();
        }

        // Add airline object to the db
        public void AddAirline(AirlineDto airlineDto)
        {
            try
            {
                var airline = _mapper.Map<Airline>(airlineDto);

                // Generate a unique AirlineID
                var newAirlineID = GetNextAirlineID();

                // Assign the unique AirlineID to the new entity
                airline.AirlineID = newAirlineID;

                _context.Airlines.Add(airline);
                _context.SaveChanges();
            }
            catch (DbUpdateException ex)
            {
                // Database update exception
                Console.WriteLine($"Error adding airline: {ex.Message}");
            }
        }

        // Update the airline object with the same key in the db
        public void UpdateAirline(AirlineDto updatedAirlineDto)
        {
            try
            {
                var updatedAirline = _mapper.Map<Airline>(updatedAirlineDto);

                var existingAirline = _context.Airlines.FirstOrDefault(a => a.AirlineID == updatedAirline.AirlineID);

                if (existingAirline != null)
                {
                    existingAirline.Name = updatedAirline.Name;
                    existingAirline.Founded = updatedAirline.Founded;
                    existingAirline.FleetSize = updatedAirline.FleetSize;
                    existingAirline.Description = updatedAirline.Description;

                    _context.SaveChanges();
                }
                else
                {
                    Console.WriteLine($"Airline with id {updatedAirlineDto.AirlineID} not found.");
                }
            }
            catch (DbUpdateException ex)
            {
                // Database update exception
                Console.WriteLine($"Error adding airline {updatedAirlineDto.AirlineID}: {ex.Message}");
                throw;
            }
        }

        // Update the airline object fleetSize with the same key in the db
        public void UpdateAirlineFleetSize(string id, int fleetSize)
        {
            if (!int.TryParse(id, out var airlineId))
            {
                throw new ArgumentException($"Invalid id: {id}. The id must be a valid integer.");
            }

            var airline = _context.Airlines.FirstOrDefault(airline => airline.AirlineID == airlineId);

            if (airline == null)
            {
                throw new ArgumentException($"Invalid id: {id}. No airline found with id equal to {id}.");
            }

            try
            {
                airline.FleetSize = fleetSize;
                _context.SaveChanges();
            }
            catch (DbUpdateException ex)
            {
                // Database update exception
                Console.WriteLine($"Error updating airline {id}: {ex.Message}");
            }
        }

        // Delete the airline with the same key from the db
        public void DeleteAirline(int id)
        {
            var airline = _context.Airlines.FirstOrDefault(airline => airline.AirlineID == id);

            if (airline == null)
            {
                throw new ArgumentException($"Invalid id: {id}. No airline found with id equal to {id}.");
            }

            try
            {
                _context.Airlines.Remove(airline);
                _context.SaveChanges();
            }
            catch (DbUpdateException ex)
            {
                // Database update exception
                Console.WriteLine($"Error deleting airline {id}: {ex.Message}");
            }
        }

        public int GetNextAirlineID()
        {
            // Retrieve the highest existing AirlineID and add 1 to it
            var nextAirlineID = _context.Airlines.Count() + 1;

            return nextAirlineID;
        }
    }
}

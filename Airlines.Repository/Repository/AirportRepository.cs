﻿using Microsoft.EntityFrameworkCore;
using Airlines.Repository.Entities;
using Airlines.Repository.Dtos;
using AutoMapper;

namespace Airlines.Repository.Repository
{
    public class AirportRepository(AirlinesContext context, IMapper mapper) : IAirportRepository
    {
        private readonly AirlinesContext _context = context;
        private readonly IMapper _mapper = mapper;

        public static void Dispose()
        {
        }

        // Return all the airports in the db
        public async Task<List<AirportDto>> GetAirportsAsync()
        {
            var airports = await _context.Airports.ToListAsync(); // Asynchronous database call
            return _mapper.Map<List<AirportDto>>(airports);
        }

        // Return the airports by search in their Code or Name
        public async Task<List<AirportDto>> SearchAirportsAsync(string searchTerm)
        {
            var airports = await _context.Airports
                .Where(a => a.Code.Contains(searchTerm)
                        || a.Name.Contains(searchTerm)
                        || a.City.Contains(searchTerm))
                .ToListAsync();

            return _mapper.Map<List<AirportDto>>(airports);
        }

        // Return the airports with filter with some property of airport and value of that property
        public List<AirportDto> GetAirportsByFilter(string filter, string value)
        {
            // Check if the filter exists in the Airport entity
            var propertyFound = typeof(Airport).GetProperty(filter);
            if (propertyFound == null)
            {
                throw new ArgumentException($"Invalid filter: {filter}. This property does not exist in the Airport entity.");
            }

            var result = _context.Airports.Where(airport => EF.Property<string>(airport, filter) == value);

            // Check if the value does not match any records
            if (result.FirstOrDefault() == null)
            {
                throw new Exception($"No airports found with {filter} equal to {value}.");
            }

            return _mapper.Map<List<AirportDto>>(result.ToList());
        }

        // Return the airport with the max runways count 
        public Airport? GetAirportMaxRunwaysCount()
        {
            var query = from airport in _context.Airports
                        orderby airport.RunwaysCount descending
                        select new Airport
                        {
                            Code = airport.Code,
                            AirportID = airport.AirportID,
                            Name = airport.Name,
                            Country = airport.Country,
                            City = airport.City,
                            RunwaysCount = airport.RunwaysCount,
                            Founded = airport.Founded
                        };

            // Check if there are any airports at all
            if (query.FirstOrDefault() == null)
            {
                throw new Exception($"No airports found.");
            }

            return query.FirstOrDefault();
        }

        // Return the airport with the min runways count
        public Airport? GetAirportMinRunwaysCount()
        {
            var query = from airport in _context.Airports
                        orderby airport.RunwaysCount ascending
                        select new Airport
                        {
                            Code = airport.Code,
                            AirportID = airport.AirportID,
                            Name = airport.Name,
                            Country = airport.Country,
                            City = airport.City,
                            RunwaysCount = airport.RunwaysCount,
                            Founded = airport.Founded
                        };

            // Check if there are any airports at all
            if (query.FirstOrDefault() == null)
            {
                throw new Exception($"No airports found.");
            }

            return query.FirstOrDefault();
        }

        // Add airport object to the db
        public void AddAirport(AirportDto airportDto)
        {
            try
            {
                var airport = _mapper.Map<Airport>(airportDto);
                _context.Airports.Add(airport);
                _context.SaveChanges();
            }
            catch (DbUpdateException ex)
            {
                // Database update exception
                Console.WriteLine($"Error adding airport: {ex.Message}");
            }
        }

        // Update the airport object with the same key in the db
        public void UpdateAirport(AirportDto updatedAirportDto)
        {
            try
            {
                var updatedAirport = _mapper.Map<Airport>(updatedAirportDto);

                var existingAirport = _context.Airports.FirstOrDefault(a => a.Code == updatedAirport.Code);

                if (existingAirport != null)
                {
                    existingAirport.Name = updatedAirport.Name;
                    existingAirport.Country = updatedAirport.Country;
                    existingAirport.City = updatedAirport.City;
                    existingAirport.RunwaysCount = updatedAirport.RunwaysCount;
                    existingAirport.Founded = updatedAirport.Founded;

                    _context.SaveChanges();
                }
                else
                {
                    Console.WriteLine($"Airport with code {updatedAirportDto.Code} not found.");
                }
            }
            catch (DbUpdateException ex)
            {
                // Database update exception
                Console.WriteLine($"Error updating airport {updatedAirportDto.Code}: {ex.Message}");
            }
        }

        // Update the airport object runtimeCount with the same key in the db
        public void UpdateAirportRunwaysCount(string code, int runwayCount)
        {
            var airport = _context.Airports.FirstOrDefault(airport => airport.Code == code);

            if (airport == null)
            {
                throw new ArgumentException($"Invalid code: {code}. No airport found with code equal to {code}.");
            }

            try
            {
                airport.RunwaysCount = runwayCount;
                _context.SaveChanges();
            }
            catch (DbUpdateException ex)
            {
                // Database update exception
                Console.WriteLine($"Error updating airport {code}: {ex.Message}");
            }
        }

        // Delete the airport with the same key from the db
        public void DeleteAirport(string code)
        {
            var airport = _context.Airports.FirstOrDefault(airport => airport.Code == code);

            if (airport == null)
            {
                throw new ArgumentException($"Invalid code: {code}. No airport found with code equal to {code}.");
            }

            try
            {
                _context.Airports.Remove(airport);
                _context.SaveChanges();
            }
            catch (DbUpdateException ex)
            {
                // Database update exception
                Console.WriteLine($"Error deleting airport {code}: {ex.Message}");
            }
        }
    }
}

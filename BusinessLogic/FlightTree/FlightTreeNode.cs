﻿using Airlines.Persistence.Basic;

namespace BusinessLogic;
public class FlightTreeNode(Flight flight)
{
    public Flight Flight { get; set; } = flight;
    public List<FlightTreeNode> Children { get; } = [];

    public int GetNumChildren()
    {
        return Children.Count;
    }

    public void AddChild(FlightTreeNode child)
    {
        Children.Add(child);
    }

    public void RemoveChild(FlightTreeNode child)
    {
        _ = Children.Remove(child);
    }
}


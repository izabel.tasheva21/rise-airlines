﻿using Airlines.Persistence.Basic;

namespace BusinessLogic;
public class FlightRouteTree(Flight rootFlight)
{
    public FlightTreeNode Root { get; set; } = new FlightTreeNode(rootFlight);

    public int Size()
    {
        return SizeRecursive(Root);
    }
    public static int SizeRecursive(FlightTreeNode node)
    {
        if (node == null)
            return 0;

        var counter = 1;
        foreach (var childNode in node.Children)
        {
            counter += SizeRecursive(childNode);
        }

        return counter;
    }

    public void Add(Flight newFlight)
    {
        AddRecursive(Root, newFlight);
    }

    private static void AddRecursive(FlightTreeNode node, Flight newFlight)
    {
        if (node == null)
        {
            return;
        }

        // Check if the arrival airport of the current node's flight matches the departure airport of the new flight
        if (node.Flight.ArrivalAirport == newFlight.DepartureAirport)
        {
            // Add the new flight as a child of the current node
            node.AddChild(new FlightTreeNode(newFlight));
            return;
        }

        // Recursively search for the appropriate parent node
        foreach (var childNode in node.Children)
        {
            AddRecursive(childNode, newFlight);
        }
    }

    public static void PrintFlightTree(FlightTreeNode node, int level)
    {
        // Print information for the root
        if (level == 0)
        {
            Console.WriteLine("Flight Tree:");
            Console.WriteLine(node.Flight.ArrivalAirport);
        }
        if (node == null)
        {
            return;
        }

        // Print flight information for all nodes except the root
        if (level > 0)
        {
            Console.WriteLine($"{new string(' ', level * 2)}{node.Flight.Id} ({node.Flight.DepartureAirport} -> {node.Flight.ArrivalAirport}) - {node.Flight.AircraftModel}");
        }

        foreach (var childNode in node.Children)
        {
            PrintFlightTree(childNode, level + 1);
        }
    }

    public FlightTreeNode Find(string arrivalAirport) => FindRecursive(Root, arrivalAirport);

    private static FlightTreeNode FindRecursive(FlightTreeNode node, string arrivalAirport)
    {
        if (node == null)
        {
            throw new InvalidInputException("No such flight with that destination airport.");
        }

        // Check if the current node's flight has the desired arrival airport
        if (node.Flight.ArrivalAirport == arrivalAirport)
        {
            return node; // Node found
        }

        // Recursively search in the child nodes
        foreach (var childNode in node.Children)
        {
            var foundNode = FindRecursive(childNode, arrivalAirport);
            if (foundNode != null)
            {
                return foundNode; // Node found in the child subtree
            }
        }

        throw new InvalidInputException("No such flight with that destination airport."); // Node not found in the subtree
    }

    public void PrintFlightRoute(FlightTreeNode? destinationNode)
    {
        // Ensure destinationNode is not null
        if (destinationNode == null)
        {
            Console.WriteLine("Sorry, there is no route leading to this airport! You should catch the train.");
            return;
        }

        // Find the path from the root to the destination node
        var flightRoute = FindFlightRoute(Root, destinationNode);

        if (flightRoute == null)
        {
            Console.WriteLine("Sorry, there is no route leading to this airport! You should catch the train.");
            return;
        }

        // Print the flight IDs along the route
        if (flightRoute.Count == 1)
        {
            Console.WriteLine("You already are on the destination airport.");
        }
        else if (flightRoute.Count > 1)
        {
            flightRoute.RemoveAt(0);
            Console.WriteLine("Flight Route:");
            for (var i = 0; i <= flightRoute.Count - 1; i++)
            {
                Console.WriteLine($"Flight ID: {flightRoute[i].Id}");
            }
        }
    }

    internal int SizeFlightRoute(FlightTreeNode destinationNode)
    {
        var nodes = FindFlightRoute(Root, destinationNode);
        return nodes != null ? nodes.Count : -1;
    }

    internal static List<Flight>? FindFlightRoute(FlightTreeNode startNode, FlightTreeNode destinationNode)
    {
        // Initialize a list to store the flight route
        var flightRoute = new List<Flight>();

        // Perform depth-first search to find the route
        if (FindFlightRouteDFS(startNode, destinationNode, flightRoute))
        {
            return flightRoute;
        }

        return null; // No route found
    }

    private static bool FindFlightRouteDFS(FlightTreeNode currentNode, FlightTreeNode destinationNode, List<Flight> flightRoute)
    {
        // If the current node is null, return false
        if (currentNode == null)
        {
            return false;
        }

        // Add the current flight to the route
        flightRoute.Add(currentNode.Flight);

        // If the current node is the destination node, return true
        if (currentNode == destinationNode)
        {
            return true;
        }

        // Recursively search in the child nodes
        foreach (var childNode in currentNode.Children)
        {
            if (FindFlightRouteDFS(childNode, destinationNode, flightRoute))
            {
                return true; // Route found
            }
        }

        // If the destination node is not found in the subtree, remove the last flight from the route
        flightRoute.RemoveAt(flightRoute.Count - 1);
        return false; // Route not found
    }
}

﻿using Airlines.Persistence.Basic;

namespace BusinessLogic;
internal class ExistCommand : ICommand<string[]>
{
    public void Execute(List<Airport> airports, List<Airline> airlines, List<Flight> flights, List<Aircraft> aircrafts, List<IFlightRoute<Flight>> routes, string[] commandParts, FlightRouteTree flightRouteTree)
    {
        var airlinesSet = new HashSet<string>();
        foreach (var airline in airlines)
        {
            _ = airlinesSet.Add(airline.Name);
        }

        Console.WriteLine("\n");

        var contains = airlinesSet.Contains(commandParts[1]);

        if (contains)
        {
            Console.WriteLine(airlinesSet.Contains(commandParts[1]));
        }
        else
        {
            throw new InvalidInputException("Airlines not found.");
        }
    }
}

﻿using static BusinessLogic.SearchingAlgorithms;
using static BusinessLogic.SortingAlgorithms;
using Airlines.Persistence.Basic;

namespace BusinessLogic;
internal class ReserveCommand() : ICommand<string[]>
{
    public void Execute(List<Airport> airports, List<Airline> airlines, List<Flight> flights, List<Aircraft> aircrafts, List<IFlightRoute<Flight>> routes, string[] commandParts, FlightRouteTree flightRouteTree)
    {
        var reservationObject = commandParts[1];

        _ = reservationObject switch
        {
            "cargo" => CargoReservation(commandParts, flights, aircrafts),
            "ticket" => TicketReservation(commandParts, flights, aircrafts),
            _ => throw new InvalidInputException("Invalid command provided."),
        };
    }

    public static bool CargoReservation(string[] commands, List<Flight> flights, List<Aircraft> aircrafts)
    {
        if (commands.Length < 5)
        {
            Console.WriteLine("Insufficient arguments for cargo reservation. Please provide flight ID, weight, and volume.");
            return false;
        }

        var id = commands[2];
        var weight = int.Parse(commands[3]);
        var volume = double.Parse(commands[4]);

        // Check if there is such a flight
        SelectionSort(flights, (f1, f2) => f1.Id.CompareTo(f2.Id), true);
        var flightIndex = SearchFlights(flights, id);

        if (flightIndex >= 0)
        {
            var flight = flights[flightIndex];
            var aircraftModel = flight.AircraftModel;

            // Find CargoAircraft with matching model
            if (aircrafts.Find(a => a is CargoAircraft && a.Model.Equals(aircraftModel)) is CargoAircraft cargoAircraft)
            {
                if (cargoAircraft.GetWeight() >= weight && cargoAircraft.GetVolume() >= volume)
                {
                    Console.WriteLine("Cargo reserved successfully.");
                    return true;
                }
                else
                {
                    Console.WriteLine("Cargo reservation failed. Insufficient weight or volume capacity.");
                }
            }
            else
            {
                Console.WriteLine("No Cargo Aircraft found for the given flight.");
            }
        }
        else
        {
            throw new InvalidFlightCodeException("No valid flight id provided.");
        }

        return false;
    }

    public static bool TicketReservation(string[] commands, List<Flight> flights, List<Aircraft> aircrafts)
    {
        if (commands.Length < 6)
        {
            Console.WriteLine("Insufficient arguments for ticket reservation. Please provide flight ID, weight, and volume.");
            return false;
        }

        var id = commands[2];
        var seats = int.Parse(commands[3]);
        var smallBaggageCount = int.Parse(commands[4]);
        var largeBaggageCount = int.Parse(commands[5]);

        var sumBaggageMaxWeight = (smallBaggageCount * 15) + (largeBaggageCount * 30);
        var sumBaggageMaxVolume = (smallBaggageCount * 0.045) + (largeBaggageCount * 0.090);

        // Check if there is such a flight
        SelectionSort(flights, (f1, f2) => f1.Id.CompareTo(f2.Id), true);
        var flightIndex = SearchFlights(flights, id);

        if (flightIndex >= 0)
        {
            var flight = flights[flightIndex];
            var aircraftModel = flight.AircraftModel;

            // Find CargoAircraft with matching model
            if (aircrafts.Find(a => a is PassengerAircraft && a.Model.Equals(aircraftModel)) is PassengerAircraft passengerAircraft)
            {
                if (passengerAircraft.GetSeats() >= seats && passengerAircraft.GetWeight() >= sumBaggageMaxWeight && passengerAircraft.GetVolume() >= sumBaggageMaxVolume)
                {
                    Console.WriteLine("Ticket reserved successfully.");
                    return true;
                }
                else
                {
                    Console.WriteLine("Ticket reservation failed. Insufficient seats, weight or volume capacity.");
                }
            }
            else
            {
                Console.WriteLine("No Passenger Aircraft found for the given flight.");
            }
        }
        else
        {
            throw new InvalidFlightCodeException("No valid flight id provided.");
        }

        return false;
    }
}
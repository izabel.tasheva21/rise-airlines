﻿using Airlines.Persistence.Basic;

namespace BusinessLogic;
public interface ICommand<T>
{
    void Execute(List<Airport> airports, List<Airline> airlines, List<Flight> flights, List<Aircraft> aircrafts, List<IFlightRoute<Flight>> routes, string[] commandParts, FlightRouteTree flightRouteTree);
}

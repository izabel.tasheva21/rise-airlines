﻿using Airlines.Persistence.Basic;

namespace BusinessLogic;
internal class InvalidCommand : ICommand<string[]>
{
    public void Execute(List<Airport> airports, List<Airline> airlines, List<Flight> flights, List<Aircraft> aircrafts, List<IFlightRoute<Flight>> routes, string[] commandParts, FlightRouteTree flightRouteTree)
    {
        throw new InvalidInputException("Invalid command provided.");
    }
}

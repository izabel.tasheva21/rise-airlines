﻿using Airlines.Persistence.Basic;

namespace BusinessLogic;
public class CommandProcessing()
{
    // Default value is false!
    public static bool IsBatchModeOn { get; set; }
    private static readonly Queue<string> _batchQueue = [];

    public static void ProcessCommand(string? commandLine, List<Airport> airports, List<Airline> airlines, List<Flight> flights, List<Aircraft> aircrafts, List<IFlightRoute<Flight>> routes, FlightRouteTree flightRouteTree)
    {
        if (commandLine == null)
        {
            throw new InvalidInputException("Empty command provided. Please provide valid command.");
        }

        var commandParts = commandLine.Split(" ");

        if (commandParts.Length == 0)
        {
            throw new InvalidInputException("Empty command provided. Please provde valid command.");
        }

        var commandType = commandParts[0];

        try
        {
            switch (commandType)
            {
                case "batch":
                    ProcessBatchCommand(commandParts, airports, airlines, flights, aircrafts, routes, flightRouteTree);
                    return;
                default:
                    ExecuteNormalCommand(commandLine, airports, airlines, flights, aircrafts, routes, commandParts, flightRouteTree);
                    return;
            }
        }
        catch (InvalidInputException ex)
        {
            Console.WriteLine($"Error: {ex.Message}");
        }
        catch (InvalidFlightCodeException ex)
        {
            Console.WriteLine($"Error: {ex.Message}");
        }
        catch (InvalidAirlineNameException ex)
        {
            Console.WriteLine($"Error: {ex.Message}");
        }
        catch (ArgumentException ex)
        {
            Console.WriteLine($"Error: {ex.Message}");
        }
        catch (Exception ex)
        {
            Console.WriteLine($"Unexpected error: {ex.Message}");
        }
    }

    internal static void ProcessBatchCommand(string[] commands, List<Airport> airports, List<Airline> airlines, List<Flight> flights, List<Aircraft> aircrafts, List<IFlightRoute<Flight>> routes, FlightRouteTree flightRouteTree)
    {
        var batchType = commands[1];

        switch (batchType)
        {
            case "start":
                IsBatchModeOn = true;
                Console.WriteLine("Batch mode started.");
                break;
            case "run":
                IsBatchModeOn = false;
                ExecuteBatchCommands(airports, airlines, flights, aircrafts, routes, flightRouteTree);
                break;
            case "cancel":
                ClearBatchQueue();
                break;
            default:
                throw new InvalidInputException("No valid batch command provided.");
        }
    }

    internal static void ExecuteNormalCommand(string commandLine, List<Airport> airports, List<Airline> airlines, List<Flight> flights, List<Aircraft> aircrafts, List<IFlightRoute<Flight>> routes, string[] commands, FlightRouteTree flightRouteTree)
    {
        if (IsBatchModeOn)
        {
            // Add the command to the batch queue
            _batchQueue.Enqueue(commandLine);
        }
        else
        {
            try
            {
                var command = CommandFactory.CreateCommand(commandLine);
                command.Execute(airports, airlines, flights, aircrafts, routes, commands, flightRouteTree);
            }
            catch (ArgumentException ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
    }

    internal static void ExecuteBatchCommands(List<Airport> airports, List<Airline> airlines, List<Flight> flights, List<Aircraft> aircrafts, List<IFlightRoute<Flight>> routes, FlightRouteTree flightRouteTree)
    {
        while (_batchQueue.Count > 0)
        {
            ProcessCommand(_batchQueue.Dequeue(), airports, airlines, flights, aircrafts, routes, flightRouteTree);
        }
        ClearBatchQueue();
    }

    internal static void ClearBatchQueue()
    {
        _batchQueue.Clear();
        IsBatchModeOn = false;
        Console.WriteLine("Batch mode canceled. Batch queue cleared.");
    }
}



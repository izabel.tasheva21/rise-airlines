﻿using static BusinessLogic.SortingAlgorithms;
using Airlines.Persistence.Basic;

namespace BusinessLogic;
internal class SortCommand() : ICommand<string[]>
{
    public void Execute(List<Airport> airports, List<Airline> airlines, List<Flight> flights, List<Aircraft> aircrafts, List<IFlightRoute<Flight>> routes, string[] commandParts, FlightRouteTree flightRouteTree)
    {
        var arrayToSort = commandParts[1];
        var isAscending = true;
        if (commandParts.Length == 3)
        {
            if (commandParts[2].Equals("descending"))
            {
                isAscending = false;
            }
        }

        switch (arrayToSort)
        {
            case "airports":
                SortAirports(airports, isAscending);
                break;
            case "airlines":
                SortAirlines(airlines, isAscending);
                break;
            case "flights":
                SortFlights(flights, isAscending);
                break;
            default:
                throw new InvalidInputException("No valid sort command provided.");
        }
    }

    public static void SortAirports(List<Airport> airports, bool isAscending)
    {
        BubbleSort(airports, (a1, a2) => string.Compare(a1.Name, a2.Name) > 0, isAscending);
        Console.WriteLine("\n");
        Console.WriteLine("Assigned airports:");
        OutputData.PrintEntities(airports, x => x.Name);
    }

    public static void SortAirlines(List<Airline> airlines, bool isAscending)
    {
        SelectionSort(airlines, (a1, a2) => a1.Name.CompareTo(a2.Name), isAscending);
        Console.WriteLine("\n");
        Console.WriteLine("Assigned airlines:");
        OutputData.PrintEntities(airlines, x => x.Name);
    }

    public static void SortFlights(List<Flight> flights, bool isAscending)
    {
        SelectionSort(flights, (f1, f2) => f1.Id.CompareTo(f2.Id), isAscending);
        Console.WriteLine("\n");
        Console.WriteLine("Assigned flights:");
        OutputData.PrintEntities(flights, x => x.Id);
    }
}

﻿using Airlines.Persistence.Basic;

namespace BusinessLogic;
internal class ListCommand() : ICommand<string[]>
{
    public void Execute(List<Airport> airports, List<Airline> airlines, List<Flight> flights, List<Aircraft> aircrafts, List<IFlightRoute<Flight>> routes, string[] commandParts, FlightRouteTree flightRouteTree)
    {
        var form = commandParts[^1];
        var place = commandParts[1];
        if (commandParts.Length > 3)
        {
            for (var i = 2; i < commandParts.Length - 1; i++)
            {
                place += " " + commandParts[i];
            }
        }

        var airportsSet = new HashSet<Airport>(airports);

        if (form.Equals("City"))
        {
            var airportsToPrint = airportsSet.Where(airport => airport.City.Equals(place)).ToList();

            if (airportsToPrint != null)
            {
                OutputData.PrintEntities(airportsSet.Where(airport => airport.City.Equals(place)).ToList(), x => x.Name);
            }
            else
            {
                throw new InvalidInputException("No such data found!");
            }
        }
        else if (form.Equals("Country"))
        {
            var airportsToPrint = airportsSet.Where(airport => airport.City.Equals(place)).ToList();

            if (airportsToPrint != null)
            {
                OutputData.PrintEntities(airportsSet.Where(airport => airport.Country.Equals(place)).ToList(), x => x.Name);
            }
            else
            {
                throw new InvalidInputException("No such data found!");
            }
        }
        else
        {
            throw new InvalidInputException("Search for a City of a Country!");
        }
    }
}

﻿using static BusinessLogic.SearchingAlgorithms;
using Airlines.Persistence.Basic;

namespace BusinessLogic;
internal class SearchCommand() : ICommand<string[]>
{
    public void Execute(List<Airport> airports, List<Airline> airlines, List<Flight> flights, List<Aircraft> aircrafts, List<IFlightRoute<Flight>> routes, string[] commandParts, FlightRouteTree flightRouteTree)
    {
        var searchWord = commandParts[1];
        _ = SearchKeyWord(airports, airlines, flights, searchWord);
    }
}

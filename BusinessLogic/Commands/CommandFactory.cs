﻿namespace BusinessLogic;
public class CommandFactory
{
    public static ICommand<string[]> CreateCommand(string commandLine)
    {
        var commandParts = commandLine.Split(' ');
        return commandParts[0] switch
        {
            "search" => new SearchCommand(),
            "sort" => new SortCommand(),
            "exist" => new ExistCommand(),
            "list" => new ListCommand(),
            "route" => new RouteCommand(),
            "reserve" => new ReserveCommand(),
            _ => new InvalidCommand(),
        };
    }
}

﻿using static BusinessLogic.SearchingAlgorithms;
using static BusinessLogic.SortingAlgorithms;
using Airlines.Persistence.Basic;
using Airline = Airlines.Persistence.Basic.Airline;
using Airport = Airlines.Persistence.Basic.Airport;
using Flight = Airlines.Persistence.Basic.Flight;

namespace BusinessLogic;
internal class RouteCommand() : ICommand<string[]>
{
    public void Execute(List<Airport> airports, List<Airline> airlines, List<Flight> flights, List<Aircraft> aircrafts, List<IFlightRoute<Flight>> routes, string[] commandParts, FlightRouteTree flightRouteTree)
    {
        var operation = commandParts[1];

        switch (operation)
        {
            case "new":
                InitialiseRoute(routes);
                break;

            case "add":
                AddFlightToLastRoute(commandParts, flights, routes);
                break;

            case "remove":
                RemoveFlightFromLastRoute(routes);
                break;

            case "print":
                PrintLastRoute(routes);
                break;

            case "find":
                FindRoute(commandParts, flightRouteTree);
                break;

            case "check":
                ExistRoute(commandParts, airports, flights);
                break;

            case "search":
                switch (commandParts[4])
                {
                    case "cheap":
                        SearchCheapestRoute(commandParts, flights, airports);
                        break;

                    case "short":
                        SearchShortestRoute(commandParts, flights, airports);
                        break;

                    case "stops":
                        SearchFewestStopsRoute(commandParts, flights, airports);
                        break;

                    default:
                        throw new InvalidInputException("No valid route search command provided!");
                }
                break;

            default:
                throw new InvalidInputException("No valid route command provided!");
        }
    }

    public static void InitialiseRoute(List<IFlightRoute<Flight>> routes)
    {
        IFlightRoute<Flight> route = new FlightRoute();
        routes.Add(route);
    }

    public static void AddFlightToLastRoute(string[] commands, List<Flight> flights, List<IFlightRoute<Flight>> routes)
    {
        var currId = commands[2];

        SelectionSort(flights, (f1, f2) => f1.Id.CompareTo(f2.Id), true);
        var flightIndex = SearchFlights(flights, currId);

        if (flightIndex < 0)
        {
            return;
        }
        var route = routes.Last();
        route.AddFlight(flights[flightIndex]);
    }

    public static void RemoveFlightFromLastRoute(List<IFlightRoute<Flight>> routes)
    {
        if (routes.Count > 0)
        {
            var route = routes.Last();
            route.RemoveLast();
        }
    }

    public static void PrintLastRoute(List<IFlightRoute<Flight>> routes)
    {
        var route = routes.Last();

        route.PrintRoute();
    }

    public static void FindRoute(string[] commands, FlightRouteTree flightRouteTree)
    {
        var arrivalAirport = commands[2];
        flightRouteTree.PrintFlightRoute(flightRouteTree.Find(arrivalAirport));
    }

    public static void ExistRoute(string[] commands, List<Airport> airports, List<Flight> flights)
    {
        var departure = commands[2];
        var arrival = commands[3];

        var weightedGraph = new WeightedGraph();

        // Assign edges to the graph
        foreach (var flight in flights)
        {
            var d = airports.Find(a => a.Identifier.Equals(flight.DepartureAirport));
            var a = airports.Find(a => a.Identifier.Equals(flight.ArrivalAirport));
            var weight = flight.Price;

            if (d != null && a != null)
            {
                weightedGraph.AddEdge(d, a, weight);
            }
        }

        weightedGraph.PrintIsReachable(airports, departure, arrival);
    }

    public static void SearchCheapestRoute(string[] commands, List<Flight> flights, List<Airport> airports)
    {
        var departure = commands[2];
        var arrival = commands[3];
        // dist[] array stores the distance of nodes from S
        var distance = new Dictionary<Airport, double>();

        // Create a graph
        var weightedGraph = new WeightedGraph();

        // Assign edges to the graph
        foreach (var flight in flights)
        {
            var d = airports.Find(a => a.Identifier.Equals(flight.DepartureAirport));
            var a = airports.Find(a => a.Identifier.Equals(flight.ArrivalAirport));
            var weight = flight.Price;

            if (d != null && a != null)
            {
                weightedGraph.AddEdge(d, a, weight);
            }
        }

        weightedGraph.PrintLowestSumWeightPath(departure, arrival, flights, airports, distance);
    }

    public static void SearchShortestRoute(string[] commands, List<Flight> flights, List<Airport> airports)
    {
        var departure = commands[2];
        var arrival = commands[3];
        // dist[] array stores the distance of nodes from S
        var distance = new Dictionary<Airport, double>();

        // Create a graph
        var weightedGraph = new WeightedGraph();

        // Assign edges to the graph
        foreach (var flight in flights)
        {
            var d = airports.Find(a => a.Identifier.Equals(flight.DepartureAirport));
            var a = airports.Find(a => a.Identifier.Equals(flight.ArrivalAirport));
            var weight = flight.Duration;

            if (d != null && a != null)
            {
                weightedGraph.AddEdge(d, a, weight);
            }
        }

        weightedGraph.PrintLowestSumWeightPath(departure, arrival, flights, airports, distance);
    }

    public static void SearchFewestStopsRoute(string[] commands, List<Flight> flights, List<Airport> airports)
    {
        var departure = commands[2];
        var arrival = commands[3];
        // dist[] array stores the distance of nodes from S
        var distance = new Dictionary<Airport, int>();

        // Create a graph
        var weightedGraph = new WeightedGraph();

        // Assign edges to the graph
        foreach (var flight in flights)
        {
            var d = airports.Find(a => a.Identifier.Equals(flight.DepartureAirport));
            var a = airports.Find(a => a.Identifier.Equals(flight.ArrivalAirport));
            var weight = 1;

            if (d != null && a != null)
            {
                weightedGraph.AddEdge(d, a, weight);
            }
        }

        weightedGraph.PrintFewestStopsPath(departure, arrival, flights, airports, distance);
    }
}

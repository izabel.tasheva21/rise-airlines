﻿using Airlines.Persistence.Basic;

namespace BusinessLogic;
public class WeightedGraph
{
    // Adjacency list representation
    private readonly Dictionary<Airport, Dictionary<Airport, double>> _adjList;

    public WeightedGraph()
    {
        _adjList = [];
    }

    // Add an edge to the graph
    public void AddEdge(Airport departure, Airport arrival, double weight)
    {
        if (!_adjList.TryGetValue(departure, out var value))
        {
            value = [];
            _adjList[departure] = value;
        }

        value.Add(arrival, weight);
    }

    // Print if there is a route from departure airport to arrival airport
    public void PrintIsReachable(List<Airport> airports, string departureName, string arrivalName)
    {
        var d = airports.Find(a => a.Identifier.Equals(departureName));
        var a = airports.Find(a => a.Identifier.Equals(arrivalName));

        if (d != null && a != null)
        {
            Console.WriteLine(IsReachable(d, a));
        }
        else
        {
            throw new InvalidInputException("No such airports exist.");
        }
    }

    // Return true if there is a route from departure airport to arrival airport, else otherways
    public bool IsReachable(Airport departure, Airport arrival)
    {
        // Mark all the airports as not visited
        var visited = new Dictionary<Airport, bool>();
        foreach (var airport in _adjList.Keys)
        {
            visited[airport] = false;
        }

        // Create a queue for BFS
        var queue = new Queue<Airport>();

        if (!visited.ContainsKey(departure))
        {
            return false;
        }

        // Mark the departure airport as visited and enqueue it
        //visited[departure] = true;
        queue.Enqueue(departure);

        // Perform BFS
        while (queue.Count != 0)
        {
            // Dequeue an airport from the queue
            var currentAirport = queue.Dequeue();

            // If the dequeued airport is the arrival airport, return true
            if (currentAirport == arrival)
            {
                return true;
            }

            if (!visited.ContainsKey(currentAirport))
            {
                continue;
            }

            // Get all adjacent airports of the dequeued airport
            foreach (var neighborAirport in _adjList[currentAirport])
            {
                if (!visited.TryGetValue(neighborAirport.Key, out var _))
                {
                    queue.Enqueue(neighborAirport.Key);
                    continue;
                }

                // If a neighbor airport has not been visited, mark it visited and enqueue it
                if (!visited[neighborAirport.Key])
                {
                    //visited[neighborAirport] = true;
                    queue.Enqueue(neighborAirport.Key);
                }
            }
        }

        // If BFS is complete without visiting the arrival airport
        return false;
    }

    // Print the adjacency list representation of the graph
    public void PrintGraph()
    {
        foreach (var keyValuePair in _adjList)
        {
            Console.Write($"Direct flights from airport {keyValuePair.Key.Identifier} to: ");
            foreach (var airport in keyValuePair.Value)
            {
                Console.Write($"{airport.Key.Identifier} ({airport.Value}) ");
            }
            Console.WriteLine();
        }
    }

    // Find the route from departure to arrival airport with the lowest sum weight
    internal List<Airport>? LowestSumWeightPath(Airport departure, Airport arrival, Dictionary<Airport, double> distance, List<Airport> airports)
    {
        // Mark all the airports as not visited
        var visited = new Dictionary<Airport, bool>();
        var parents = new Dictionary<Airport, Dictionary<Airport, double>?>();
        foreach (var airport in airports)
        {
            visited[airport] = false;
            parents[airport] = [];
        }

        // Create a queue for BFS
        var queue = new Queue<Airport>();

        if (!visited.ContainsKey(departure))
        {
            throw new InvalidInputException("Departure airport does not exist.");
        }

        if (!_adjList.ContainsKey(departure))
        {
            return null;
        }

        // Mark the distance of the source node as 0
        distance.Add(departure, 0);

        queue.Enqueue(departure);

        while (queue.Count != 0)
        {
            var currentAirport = queue.Dequeue();

            // The arrival airport if found
            if (currentAirport == arrival)
            {
                var cheapestPath = new List<Airport>();
                var vertix = arrival;

                while (vertix != null)
                {
                    cheapestPath.Add(vertix);
                    var currParents = parents[vertix];

                    if (currParents != null)
                    {
                        if (currParents.Count == 0)
                        {
                            vertix = null;
                            continue;
                        }

                        var closestVert = currParents.First();

                        foreach (var currParent in currParents)
                        {
                            if (distance[currParent.Key] < distance[closestVert.Key] + closestVert.Value)
                            {
                                closestVert = currParent;
                            }
                        }

                        vertix = closestVert.Key;
                    }
                }
                cheapestPath.Reverse();
                return cheapestPath;
            }

            if (!visited.ContainsKey(currentAirport))
            {
                continue;
            }

            foreach (var neighbourAirport in _adjList[currentAirport])
            {

                if (!visited.TryGetValue(neighbourAirport.Key, out var _))
                {
                    queue.Enqueue(neighbourAirport.Key);

                    if (parents[neighbourAirport.Key] != null)
                    {
                        var parent = parents[neighbourAirport.Key];

                        var weight = _adjList[currentAirport][neighbourAirport.Key];

                        if (weight != 0)
                        {
                            parent?.Add(currentAirport, weight);
                            parents[neighbourAirport.Key] = parent;
                        }

                        if (distance[neighbourAirport.Key] == 0)
                        {
                            distance[neighbourAirport.Key] = distance[currentAirport] + weight;
                        }
                    }

                    continue;
                }

                // If a neighbor airport has not been visited, mark it visited and enqueue it
                if (!visited[neighbourAirport.Key])
                {
                    if (parents[neighbourAirport.Key] != null)
                    {
                        var parent = parents[neighbourAirport.Key];

                        var weight = _adjList[currentAirport][neighbourAirport.Key];

                        if (weight != 0)
                        {
                            parent?.Add(currentAirport, weight);
                            parents[neighbourAirport.Key] = parent;
                        }

                        distance[neighbourAirport.Key] = distance[currentAirport] + weight;
                    }

                    if (!queue.Contains(neighbourAirport.Key))
                    {
                        queue.Enqueue(neighbourAirport.Key);
                    }
                }
            }
        }

        // If the route is not found
        return null;
    }

    // Print the route with LowestSumWeight or throw exception 
    public void PrintLowestSumWeightPath(string departureAirport, string arrivalAirport, List<Flight> flights, List<Airport> airports, Dictionary<Airport, double> distance)
    {
        var departure = airports.Find(a => a.Identifier.Equals(departureAirport));
        var arrival = airports.Find(a => a.Identifier.Equals(arrivalAirport));

        var cheapestPath = new List<Airport>();

        cheapestPath = departure == null || arrival == null
            ? throw new InvalidInputException("Departure or arrival airport does not exist.")
            : LowestSumWeightPath(departure, arrival, distance, airports);

        if (cheapestPath == null)
        {
            Console.WriteLine("No path found from " + departureAirport + " to " + arrivalAirport);
            return;
        }

        Console.WriteLine("Cheapest path from " + departureAirport + " to " + arrivalAirport + ":");

        Console.WriteLine("Flights to catch:");
        for (var i = 0; i < cheapestPath.Count - 1; i++)
        {
            var currFlight = flights.Find(f => f.DepartureAirport.Equals(cheapestPath[i].Identifier) && f.ArrivalAirport.Equals(cheapestPath[i + 1].Identifier));

            if (currFlight != null)
            {
                if (i == cheapestPath.Count - 1 - 1)
                {
                    Console.WriteLine(currFlight.Id);
                    break;
                }
                Console.Write(currFlight.Id + " -> ");
            }
        }

        Console.WriteLine("Airports to go through:");
        foreach (var airport in cheapestPath)
        {
            Console.Write(airport.Identifier + " ");
        }
        Console.WriteLine();
    }

    // Find the route from departure to arrival airport with the fewest stops
    internal List<Airport>? FewestStopsPath(Airport departure, Airport arrival, Dictionary<Airport, int> distance, List<Airport> airports)
    {
        // Mark all the airports as not visited
        var visited = new Dictionary<Airport, bool>();
        var parents = new Dictionary<Airport, List<Airport>?>();
        foreach (var airport in airports)
        {
            visited[airport] = false;
            parents[airport] = [];
        }

        // Create a queue for BFS
        var queue = new Queue<Airport>();

        if (!visited.ContainsKey(departure))
        {
            throw new InvalidInputException("Departure airport does not exist.");
        }

        if (!_adjList.ContainsKey(departure))
        {
            return null;
        }

        // Mark the distance of the source node as 0
        distance.Add(departure, 0);

        queue.Enqueue(departure);

        while (queue.Count != 0)
        {
            var currentAirport = queue.Dequeue();

            if (currentAirport == arrival)
            {
                var shortestPath = new List<Airport>();
                var vertix = arrival;

                while (vertix != null)
                {
                    shortestPath.Add(vertix);
                    var currParents = parents[vertix];

                    if (currParents != null)
                    {
                        if (currParents.Count == 0)
                        {
                            vertix = null;
                            continue;
                        }
                        var closestVert = currParents.First();

                        foreach (var currParent in currParents)
                        {
                            if (distance[currParent] < distance[closestVert])
                            {
                                closestVert = currParent;
                            }
                        }

                        vertix = closestVert;
                    }
                }
                shortestPath.Reverse();
                return shortestPath;
            }

            if (!visited.ContainsKey(currentAirport))
            {
                continue;
            }

            foreach (var neighbourAirport in _adjList[currentAirport])
            {

                if (!visited.TryGetValue(neighbourAirport.Key, out var _))
                {
                    queue.Enqueue(neighbourAirport.Key);

                    if (parents[neighbourAirport.Key] != null)
                    {
                        var parent = parents[neighbourAirport.Key];
                        parent?.Add(currentAirport);
                        parents[neighbourAirport.Key] = parent;

                        if (distance[neighbourAirport.Key] == 0)
                        {
                            distance[neighbourAirport.Key] = distance[currentAirport] + 1;
                        }
                    }

                    continue;
                }

                // If a neighbor airport has not been visited, mark it visited and enqueue it
                if (!visited[neighbourAirport.Key])
                {
                    if (parents[neighbourAirport.Key] != null)
                    {
                        var parent = parents[neighbourAirport.Key];
                        parent?.Add(currentAirport);

                        parents[neighbourAirport.Key] = parent;

                        distance[neighbourAirport.Key] = distance[currentAirport] + 1;
                    }

                    if (!queue.Contains(neighbourAirport.Key))
                    {
                        queue.Enqueue(neighbourAirport.Key);
                    }
                }
            }
        }

        // If the route is not found
        return null;
    }

    // Print the route with fewest stops or throw exception 
    public void PrintFewestStopsPath(string departureAirport, string arrivalAirport, List<Flight> flights, List<Airport> airports, Dictionary<Airport, int> distance)
    {
        var departure = airports.Find(a => a.Identifier.Equals(departureAirport));
        var arrival = airports.Find(a => a.Identifier.Equals(arrivalAirport));

        var shortestPath = new List<Airport>();

        shortestPath = departure == null || arrival == null
            ? throw new InvalidInputException("Departure or arrival airport does not exist.")
            : FewestStopsPath(departure, arrival, distance, airports);

        if (shortestPath == null)
        {
            Console.WriteLine("No path found from " + departureAirport + " to " + arrivalAirport);
            return;
        }

        Console.WriteLine("Shortest path from " + departureAirport + " to " + arrivalAirport + ":");

        Console.WriteLine("Flights to catch:");
        for (var i = 0; i < shortestPath.Count - 1; i++)
        {
            var currFlight = flights.Find(f => f.DepartureAirport.Equals(shortestPath[i].Identifier) && f.ArrivalAirport.Equals(shortestPath[i + 1].Identifier));

            if (currFlight != null)
            {
                if (i == shortestPath.Count - 1 - 1)
                {
                    Console.WriteLine(currFlight.Id);
                    break;
                }
                Console.Write(currFlight.Id + " -> ");
            }
        }

        Console.WriteLine("Airports to go through:");
        foreach (var airport in shortestPath)
        {
            Console.Write(airport.Identifier + " ");
        }
        Console.WriteLine();
    }
}

﻿using Airlines.Persistence.Basic;

namespace BusinessLogic;
internal class FlightNode(Flight flight)
{
    public Flight Flight { get; set; } = flight;
    public FlightNode? Next { get; set; }
}

using System.Collections;
using Airlines.Persistence.Basic;

namespace BusinessLogic;

public class FlightRoute : IEnumerable<Flight>, IFlightRoute<Flight>
{
    private FlightNode? _head;

    public uint Count
    {
        get
        {
            uint count = 0;
            var current = _head;

            while (current != null)
            {
                count++;
                current = current.Next;
            }

            return count;
        }
    }

    public void AddFlight(Flight flight)
    {
        // Check if the new flight is null
        if (flight == null)
        {
            throw new InvalidInputException("No command provided.");
        }

        // Check if the route is empty
        if (_head == null)
        {
            _head = new FlightNode(flight);
            Console.WriteLine($"Added flight {flight.Id} to the route.");
            return;
        }

        // Find the last flight node
        var current = _head;
        while (current.Next != null)
        {
            current = current.Next;
        }

        // Check if the departure airport of the new flight matches the arrival airport of the last flight
        if (current.Flight.ArrivalAirport == flight.DepartureAirport)
        {
            current.Next = new FlightNode(flight);
            Console.WriteLine($"Added flight {flight.Id} to the route.");
        }
        else
        {
            throw new InvalidFlightCodeException("Invalid flight id provided.");
        }
    }

    public void RemoveLast()
    {
        if (_head == null)
        {
            Console.WriteLine("No flights on the route.");
            return;
        }

        if (_head.Next == null)
        {
            _head = null;
            return;
        }

        var current = _head;
        while (current.Next != null)
        {
            current = current.Next;
        }
        current.Next = null;
    }

    public void PrintRoute()
    {
        if (_head == null)
        {
            Console.WriteLine("No flights in the route.");
            return;
        }

        var current = _head;
        Console.WriteLine("Flight Route:");
        while (current != null)
        {
            Console.WriteLine($"Flight ID: {current.Flight.Id}, Departure Airport: {current.Flight.DepartureAirport}, Arrival Airport: {current.Flight.ArrivalAirport}, Aircraft Model: {current.Flight.AircraftModel}");
            current = current.Next;
        }
    }

    IEnumerator<Flight> IEnumerable<Flight>.GetEnumerator() => throw new NotImplementedException();
    IEnumerator IEnumerable.GetEnumerator() => throw new NotImplementedException();
}

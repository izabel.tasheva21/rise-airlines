﻿using Airlines.Persistence.Basic;

namespace BusinessLogic;
public interface IFlightRoute<T>
{
    uint Count { get; }
    void AddFlight(Flight flight);
    void RemoveLast();
    void PrintRoute();
}

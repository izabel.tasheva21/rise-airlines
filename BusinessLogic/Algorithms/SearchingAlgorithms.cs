using static BusinessLogic.SortingAlgorithms;
using Airlines.Persistence.Basic;

namespace BusinessLogic
{
    public class SearchingAlgorithms
    {
        // Search the word in airport and return the index if found, else -1
        internal static int SearchAirports(List<Airport> airports, string searchWord)
        {

            var l = 0;
            var r = airports.Count - 1;

            while (l <= r)
            {
                var m = l + ((r - l) / 2);
                var res = searchWord.CompareTo(airports[m].Identifier);

                if (res == 0)
                {
                    return m;
                }
                else if (res > 0)
                {
                    l = m + 1;

                }
                else
                {
                    r = m - 1;

                }
            }
            return -1;


        }

        // Search the word in airlines and return the index if found, else -1
        internal static int SearchAirlines(List<Airline> airline, string searchWord)
        {

            var l = 0;
            var r = airline.Count - 1;

            while (l <= r)
            {
                var m = l + ((r - l) / 2);
                var res = searchWord.CompareTo(airline[m].Name);

                if (res == 0)
                {
                    return m;
                }
                else if (res > 0)
                {
                    l = m + 1;

                }
                else
                {
                    r = m - 1;

                }
            }
            return -1;


        }

        // Search the word in flights and return the index if found, else -1
        internal static int SearchFlights(List<Flight> flights, string searchWord)
        {

            var l = 0;
            var r = flights.Count - 1;

            while (l <= r)
            {
                var m = l + ((r - l) / 2);
                var res = searchWord.CompareTo(flights[m].Id);

                if (res == 0)
                {
                    return m;
                }
                else if (res > 0)
                {
                    l = m + 1;

                }
                else
                {
                    r = m - 1;

                }
            }
            return -1;


        }

        internal static bool SearchKeyWord(List<Airport> airports, List<Airline> airlines, List<Flight> flights, string searchWord)
        {
            BubbleSort(airports, (a1, a2) => string.Compare(a1.Identifier, a2.Identifier) > 0, true);
            SelectionSort(airlines, (a1, a2) => a1.Name.CompareTo(a2.Name), true);
            SelectionSort(flights, (f1, f2) => f1.Id.CompareTo(f2.Id), true);

            Console.WriteLine(" ");
            Console.WriteLine("Search for Airport, Airline or Flight:");

            var isFound = false;

            var indexOfAirport = SearchAirports(airports, searchWord);
            if (indexOfAirport >= 0)
            {
                Console.WriteLine($"Your search has been found in Airports at position {indexOfAirport}.");
                isFound = true;
            }

            var indexOfAirline = SearchAirlines(airlines, searchWord);
            if (indexOfAirline >= 0)
            {
                Console.WriteLine($"Your search has been found in Airlines at position {indexOfAirline}.");
                isFound = true;
            }

            var indexOfFlight = SearchFlights(flights, searchWord);
            if (indexOfFlight >= 0)
            {
                Console.WriteLine($"Your search has been found in Flights at position {indexOfFlight}.");
                isFound = true;
            }

            return !isFound ? throw new InvalidInputException("Word not found in aiports, airplines and flights.") : isFound;
        }
    }
}
﻿namespace BusinessLogic
{
    public class SortingAlgorithms
    {
        internal static void BubbleSort<T>(List<T> items, Func<T, T, bool> compare, bool ascending = true)
        {
            bool swapped;
            do
            {
                swapped = false;
                for (var i = 0; i < items.Count - 1; i++)
                {
                    // Determine if a swap should occur based on the sort order
                    var shouldSwap = ascending ? compare(items[i], items[i + 1]) : !compare(items[i], items[i + 1]);

                    if (shouldSwap)
                    {
                        // Swap the elements
                        (items[i], items[i + 1]) = (items[i + 1], items[i]);
                        swapped = true;
                    }
                }
            } while (swapped);
        }

        internal static void SelectionSort<T>(List<T> items, Func<T, T, int> compareFunction, bool ascending = true)
        {
            for (var i = 0; i < items.Count - 1; i++)
            {
                var minOrMaxIndex = i;
                for (var j = i + 1; j < items.Count; j++)
                {
                    // Adjusting the comparison based on the sort order
                    var shouldSwap = ascending
                        ? compareFunction(items[j], items[minOrMaxIndex]) < 0
                        : compareFunction(items[j], items[minOrMaxIndex]) > 0;

                    if (shouldSwap)
                    {
                        minOrMaxIndex = j;
                    }
                }

                if (minOrMaxIndex != i)
                {
                    // Swapping the elements
                    (items[i], items[minOrMaxIndex]) = (items[minOrMaxIndex], items[i]);
                }
            }
        }
    }
}

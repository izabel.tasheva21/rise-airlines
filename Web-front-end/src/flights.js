import { fetchAll } from "./Api/httpRequests.js";
import { createFlightTableRow } from "./Utils/createDomElements.js";
import { handleAddFlight } from "./Form/submitForm.js";
import { ENDPOINTS } from "./Api/config.js";

const tbody = document.querySelector("#main-table tbody");

document.addEventListener("DOMContentLoaded", () => {
  document
    .getElementById("submit-entity-form")
    .addEventListener("submit", handleAddFlight);

  fetchAll(ENDPOINTS.FLIGHTS)
    .then((flights) => {
      flights.forEach((flight) => {
        const row = createFlightTableRow(flight);
        tbody.appendChild(row);
      });
    })
    .catch((e) => {
      console.error(e.message);
    });
});

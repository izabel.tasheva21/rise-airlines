import { add } from "../Api/httpRequests.js";
import {
  createAirlineTableRow,
  createAirportTableRow,
  createFlightTableRow,
} from "../Utils/createDomElements.js";
import { ENDPOINTS } from "../Api/config.js";

// Airline form
const airlineNameInput = document.getElementById("airline-name");
const airlineFoundedInput = document.getElementById("airline-founded");
const airlineFleetSizeInput = document.getElementById("airline-fleet-size");
const airlineDescriptionInput = document.getElementById("airline-description");
const tbody = document.querySelector("#main-table tbody");

// Airport form
const airportNameInput = document.getElementById("airport-name");
const airportCountryInput = document.getElementById("airport-country");
const airportCityInput = document.getElementById("airport-city");
const airportCodeInput = document.getElementById("airport-code");
const airportRunwaysInput = document.getElementById("airport-runways-count");
const airportFoundedInput = document.getElementById("airport-founded");

// Flight form
const flightNumberInput = document.getElementById("flight-number");
const flightFromInput = document.getElementById("flight-from-airport");
const flightToInput = document.getElementById("flight-to-airport");
const flightDepartureInput = document.getElementById(
  "flight-departure-datetime"
);
const flightArrivalInput = document.getElementById("flight-arrival-datetime");

export const handleAddAirline = (event) => {
  event.preventDefault();

  const newAirline = {
    name: airlineNameInput.value,
    founded: airlineFoundedInput.value,
    fleetSize: airlineFleetSizeInput.value,
    description: airlineDescriptionInput.value,
  };

  add(ENDPOINTS.AIRLINES, newAirline)
    .then((airline) => {
      const row = createAirlineTableRow(airline);
      tbody.appendChild(row);
      event.target.reset();
    })
    .catch((error) => {
      console.error("Error adding airline:", error.message);
    });
};

export const handleAddAirport = (event) => {
  event.preventDefault();

  const newAirport = {
    name: airportNameInput.value,
    country: airportCountryInput.value,
    city: airportCityInput.value,
    code: airportCodeInput.value,
    runwaysCount: airportRunwaysInput.value,
    founded: airportFoundedInput.value,
  };

  add(ENDPOINTS.AIRPORTS, newAirport)
    .then((airport) => {
      const row = createAirportTableRow(airport);
      tbody.appendChild(row);
      event.target.reset();
    })
    .catch((error) => {
      console.error("Error adding airline:", error.message);
    });
};

export const handleAddFlight = (event) => {
  event.preventDefault();

  const newFlight = {
    flightNumber: flightNumberInput.value,
    fromAirport: flightFromInput.value,
    toAirport: flightToInput.value,
    departureDateTime: flightDepartureInput.value,
    arrivalDateTime: flightArrivalInput.value,
  };

  add(ENDPOINTS.FLIGHTS, newFlight)
    .then((flight) => {
      const row = createFlightTableRow(flight);
      tbody.appendChild(row);
      event.target.reset();
    })
    .catch((error) => {
      console.error("Error adding airline:", error.message);
    });
};

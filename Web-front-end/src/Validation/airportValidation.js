﻿document.addEventListener("DOMContentLoaded", () => {
  // Form Fields
  const name = document.getElementById("airport-name");
  const country = document.getElementById("airport-country");
  const city = document.getElementById("airport-city");
  const code = document.getElementById("airport-code");
  const runways = document.getElementById("airport-runways-count");
  const founded = document.getElementById("airport-founded");

  // Error Message Fields
  const nameError = document.getElementById("airport-name-error");
  const countryError = document.getElementById("airport-country-error");
  const cityError = document.getElementById("airport-city-error");
  const codeError = document.getElementById("airport-code-error");
  const runwaysError = document.getElementById("airport-runways-error");
  const foundedError = document.getElementById("airport-founded-error");

  // Button Disable/Enable Logic
  const submitErrors = {};
  document.getElementById("submit-form-button").disabled = false;
  const enableSubmitButton = (button) =>
    (document.getElementById(button).disabled = false);
  const disableSubmitButton = (button) =>
    (document.getElementById(button).disabled = true);
  const updateButtonStatus = (button, errorObj) => {
    if (Object.values(errorObj).some((error) => error != null)) {
      disableSubmitButton(button);
    } else {
      enableSubmitButton(button);
    }
  };

  // Define validation functions
  const validateName = (field, errorField, errorsObj, updateButton) => {
    const value = field.value.trim();

    if (value === "") {
      errorsObj.name = "Please enter a name.";
      errorField.textContent = errorsObj.name;
      updateButtonStatus(updateButton, errorsObj);

      return false;
    } else if (value.length < 1 || value.length >= 20) {
      errorsObj.name = "Name must be between 1 and 20 symbols.";
      errorField.textContent = errorsObj.name;
      updateButtonStatus(updateButton, errorsObj);

      return false;
    } else {
      errorsObj.name = null;
      errorField.textContent = "";
      updateButtonStatus(updateButton, errorsObj);

      return true;
    }
  };

  const validateCountry = (field, errorField, errorsObj, updateButton) => {
    const value = field.value.trim();

    if (value === "") {
      errorsObj.country = "Please enter a country.";
      errorField.textContent = errorsObj.country;
      updateButtonStatus(updateButton, errorsObj);

      return false;
    } else if (value.length < 1 || value.length >= 20) {
      errorsObj.country = "Country must be between 1 and 20 symbols.";
      errorField.textContent = errorsObj.country;
      updateButtonStatus(updateButton, errorsObj);

      return false;
    } else {
      errorsObj.country = null;
      errorField.textContent = "";
      updateButtonStatus(updateButton, errorsObj);

      return true;
    }
  };

  const validateCity = (field, errorField, errorsObj, updateButton) => {
    const value = field.value.trim();

    if (value === "") {
      errorsObj.city = "Please enter a city.";
      errorField.textContent = errorsObj.city;
      updateButtonStatus(updateButton, errorsObj);

      return false;
    } else if (value.length < 1 || value.length >= 20) {
      errorsObj.city = "City must be between 1 and 20 symbols.";
      errorField.textContent = errorsObj.city;
      updateButtonStatus(updateButton, errorsObj);

      return false;
    } else {
      errorsObj.city = null;
      errorField.textContent = "";
      updateButtonStatus(updateButton, errorsObj);

      return true;
    }
  };

  const validateCode = (field, errorField, errorsObj, updateButton) => {
    const value = field.value.trim();

    if (value === "") {
      errorsObj.code = "Please enter a code.";
      errorField.textContent = errorsObj.code;
      updateButtonStatus(updateButton, errorsObj);

      return false;
    } else if (value.length != 3) {
      errorsObj.code = "Code must be exactly 3 letters.";
      errorField.textContent = errorsObj.code;
      updateButtonStatus(updateButton, errorsObj);

      return false;
    } else {
      errorsObj.code = null;
      errorField.textContent = "";
      updateButtonStatus(updateButton, errorsObj);

      return true;
    }
  };

  const validateFounded = (field, errorField, errorsObj, updateButton) => {
    const value = field.value.trim();

    if (value === "") {
      errorsObj.founded = "Please enter the date of foundation.";
      errorField.textContent = errorsObj.founded;
      updateButtonStatus(updateButton, errorsObj);

      return false;
    } else {
      errorsObj.founded = null;
      errorField.textContent = "";
      updateButtonStatus(updateButton, errorsObj);

      return true;
    }
  };

  const validateRunways = (field, errorField, errorsObj, updateButton) => {
    const value = field.value.trim();

    if (value === "") {
      errorsObj.runways = "Please enter the runways.";
      errorField.textContent = errorsObj.runways;
      updateButtonStatus(updateButton, errorsObj);

      return false;
    } else if (value < 1 || isNaN(value)) {
      errorsObj.runways = "Runways count must be a positive number.";
      errorField.textContent = errorsObj.runways;
      updateButtonStatus(updateButton, errorsObj);

      return false;
    } else {
      errorsObj.runways = null;
      errorField.textContent = "";
      updateButtonStatus(updateButton, errorsObj);

      return true;
    }
  };

  // Add event listeners
  name.addEventListener("blur", () =>
    validateName(name, nameError, submitErrors, "submit-form-button")
  );
  country.addEventListener("blur", () =>
    validateCountry(country, countryError, submitErrors, "submit-form-button")
  );
  city.addEventListener("blur", () =>
    validateCity(city, cityError, submitErrors, "submit-form-button")
  );
  code.addEventListener("blur", () =>
    validateCode(code, codeError, submitErrors, "submit-form-button")
  );
  runways.addEventListener("blur", () =>
    validateRunways(runways, runwaysError, submitErrors, "submit-form-button")
  );
  founded.addEventListener("blur", () =>
    validateFounded(founded, foundedError, submitErrors, "submit-form-button")
  );

});

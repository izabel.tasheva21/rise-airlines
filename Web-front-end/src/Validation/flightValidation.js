﻿document.addEventListener("DOMContentLoaded", () => {
  // Form Fields
  const flightNumber = document.getElementById("flight-number");
  const fromAirport = document.getElementById("flight-from-airport");
  const toAirport = document.getElementById("flight-to-airport");
  const departureDateTime = document.getElementById(
    "flight-departure-datetime"
  );
  const arrivalDateTime = document.getElementById("flight-arrival-datetime");

  // Error Message Fields
  const flightNumberError = document.getElementById("flight-number-error");
  const fromAirportError = document.getElementById("from-airport-error");
  const toAirportError = document.getElementById("to-airport-error");
  const departureDateTimeError = document.getElementById(
    "departure-datetime-error"
  );
  const arrivalDateTimeError = document.getElementById(
    "arrival-datetime-error"
  );

  // Button Disable/Enable Logic
  const submitErrors = {};
  document.getElementById("submit-form-button").disabled = false;
  const enableSubmitButton = (button) =>
    (document.getElementById(button).disabled = false);
  const disableSubmitButton = (button) =>
    (document.getElementById(button).disabled = true);
  const updateButtonStatus = (button, errorObj) => {
    if (Object.values(errorObj).some((error) => error != null)) {
      disableSubmitButton(button);
    } else {
      enableSubmitButton(button);
    }
  };

  // Define validation functions
  const validateFlightNumber = (field, errorField, errorsObj, updateButton) => {
    const value = field.value.trim();

    if (value === "") {
      errorsObj.flightNumber = "Please enter a flight number.";
      errorField.textContent = errorsObj.flightNumber;
      updateButtonStatus(updateButton, errorsObj);

      return false;
    } else if (value.length < 1 || value.length > 5) {
      errorsObj.flightNumber = "Flight number must be between 1 and 6 symbols.";
      errorField.textContent = errorsObj.flightNumber;
      updateButtonStatus(updateButton, errorsObj);

      return false;
    } else {
      errorsObj.flightNumber = null;
      errorField.textContent = "";
      updateButtonStatus(updateButton, errorsObj);

      return true;
    }
  };

  const validateFromAirport = (field, errorField, errorsObj, updateButton) => {
    const value = field.value.trim();

    if (value === "") {
      errorsObj.fromAirport = "Please enter a your 'from' airport.";
      errorField.textContent = errorsObj.fromAirport;
      updateButtonStatus(updateButton, errorsObj);

      return false;
    } else if (value.length != 3) {
      errorsObj.fromAirport =
        "'From' airport code must be exactly 3 letters long.";
      errorField.textContent = errorsObj.fromAirport;
      updateButtonStatus(updateButton, errorsObj);

      return false;
    } else {
      errorsObj.fromAirport = null;
      errorField.textContent = "";
      updateButtonStatus(updateButton, errorsObj);

      return true;
    }
  };

  const validateToAirport = (field, errorField, errorsObj, updateButton) => {
    const value = field.value.trim();

    if (value === "") {
      errorsObj.toAirport = "Please enter a your 'to' airport.";
      errorField.textContent = errorsObj.toAirport;
      updateButtonStatus(updateButton, errorsObj);

      return false;
    } else if (value.length != 3) {
      errorsObj.toAirport = "'To' airport code must be exactly 3 letters long.";
      errorField.textContent = errorsObj.toAirport;
      updateButtonStatus(updateButton, errorsObj);

      return false;
    } else {
      errorsObj.toAirport = null;
      errorField.textContent = "";
      updateButtonStatus(updateButton, errorsObj);

      return true;
    }
  };

  const validateDepartureDateTime = (
    field,
    errorField,
    errorsObj,
    updateButton
  ) => {
    const value = new Date(field.value);

    if (field.value === "") {
      errorsObj.departureDateTime = "Departure date and time can't be empty.";
      errorField.textContent = errorsObj.departureDateTime;
      updateButtonStatus(updateButton, errorsObj);

      return false;
    } else if (value < Date.now()) {
      errorsObj.departureDateTime =
        "Departure date and time can't be in the past.";
      errorField.textContent = errorsObj.departureDateTime;
      updateButtonStatus(updateButton, errorsObj);

      return false;
    } else {
      errorsObj.departureDateTime = null;
      errorField.textContent = "";
      updateButtonStatus(updateButton, errorsObj);

      return true;
    }
  };

  const validateArrivalDateTime = (
    field,
    fieldTwo,
    errorField,
    errorsObj,
    updateButton
  ) => {
    const value = new Date(field.value);

    if (arrivalDateTime.value === "") {
      errorsObj.arrivalDateTime = "Arrival date and time can't be empty.";
      errorField.textContent = errorsObj.arrivalDateTime;
      updateButtonStatus(updateButton, errorsObj);

      return false;
    } else if (value < Date.now()) {
      errorsObj.arrivalDateTime = "Arrival date and time can't be in the past.";
      errorField.textContent = errorsObj.arrivalDateTime;
      updateButtonStatus(updateButton, errorsObj);

      return false;
    } else if (fieldTwo.value && new Date(fieldTwo.value) > value) {
      errorsObj.arrivalDateTime =
        "Arrival date and time can't be before the departure date.";
      errorField.textContent = errorsObj.arrivalDateTime;
      updateButtonStatus(updateButton, errorsObj);

      return false;
    } else {
      errorsObj.arrivalDateTime = null;
      errorField.textContent = "";
      updateButtonStatus(updateButton, errorsObj);

      return true;
    }
  };

  // Add event listeners
  flightNumber.addEventListener("blur", () =>
    validateFlightNumber(
      flightNumber,
      flightNumberError,
      submitErrors,
      "submit-form-button"
    )
  );
  fromAirport.addEventListener("blur", () =>
    validateFromAirport(
      fromAirport,
      fromAirportError,
      submitErrors,
      "submit-form-button"
    )
  );
  toAirport.addEventListener("blur", () =>
    validateToAirport(
      toAirport,
      toAirportError,
      submitErrors,
      "submit-form-button"
    )
  );
  departureDateTime.addEventListener("blur", () =>
    validateDepartureDateTime(
      departureDateTime,
      departureDateTimeError,
      submitErrors,
      "submit-form-button"
    )
  );
  arrivalDateTime.addEventListener("blur", () =>
    validateArrivalDateTime(
      arrivalDateTime,
      departureDateTime,
      arrivalDateTimeError,
      submitErrors,
      "submit-form-button"
    )
  );

});

﻿document.addEventListener("DOMContentLoaded", () => {
  // Form Fields
  const name = document.getElementById("airline-name");
  const founded = document.getElementById("airline-founded");
  const fleetSize = document.getElementById("airline-fleet-size");
  const description = document.getElementById("airline-description");

  // Error Message Fields
  const nameError = document.getElementById("airline-name-error");
  const foundedError = document.getElementById("airline-founded-error");
  const fleetSizeError = document.getElementById("airline-fleet-size-error");
  const descriptionError = document.getElementById("airline-description-error");

  // Button Disable/Enable Logic
  const submitErrors = {};
  document.getElementById("submit-form-button").disabled = false;
  const enableSubmitButton = (button) =>
    (document.getElementById(button).disabled = false);
  const disableSubmitButton = (button) =>
    (document.getElementById(button).disabled = true);
  const updateButtonStatus = (button, errorObj) => {
    if (Object.values(errorObj).some((error) => error != null)) {
      disableSubmitButton(button);
    } else {
      enableSubmitButton(button);
    }
  };

  // Define validation functions
  const validateName = (field, errorField, errorsObj, updateButton) => {
    const value = field.value.trim();

    if (value === "") {
      errorsObj.name = "Please enter a name.";
      errorField.textContent = errorsObj.name;
      updateButtonStatus(updateButton, errorsObj);

      return false;
    } else if (value.length < 1 || value.length >= 20) {
      errorsObj.name = "Name must be between 1 and 20 symbols.";
      errorField.textContent = errorsObj.name;
      updateButtonStatus(updateButton, errorsObj);

      return false;
    } else {
      errorsObj.name = null;
      errorField.textContent = "";
      updateButtonStatus(updateButton, errorsObj);

      return true;
    }
  };

  const validateFounded = (field, errorField, errorsObj, updateButton) => {
    const value = field.value.trim();

    if (value === "") {
      errorsObj.founded = "Please enter the date of foundation.";
      errorField.textContent = errorsObj.founded;
      updateButtonStatus(updateButton, errorsObj);

      return false;
    } else {
      errorsObj.founded = null;
      errorField.textContent = "";
      updateButtonStatus(updateButton, errorsObj);

      return true;
    }
  };

  const validateFleetSize = (field, errorField, errorsObj, updateButton) => {
    const value = field.value.trim();

    if (value === "") {
      errorsObj.fleetSize = "Please enter the fleet size.";
      errorField.textContent = errorsObj.fleetSize;
      updateButtonStatus(updateButton, errorsObj);

      return false;
    } else if (value < 1 || isNaN(value)) {
      errorsObj.fleetSize = "Fleet size must be a positive number.";
      errorField.textContent = errorsObj.fleetSize;
      updateButtonStatus(updateButton, errorsObj);

      return false;
    } else {
      errorsObj.fleetSize = null;
      errorField.textContent = "";
      updateButtonStatus(updateButton, errorsObj);

      return true;
    }
  };

  const validateDescription = (field, errorField, errorsObj, updateButton) => {
    const value = field.value.trim();

    if (value === "") {
      errorsObj.description = "Please enter a description.";
      errorField.textContent = errorsObj.description;
      updateButtonStatus(updateButton, errorsObj);

      return false;
    } else if (value.length < 1 || value.length >= 100) {
      errorsObj.description = "Description must be between 1 and 100 symbols.";
      errorField.textContent = errorsObj.description;
      updateButtonStatus(updateButton, errorsObj);

      return false;
    } else {
      errorsObj.description = null;
      errorField.textContent = "";
      updateButtonStatus(updateButton, errorsObj);

      return true;
    }
  };

  // Add event listeners for creation form
  name.addEventListener("blur", () =>
    validateName(name, nameError, submitErrors, "submit-form-button")
  );
  founded.addEventListener("blur", () =>
    validateFounded(founded, foundedError, submitErrors, "submit-form-button")
  );
  fleetSize.addEventListener("blur", () =>
    validateFleetSize(
      fleetSize,
      fleetSizeError,
      submitErrors,
      "submit-form-button"
    )
  );
  description.addEventListener("blur", () =>
    validateDescription(
      description,
      descriptionError,
      submitErrors,
      "submit-form-button"
    )
  );
});

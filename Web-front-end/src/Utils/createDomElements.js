export const createAirlineTableRow = (airline) => {
  const row = document.createElement("tr");
  row.setAttribute("id", airline.id);
  appendTableCellWithText(row, airline.name);
  appendTableCellWithText(row, airline.founded);
  appendTableCellWithText(row, airline.fleetSize);
  appendTableCellWithText(row, airline.description);

  return row;
};

export const createAirportTableRow = (airport) => {
  const row = document.createElement("tr");
  row.setAttribute("id", airport.code);
  appendTableCellWithText(row, airport.name);
  appendTableCellWithText(row, airport.country);
  appendTableCellWithText(row, airport.city);
  appendTableCellWithText(row, airport.code);
  appendTableCellWithText(row, airport.runwaysCount);
  appendTableCellWithText(row, airport.founded);

  return row;
};

export const createFlightTableRow = (flight) => {
  const row = document.createElement("tr");
  row.setAttribute("id", flight.flightID);
  appendTableCellWithText(row, flight.flightNumber);
  appendTableCellWithText(row, flight.departureAirport);
  appendTableCellWithText(row, flight.arrivalAirport);
  appendTableCellWithText(row, flight.departureDateTime);
  appendTableCellWithText(row, flight.arrivalDateTime);

  return row;
};

const appendTableCellWithText = (row, text) => {
  const cell = document.createElement("td");
  cell.textContent = text;
  row.appendChild(cell);
};
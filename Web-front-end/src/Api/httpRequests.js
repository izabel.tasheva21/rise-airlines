export const fetchAll = async (endpoint) => {
  try {
    const response = await fetch(endpoint);

    if (!response.ok) {
      return Promise.reject(new Error("Failed to fetch data"));
    }

    const data = await response.json();
    console.log("Fetched data:", data);
    return Promise.resolve(data);
  } catch (error) {
    console.error("Error fetching data:", error);
    return Promise.reject(error);
  }
};

export const fetchOne = async (endpoint, id) => {
  try {
    const response = await fetch(`${endpoint}/${id}`);

    if (!response.ok) {
      return Promise.reject(new Error("Failed to fetch data"));
    }

    const data = await response.json();
    console.log("Fetched data:", data);
    return Promise.resolve(data);
  } catch (error) {
    console.error("Error fetching data:", error);
    return Promise.reject(error);
  }
};

export const add = async (endpoint, entity) => {
  try {
    const response = await fetch(endpoint, {
      method: "POST",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify(entity),
    });

    if (!response.ok) {
      return Promise.reject(new Error("Failed to add entity"));
    }

    const data = await response.json();
    console.log("Added data:", data);
    return Promise.resolve(data);
  } catch (error) {
    console.error("Error adding entity:", error);
    return Promise.reject(error);
  }
};

export const update = async (endpoint, entity) => {
  try {
    const response = await fetch(endpoint, {
      method: "PUT",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify(entity),
    });

    if (!response.ok) {
      return Promise.reject(new Error("Failed to update entity"));
    }

    const data = await response.json();
    console.log("Updated data:", data);
    return Promise.resolve(data);
  } catch (error) {
    console.error("Error updating entity:", error);
    return Promise.reject(error);
  }
};

export const deleteOne = async (endpoint, id) => {
  try {
    const response = await fetch(`${endpoint}/${id}`, {
      method: "DELETE",
      headers: { "Content-Type": "application/json" },
    });

    if (!response.ok) {
      return Promise.reject(new Error("Failed to delete entity"));
    }

    const data = await response.json();
    console.log("Deleted data:", data);
    return Promise.resolve(data);
  } catch (error) {
    console.error("Error deleting entity:", error);
    return Promise.reject(error);
  }
};

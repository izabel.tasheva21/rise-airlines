export const API_BASE_URL = "http://localhost:5254";

export const ENDPOINTS = {
  AIRLINES: `${API_BASE_URL}/Airline`,
  AIRPORTS: `${API_BASE_URL}/Airport`,
  FLIGHTS: `${API_BASE_URL}/Flight`,
};

import { ENDPOINTS } from "./Api/config.js";
import { fetchAll } from "./Api/httpRequests.js";

document.addEventListener("DOMContentLoaded", () => {
  const airportCountElement = document.getElementById("airport-count");
  const airlineCountElement = document.getElementById("airline-count");
  const flightCountElement = document.getElementById("flight-count");

  Promise.all([
    fetchAll(ENDPOINTS.AIRPORTS),
    fetchAll(ENDPOINTS.AIRLINES),
    fetchAll(ENDPOINTS.FLIGHTS),
  ])
    .then(([airports, airlines, flights]) => {
      airportCountElement.textContent = airports.length;
      airlineCountElement.textContent = airlines.length;
      flightCountElement.textContent = flights.length;
    })
    .catch((error) => {
      console.error("Error fetching data:", error.message);
    });
});

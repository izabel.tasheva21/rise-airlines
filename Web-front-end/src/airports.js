import { fetchAll } from "./Api/httpRequests.js";
import { createAirportTableRow } from "./Utils/createDomElements.js";
import { handleAddAirport } from "./Form/submitForm.js";
import { ENDPOINTS } from "./Api/config.js";

const tbody = document.querySelector("#main-table tbody");

document.addEventListener("DOMContentLoaded", () => {
  document
    .getElementById("submit-entity-form")
    .addEventListener("submit", handleAddAirport);

  fetchAll(ENDPOINTS.AIRPORTS)
    .then((airports) => {
      airports.forEach((airport) => {
        const row = createAirportTableRow(airport);
        tbody.appendChild(row);
      });
    })
    .catch((e) => {
      console.error(e.message);
    });
});

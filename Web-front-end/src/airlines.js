import { fetchAll } from "./Api/httpRequests.js";
import { createAirlineTableRow } from "./Utils/createDomElements.js";
import { handleAddAirline } from "./Form/submitForm.js";
import { ENDPOINTS } from "./Api/config.js";

const tbody = document.querySelector("#main-table tbody");

document.addEventListener("DOMContentLoaded", () => {
  document
    .getElementById("submit-entity-form")
    .addEventListener("submit", handleAddAirline);

  fetchAll(ENDPOINTS.AIRLINES)
    .then((airlines) => {
      airlines.forEach((airline) => {
        const row = createAirlineTableRow(airline);
        tbody.appendChild(row);
      });
    })
    .catch((e) => {
      console.error(e.message);
    });
});

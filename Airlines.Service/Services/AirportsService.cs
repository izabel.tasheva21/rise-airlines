﻿using Airlines.Repository.Dtos;
using Airlines.Repository.Repository;

namespace Airlines.Service.Services
{
    public class AirportsService(IAirportRepository airportRepository)
    {
        private readonly IAirportRepository _airportRepository = airportRepository;

        public async Task<List<AirportDto>> GetAllAirportsAsync()
        {
            return await _airportRepository.GetAirportsAsync();
        }

        public async Task<List<AirportDto>> GetAirportsPerPageAsync(int startIndex, int itemsPerPage)
        {
            var airports = await _airportRepository.GetAirportsAsync();

            return airports.Skip(startIndex).Take(itemsPerPage).ToList();
        }

        public async Task<List<AirportDto>> SearchAirportsAsync(string searchTerm)
        {
            return await _airportRepository.SearchAirportsAsync(searchTerm);
        }

        public List<AirportDto> GetAirportsByFilter(string filter, string value)
        {
            return _airportRepository.GetAirportsByFilter(filter, value);
        }

        public bool AddAirport(AirportDto airportDto)
        {
            var existingAirport = _airportRepository.GetAirportsAsync().Result.FirstOrDefault(c => c.Code.Trim() == airportDto.Code.Trim());

            if (existingAirport != null)
            {
                return false;
            }

            _airportRepository.AddAirport(airportDto);
            return true;
        }

        public bool UpdateAirport(AirportDto airportDto)
        {
            var existingAirport = _airportRepository.GetAirportsAsync().Result.FirstOrDefault(c => c.Code == airportDto.Code);

            if (existingAirport != null)
            {
                return false;
            }

            _airportRepository.UpdateAirport(airportDto);
            return true;
        }

        public bool DeleteAirport(string code)
        {
            var existingAirport = _airportRepository.GetAirportsAsync().Result.FirstOrDefault(c => c.Code == code);

            if (existingAirport != null)
            {
                return false;
            }

            _airportRepository.DeleteAirport(code);
            return true;
        }
    }
}

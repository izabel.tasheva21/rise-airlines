﻿using Airlines.Repository.Dtos;
using Airlines.Repository.Repository;

namespace Airlines.Service.Services
{
    public class FlightsService(IFlightRepository flightRepository)
    {
        private readonly IFlightRepository _flightRepository = flightRepository;

        public async Task<List<FlightDto>> GetAllFlightsAsync()
        {
            return await _flightRepository.GetFlightsAsync();
        }

        public async Task<List<FlightDto>> GetFlightsPerPageAsync(int startIndex, int itemsPerPage)
        {
            var flights = await _flightRepository.GetFlightsAsync();

            return flights.Skip(startIndex).Take(itemsPerPage).ToList();
        }

        public async Task<List<FlightDto>> SearchFlightsAsync(string searchTerm)
        {
            return await _flightRepository.SearchFlightsAsync(searchTerm);
        }

        public List<FlightDto> GetFlightsByFilter(string filter, string value)
        {
            return _flightRepository.GetFlightsByFilter(filter, value);
        }

        public bool AddFlight(FlightDto flightDto)
        {
            var existingFlight = _flightRepository.GetFlightsAsync().Result.FirstOrDefault(c => c.FlightNumber.Trim() == flightDto.FlightNumber.Trim());

            if (existingFlight != null)
            {
                return false;
            }

            _flightRepository.AddFlight(flightDto);
            return true;
        }

        public bool UpdateFlight(FlightDto flightDto)
        {
            var existingFlight = _flightRepository.GetFlightsAsync().Result.FirstOrDefault(c => c.FlightNumber == flightDto.FlightNumber);

            if (existingFlight != null)
            {
                return false;
            }

            _flightRepository.UpdateFlight(flightDto);
            return true;
        }

        public bool DeleteFlight(string flightNumber)
        {
            var existingFlight = _flightRepository.GetFlightsAsync().Result.FirstOrDefault(c => c.FlightNumber == flightNumber);

            if (existingFlight != null)
            {
                return false;
            }

            _flightRepository.DeleteFlight(flightNumber);
            return true;
        }
    }
}


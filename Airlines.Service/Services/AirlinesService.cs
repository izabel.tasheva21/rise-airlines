﻿using Airlines.Repository.Repository;
using Airlines.Repository.Dtos;
using Microsoft.CodeAnalysis.CSharp.Syntax;

namespace Airlines.Service.Services
{
    public class AirlinesService(IAirlineRepository airlineRepository)
    {
        private readonly IAirlineRepository _airlineRepository = airlineRepository;

        public async Task<List<AirlineDto>> GetAllAirlinesAsync()
        {
            return await _airlineRepository.GetAirlinesAsync();
        }

        public async Task<List<AirlineDto>> GetAirlinesPerPageAsync(int startIndex, int itemsPerPage)
        {
            var airlines = await _airlineRepository.GetAirlinesAsync();

            return airlines.Skip(startIndex).Take(itemsPerPage).ToList();
        }

        public async Task<List<AirlineDto>> SearchAirlinesAsync(string searchTerm)
        {
            return await _airlineRepository.SearchAirlinesAsync(searchTerm);
        }

        public List<AirlineDto> GetAirlinesByFilter(string filter, string value)
        {
            return _airlineRepository.GetAirlinesByFilter(filter, value);
        }

        public bool AddAirline(AirlineDto airlineDto)
        {
            var existingAirline = _airlineRepository.GetAirlinesAsync().Result.FirstOrDefault(c => c.Name.Trim() == airlineDto.Name.Trim());

            if (existingAirline == null)
            {
                return false;
            }

            _airlineRepository.AddAirline(airlineDto);
            return true;
        }

        public bool UpdateAirline(AirlineDto airlineDto)
        {
            var existingAirline = _airlineRepository.GetAirlinesAsync().Result.FirstOrDefault(c => c.AirlineID == airlineDto.AirlineID);

            if (existingAirline == null)
            {
                return false;
            }

            _airlineRepository.UpdateAirline(airlineDto);
            return true;
        }

        public bool DeleteAirline(int airlineId)
        {
            var existingAirline = _airlineRepository.GetAirlinesAsync().Result.FirstOrDefault(c => c.AirlineID == airlineId);

            if (existingAirline == null)
            {
                return false;
            }

            _airlineRepository.DeleteAirline(airlineId);
            return true;
        }
    }
}

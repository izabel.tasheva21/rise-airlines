using Airlines.Repository.Entities;
using Airlines.Repository.Profiles;
using Airlines.Repository.Repository;
using Airlines.Service.Services;
using Microsoft.EntityFrameworkCore;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

// Call ConfigureServices to register services
ConfigureServices(builder.Services);

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseCors("AllowAll");

app.UseAuthorization();

app.MapControllers();

app.Run();

void ConfigureServices(IServiceCollection services)
{
    services.AddDbContext<AirlinesContext>(options =>
        options.UseSqlServer(builder.Configuration.GetConnectionString("Local")));

    // Add services
    services.AddScoped<FlightsService>();
    services.AddScoped<IFlightRepository, FlightRepository>();
    services.AddScoped<AirlinesService>();
    services.AddScoped<IAirlineRepository, AirlineRepository>();
    services.AddScoped<AirportsService>();
    services.AddScoped<IAirportRepository, AirportRepository>();

    // Add AutoMapper profiles
    services.AddAutoMapper(typeof(AirlineProfile));

    services.AddCors(options =>
    {
        options.AddPolicy("AllowAll",
            builder => builder.AllowAnyOrigin()
                              .AllowAnyMethod()
                              .AllowAnyHeader());
    });


}
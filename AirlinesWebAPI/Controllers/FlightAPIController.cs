﻿using Microsoft.AspNetCore.Mvc;
using Airlines.Service.Services;
using Airlines.Repository.Dtos;

namespace AirlinesWebAPI.Controllers;

[ApiController]
[Route("Flight")]
public class FlightAPIController(FlightsService flightsService) : ControllerBase
{
    private readonly FlightsService _flightsService = flightsService;

    [HttpGet]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status500InternalServerError)]
    public async Task<IActionResult> GetAllFlights()
    {
        var flights = await _flightsService.GetAllFlightsAsync();

        var error = true;

        if (flights != null)
        {
            error = false;
        }

        if (error)
        {
            return StatusCode(500, new { message = "Something went wrong while getting flights!" });
        }

        return Ok(flights);
    }

    // Request body:
    /*{
        "airlineID": 0,
        "name": "Romanian Airlines",
        "founded": "1980-05-02",
        "fleetSize": 200,
        "description": "Lorem Ipsum Dolor Sit En Consectetur Adipisicing Elit. Culpa Nisi Nostrum Explicabo Voluptatum Tempore Aliquam Quis."
      }*/


    [HttpPost]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    [ProducesResponseType(StatusCodes.Status409Conflict)]
    [ProducesResponseType(StatusCodes.Status500InternalServerError)]
    public IActionResult CreateNewFlight([FromBody] FlightDto flightFromForm)
    {
        if (flightFromForm == null)
        {
            return BadRequest(new { message = "Request body is empty." });
        }

        try
        {
            if (_flightsService.AddFlight(flightFromForm))
            {
                return Ok("Successfully created!");
            }
            else
            {
                return Conflict(new { message = "Flight already exists!" });
            }
        }
        catch (Exception)
        {
            return StatusCode(500, new { message = "Something went wrong while creating new flight!" });
        }
    }

    //Request body:
    /*{
        "flightNumber": "FL101",
        "flightID": 5,
        "departureAirport": "DCA",
        "arrivalAirport": "SFO",
        "departureDateTime": "2025-03-01T13:20:00",
        "arrivalDateTime": "2025-03-01T14:50:00"
      }*/

    [HttpPut("{flightNumber}")]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    [ProducesResponseType(StatusCodes.Status500InternalServerError)]
    public IActionResult UpdateFlight(string flightNumber, [FromBody] FlightDto updatedFlight)
    {
        if (updatedFlight == null)
        {
            return BadRequest(new { message = "Request body is empty." });
        }

        if (flightNumber != updatedFlight.FlightNumber)
        {
            return BadRequest(new { message = "Not valid flight number!" });
        }

        try
        {
            if (_flightsService.UpdateFlight(updatedFlight))
            {
                return Ok("Successfully updated!");
            }
            else
            {
                return NotFound(new { message = "Flight with this flight number is not found!" });
            }
        }
        catch (Exception)
        {
            return StatusCode(500, new { message = "Something went wrong while updating flight!" });
        }
    }

    [HttpDelete("{flightNumber}")]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    [ProducesResponseType(StatusCodes.Status500InternalServerError)]
    public IActionResult DeleteFlight(string flightNumber)
    {
        try
        {
            if (_flightsService.DeleteFlight(flightNumber))
            {
                return Ok("Successfully deleted!");
            }
            else
            {
                return NotFound(new { message = "Flight with this flight number is not found!" });
            }
        }
        catch (Exception)
        {
            return StatusCode(500, new { message = "Something went wrong while deleting flight!" });
        }
    }
}

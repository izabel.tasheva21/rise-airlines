﻿using Microsoft.AspNetCore.Mvc;
using Airlines.Service.Services;
using Airlines.Repository.Dtos;
using Microsoft.EntityFrameworkCore.SqlServer.Scaffolding.Internal;

namespace AirlinesWebAPI.Controllers;

[ApiController]
[Route("Airline")]
public class AirlineAPIController(AirlinesService airlinesService) : ControllerBase
{
    private readonly AirlinesService _airlinesService = airlinesService;

    [HttpGet]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status500InternalServerError)]
    public async Task<IActionResult> GetAllAirlines()
    {
        var airlines = await _airlinesService.GetAllAirlinesAsync();

        var error = true;

        if (airlines != null)
        {
            error = false;
        }

        if (error)
        {
            return StatusCode(500, new { message = "Something went wrong while getting airlines!" });
        }

        return Ok(airlines);
    }

    // Request body:
    /*{
        "airlineID": 0,
        "name": "Romanian Airlines",
        "founded": "1980-05-02",
        "fleetSize": 200,
        "description": "Lorem Ipsum Dolor Sit En Consectetur Adipisicing Elit. Culpa Nisi Nostrum Explicabo Voluptatum Tempore Aliquam Quis."
      }*/


    [HttpPost]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    [ProducesResponseType(StatusCodes.Status409Conflict)]
    [ProducesResponseType(StatusCodes.Status500InternalServerError)]
    public IActionResult CreateNewAirline([FromBody] AirlineDto airlineFromForm)
    {
        if (airlineFromForm == null)
        {
            return BadRequest(new { message = "Request body is empty." });
        }

        var existingAirline = _airlinesService.GetAllAirlinesAsync().Result.FirstOrDefault(c => c.Name.Trim() == airlineFromForm.Name.Trim());

        if (existingAirline != null)
        {
            return Conflict(new { message = "Airline already exists!" });
        }

        try
        {
            _airlinesService.AddAirline(airlineFromForm);
            return Ok("Successfully created!");
        }
        catch (Exception)
        {
            return StatusCode(500, new { message = "Something went wrong while creating new airline!" });
        }
    }

    //Request body:
    /*{
        "airlineID": 4,
        "name": "Canadian Airlines",
        "founded": "1986-05-05",
        "fleetSize": 350,
        "description": "Lorem Ipsum Dolor Sit En Consectetur Adipisicing Elit. Culpa Nisi Nostrum Explicabo Voluptatum Tempore Aliquam Quis."
      }*/

    [HttpPut("{airlineId}")]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    [ProducesResponseType(StatusCodes.Status500InternalServerError)]
    public IActionResult UpdateAirline(int airlineId, [FromBody] AirlineDto updatedAirline)
    {
        if (updatedAirline == null)
        {
            return BadRequest(new { message = "Request body is empty." });
        }

        if (airlineId != updatedAirline.AirlineID)
        {
            return BadRequest(new { message = "Not valid Id!" });
        }

        try
        {
            if (_airlinesService.UpdateAirline(updatedAirline))
            {
                return Ok("Successfully updated!");
            }
            else
            {
                return NotFound(new { message = "Airline with this id is not found!" });
            }
        }
        catch (Exception)
        {
            return StatusCode(500, new { message = "Something went wrong while updating airline!" });
        }
    }

    [HttpDelete("{airlineId}")]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    [ProducesResponseType(StatusCodes.Status500InternalServerError)]
    public IActionResult DeleteAirline(int airlineId)
    {
        try
        {
            if (_airlinesService.DeleteAirline(airlineId))
            {
                return Ok("Successfully deleted!");
            }
            else
            {
                return NotFound(new { message = "Airline with this id is not found!" });
            }
        }
        catch (Exception)
        {
            return StatusCode(500, new { message = "Something went wrong while deleting airline!" });
        }
    }
}

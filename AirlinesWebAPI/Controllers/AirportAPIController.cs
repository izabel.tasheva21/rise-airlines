﻿using Microsoft.AspNetCore.Mvc;
using Airlines.Service.Services;
using Airlines.Repository.Dtos;

namespace AirlinesWebAPI.Controllers;

[ApiController]
[Route("Airport")]
public class AirportAPIController(AirportsService airportsService) : ControllerBase
{
    private readonly AirportsService _airportsService = airportsService;

    [HttpGet]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status500InternalServerError)]
    public async Task<IActionResult> GetAllAirports()
    {
        var airports = await _airportsService.GetAllAirportsAsync();

        var error = true;

        if (airports != null)
        {
            error = false;
        }

        if (error)
        {
            return StatusCode(500, new { message = "Something went wrong while getting airports!" });
        }

        return Ok(airports);
    }

    [HttpPost]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    [ProducesResponseType(StatusCodes.Status409Conflict)]
    [ProducesResponseType(StatusCodes.Status500InternalServerError)]
    public IActionResult CreateNewAirport([FromBody] AirportDto airportFromForm)
    {
        if (airportFromForm == null)
        {
            return BadRequest(new { message = "Request body is empty." });
        }

        try
        {
            if (_airportsService.AddAirport(airportFromForm))
            {
                return Ok("Successfully created!");
            }
            else
            {
                return Conflict(new { message = "Airport already exists!" });
            }
        }
        catch (Exception)
        {
            return StatusCode(500, new { message = "Something went wrong while creating new airport!" });
        }
    }

    //Request body:
    /*{
        "code": "ATL",
        "airportID": 4,
        "name": "Hartsfield Jackson Atlanta International Airport",
        "country": "USA",
        "city": "Atlanta",
        "runwaysCount": 2,
        "founded": "2024-05-20"
      }*/

    [HttpPut("{code}")]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    [ProducesResponseType(StatusCodes.Status500InternalServerError)]
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Style", "IDE0046:Convert to conditional expression", Justification = "<Pending>")]
    public IActionResult UpdateAirport(string code, [FromBody] AirportDto updatedAirport)
    {
        if (updatedAirport == null)
        {
            return BadRequest(new { message = "Request body is empty." });
        }

        if (code != updatedAirport.Code)
        {
            return BadRequest(new { message = "Not valid code!" });
        }

        try
        {
            if (_airportsService.UpdateAirport(updatedAirport))
            {
                return Ok("Successfully updated!");
            }
            else
            {
                return NotFound(new { message = "Airport with this code is not found!" });
            }
        }
        catch (Exception)
        {
            return StatusCode(500, new { message = "Something went wrong while updating airport!" });
        }
    }

    [HttpDelete("{code}")]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    [ProducesResponseType(StatusCodes.Status500InternalServerError)]
    public IActionResult DeleteAirline(string code)
    {
        try
        {
            if (_airportsService.DeleteAirport(code))
            {
                return Ok("Successfully deleted!");
            }
            else
            {
                return NotFound(new { message = "Airport with this id is not found!" });
            }
        }
        catch (Exception)
        {
            return StatusCode(500, new { message = "Something went wrong while deleting airport!" });
        }
    }
}

﻿CREATE TABLE [dbo].[Airline] (
    [AirlineID]   INT           NOT NULL,
    [Name]        SMALLINT      NOT NULL,
    [Founded]     DATE          NULL,
    [FleetSize]   INT           NULL,
    [Description] VARCHAR (255) NULL,
    PRIMARY KEY CLUSTERED ([AirlineID] ASC)
);


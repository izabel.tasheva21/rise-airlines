﻿CREATE TABLE [dbo].[Airport] (
    [AirportID]    INT           IDENTITY (1, 1) NOT NULL,
    [Code]         CHAR (3)      NULL,
    [Name]         VARCHAR (100) NULL,
    [Country]      VARCHAR (100) NULL,
    [City]         VARCHAR (100) NULL,
    [RunwaysCount] INT           NULL,
    [Founded]      DATE          NULL,
    PRIMARY KEY CLUSTERED ([AirportID] ASC)
);


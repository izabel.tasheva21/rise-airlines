﻿CREATE TABLE [dbo].[Flight] (
    [FlightID]          INT           NOT NULL,
    [FlightNumber]      VARCHAR (5)   NOT NULL,
    [DepartureAirport]  VARCHAR (255) NULL,
    [ArrivalAirport]    VARCHAR (255) NULL,
    [DepartureDateTime] DATETIME      NULL,
    [ArrivalDateTime]   DATETIME      NULL,
    PRIMARY KEY CLUSTERED ([FlightID] ASC)
);

